Папка «прочей документации»
===========================

Штуки, не относящиеся ни к программе, ни к диплому/НИРу/практике.

- !Проверено.rar -- примеры лаб, на которых программа прогонялась;
- ns_statya -- статья для XXIII международной научно-технической конференции
	студентов, аспирантов и молодых учёных «Научная сессия ТУСУР–2018», за
	которую даже дали диплом второй степени;
- rsdn_checklist.doc -- изначальный анализ правил RSDN на автоматизацию;
- Описание процесса проверки ФДО.txt -- что-то вроде ТЗ с самого начала этого
	всего.