﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;

using NtvpChecker.Checking.Building;
using NtvpChecker.Checking.Originality;
using NtvpChecker.Checking.Rsdn;
using NtvpChecker.DataStructures;

namespace NtvpChecker.Checking
{
	/// <summary>
	/// Class that incapsulates code checks: compilation, RSDN, and originality checks.
	/// </summary>
	public class CodeCheckManager
	{
		/// <summary>
		/// SolutionCompiler instance to get binaries or build errors from.
		/// </summary>
		private readonly SolutionCompiler _compiler = new SolutionCompiler();

		/// <summary>
		/// Property to access last build's errors from outside.
		/// </summary>
		public IReadOnlyList<BuildError> BuildErrors { get; private set; }

		/// <summary>
		/// Property to hold last built binary's path.
		/// </summary>
		public string BinaryPath { get; private set; }

		/// <summary>
		/// SourceChecker instance to check sources.
		/// </summary>
		private readonly SourceChecker _sourceChecker = new SourceChecker();

		/// <summary>
		/// OriginalityCheckerInstance to perform originality checks.
		/// </summary>
		private readonly OriginalityChecker _originalityChecker = new OriginalityChecker();


		/// <summary>
		/// Tries to compile given solution, sets appropriate properties of itself
		/// regardless of result.
		/// </summary>
		/// <param name="solutionPath">Path to solution to compile.</param>
		/// <returns>True: build success, BinaryPath contains a valid value;
		/// false: build failure, check BuildErrors for details.</returns>
		public bool Compile(string solutionPath)
		{
			if (solutionPath == null)
			{
				throw new ArgumentNullException("solutionPath");
			}
			if (!File.Exists(solutionPath))
			{
				throw new ArgumentException($"Solution file {solutionPath} does not exist",
					"solutionPath");
			}
			// if nothing bad happens during build, this empty list will be used
			BuildErrors = new List<BuildError>().AsReadOnly();
			try
			{
				BinaryPath = _compiler.TryCompileAndGetBinaryPath(solutionPath);
				return true;
			}
			catch (ApplicationException)
			{
				BuildErrors = _compiler.GetLastBuildErrors();
				return false;
			}
		}

		/// <summary>
		/// Handles RSDN checking.
		/// </summary>
		/// <param name="sources">List of Sources to check.</param>
		/// <returns>Dictionary in which list of detected errors
		/// corresponds to Source they was detected in.
		/// If there are no erros in file, it is not present in dictionary.</returns>
		public IReadOnlyDictionary<Source, List<Error>> CheckRsdn(IReadOnlyList<Source> sources)
		{
			var ret = new Dictionary<Source, List<Error>>();
			foreach (var source in sources)
			{
				var errors = _sourceChecker.Check(source);
				if (errors.Count > 0)
				{
					ret[source] = errors;
				}
			}

			return ret;
		}

		/// <summary>
		/// Event handler for BackgroundWorker's DoWork.
		/// Does async originality checking with progress reports.
		/// </summary>
		/// <param name="sender">Event sender, BackgroundWorker that is tasked with this.</param>
		/// <param name="e">A Tuple of AnswerData and List of another AnswerDatas to check against.</param>
		public void CheckOriginalityDoWork(object sender, DoWorkEventArgs e)
		{
			var backgroundWorker = sender as BackgroundWorker;
			var argument = e.Argument as Tuple<AnswerData, IReadOnlyList<AnswerData>>;
			// this assignments made copy-pasting from old method way more straightforward
			var current = argument.Item1;
			var previous = argument.Item2;

			var ret = new List<Tuple<string, string>>();

			// so we have three loops inside each other there,
			// outermost for the sources of the current lab,
			// then for the previous labs in the middle,
			// then for sources of every previous labs as innermost

			int currentSourcesDone = 0;
			double currentSourcesTotal = current.Sources.Count;
			double previousLabsTotal = previous.Count;

			foreach (var currentSource in current.Sources)
			{
				var previousLabsDone = 0;
				foreach (var previousLab in previous)
				{
					double previousSourcesTotal = previousLab.Sources.Count;
					int previousSourcesDone = 0;
					foreach (var previousSource in previousLab.Sources)
					{
						if (backgroundWorker.CancellationPending)
						{
							backgroundWorker.ReportProgress(0);
							e.Cancel = true;
							return;
						}

						var tupleList = _originalityChecker.CheckSources(currentSource,
							previousSource);
						foreach (var tuple in tupleList)
						{
							var newItem1 = current.Data.Author + ": " + tuple.Item1;
							var newItem2 = previousLab.Data.Author + ": " + tuple.Item2;
							ret.Add(Tuple.Create(newItem1, newItem2));
						}

						previousSourcesDone++;

						// counters are relative: for instance, previousLabsPart of 66% 
						// means we processed current source from current lab to 66%.
						// If there's 10 such sources, it's 6,6% of the
						// overall progressbar. Same with the sources from previous labs.

						var currentSourcesPart = currentSourcesDone / currentSourcesTotal;
						var previousLabsPart = previousLabsDone / previousLabsTotal
							/ currentSourcesTotal;
						var previousSourcesPart = previousSourcesDone / previousSourcesTotal
							/ previousLabsTotal / currentSourcesTotal;

						var percentage = currentSourcesPart + previousLabsPart +
							previousSourcesPart;

						backgroundWorker.ReportProgress((int) (percentage * 100));
					}

					previousLabsDone++;
				}

				currentSourcesDone++;
			}

			e.Result = ret.AsReadOnly();
		}
	}
}