﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
using NtvpChecker.DataStructures;

namespace NtvpChecker.Checking.Building
{
	/// <summary>
	/// Class that creates Instances of Error on MSBuild build 
	/// by listening to error raising events.
	/// </summary>
	internal class BuildErrorLogger : Logger
	{
		/// <summary>
		/// String to hold path to solution being built. 
		/// Used to construct project's relative path like Source.Identifier.
		/// </summary>
		private string _solutionPath;

		/// <summary>
		/// List to store all build error encountered.
		/// </summary>
		public readonly List<BuildError> BuildErrors = new List<BuildError>();

		/// <summary>
		/// Prepares logger to log for a new solution and clears previous build errors.
		/// </summary>
		/// <param name="solutionPath">Path to new solution file.</param>
		public void ReadyForNewSolution(string solutionPath)
		{
			_solutionPath = solutionPath;
			BuildErrors.Clear();
		}

		/// <summary>
		/// Initialize is guaranteed to be called by MSBuild at the start of the build
		/// before any events are raised. The event handler is set here.
		/// </summary>
		/// <param name="eventSource">EventSource to listen to.</param>
		public override void Initialize(IEventSource eventSource)
		{
			if (eventSource == null)
			{
				throw new ArgumentNullException("eventSource");
			}
			eventSource.ErrorRaised += new BuildErrorEventHandler(OnErrorRaised);
		}

		/// <summary>
		/// Event handler for ErrorRaised event. Adds a new Error to BuildErrors list.
		/// </summary>
		/// <param name="sender">Event sender.</param>
		/// <param name="e">Event arguments.</param>
		private void OnErrorRaised(object sender, BuildErrorEventArgs e)
		{
			var relativeProjectPath = MakeRelativePath(_solutionPath, e.ProjectFile);
			var id = $"{relativeProjectPath}->{e.File}";
			var error = new BuildError(id, (uint) e.LineNumber, e.Message);
			BuildErrors.Add(error);
		}

		/// <summary>
		/// Creates a relative path from one file or folder to another.
		/// </summary>
		/// <param name="fromPath">Contains the directory that defines the start 
		/// of the relative path.</param>
		/// <param name="toPath">Contains the path that defines the endpoint 
		/// of the relative path.</param>
		/// <returns>The relative path from the start directory to the end path 
		/// or toPath if the paths are not related.</returns>
		public static String MakeRelativePath(String fromPath, String toPath)
		{
			// https://stackoverflow.com/a/340454

			if (String.IsNullOrEmpty(fromPath))
			{
				throw new ArgumentNullException("fromPath");
			}

			if (String.IsNullOrEmpty(toPath))
			{
				throw new ArgumentNullException("toPath");
			}

			var fromUri = new Uri(fromPath);
			var toUri = new Uri(toPath);

			if (fromUri.Scheme != toUri.Scheme)
			{
				// path can't be made relative.
				return toPath;
			}

			var relativeUri = fromUri.MakeRelativeUri(toUri);
			var relativePath = Uri.UnescapeDataString(relativeUri.ToString());

			if (toUri.Scheme.Equals("file", StringComparison.InvariantCultureIgnoreCase))
			{
				relativePath = relativePath.Replace(Path.AltDirectorySeparatorChar,
					Path.DirectorySeparatorChar);
			}

			return relativePath;
		}
	}
}