﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.Build.Evaluation;
using Microsoft.Build.Execution;
using Microsoft.Build.Framework;
using NtvpChecker.DataStructures;

namespace NtvpChecker.Checking.Building
{
	/// <summary>
	/// Class that handles MSBuild interaction to determine whether lab can be compiled.
	/// </summary>
	internal class SolutionCompiler
	{
		/// <summary>
		/// ErrorLogger instance to get errors.
		/// </summary>
		private readonly BuildErrorLogger _logger = new BuildErrorLogger();

		/// <summary>
		/// Empty dictionary to represent default settings, most probably Debug|Any CPU.
		/// </summary>
		private readonly Dictionary<string, string> _defaultSettings
			= new Dictionary<string, string>();

		/// <summary>
		/// String specifying desired build target — yep, to build the solution.
		/// </summary>
		private const string _target = "Build";

		/// <summary>
		/// Tries to compile provided solution. Raises ApplicationException on build failure.
		/// If succeeds, returns path to built executable file.
		/// Things may go hairy if there's multiple or none.
		/// </summary>
		/// <param name="solutionPath">Path to solution to compile.</param>
		/// <returns></returns>
		public string TryCompileAndGetBinaryPath(string solutionPath)
		{
			if (solutionPath == null)
			{
				throw new ArgumentNullException("solutionPath");
			}
			if (!File.Exists(solutionPath))
			{
				throw new ArgumentException($"Solution file {solutionPath} does not exist",
					"solutionPath");
			}

			_logger.ReadyForNewSolution(solutionPath);

			var projectCollection = new ProjectCollection();
			var buildParameters = new BuildParameters(projectCollection)
			{
				Loggers = new List<ILogger>
				{
					_logger
				}
			};


			var buildRequest = new BuildRequestData(solutionPath, _defaultSettings, null,
				new[] {_target}, null);
			var buildResult = BuildManager.DefaultBuildManager.Build(buildParameters,
				buildRequest);

			if (buildResult.ResultsByTarget[_target].ResultCode == TargetResultCode.Failure)
			{
				throw new ApplicationException("Compilation failed.");
			}

			// look ma, i know what linq is!
			var binary = buildResult.ResultsByTarget[_target].Items
				.First(x => x.ToString().EndsWith(".exe"));

			return binary.ToString();
		}

		/// <summary>
		/// Returns errors (if any) from the most recent build attempt.
		/// </summary>
		/// <returns>List of Errors from last TryCompileAndGetBinaryPath call.</returns>
		public IReadOnlyList<BuildError> GetLastBuildErrors()
		{
			return _logger.BuildErrors.AsReadOnly();
		}
	}
}