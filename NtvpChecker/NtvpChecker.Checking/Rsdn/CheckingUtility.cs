﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace NtvpChecker.Checking.Rsdn
{
	/// <summary>
	/// Class that houses methods used in more than one individual rule.
	/// </summary>
	internal static class CheckingUtility
	{

		/// <summary>
		/// Number of space to convert tabs to.
		/// </summary>
		private const int _tabLength = 4;

		/// <summary>
		/// Maximum allowed line length.
		/// </summary>
		private const int _maxLength = 78;

		/// <summary>
		/// Gets line number from syntax tree element.
		/// </summary>
		/// <param name="nodeOrToken">Syntax tree element to get line number from.</param>
		/// <returns>Number of first line in source code the node is on.</returns>
		public static uint GetLineNumber(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}

			var tree = nodeOrToken.SyntaxTree;
			return (uint)(tree.GetText().Lines
				.GetLineFromPosition(nodeOrToken.SpanStart).LineNumber + 1);
		}

		/// <summary>
		/// Gets any nodes inside { and } tokens. Meant to be called on stuff like class
		/// or namespace declarations.
		/// </summary>
		/// <param name="nodeOrToken">Node to get child nodes from between { and }.</param>
		/// <returns>IEnumerable of such nodes.</returns>
		public static IEnumerable<SyntaxNodeOrToken> GetInnerDeclarations(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}

				// all decarations are between { and } tokens
			var declarations = nodeOrToken.ChildNodesAndTokens()
				.SkipWhile(x => x.Kind() != SyntaxKind.OpenBraceToken)
				.Skip(1)
				.TakeWhile(x => x.Kind() != SyntaxKind.CloseBraceToken);
			return declarations;
		}

		/// <summary>
		/// Gets node identifier as a string.
		/// </summary>
		/// <param name="nodeOrToken">Node to get identifier from.</param>
		/// <returns>Identifer of the node.</returns>
		public static string GetIdentifier(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}
			string identifier;
			// namespace nesting is invalid anyway
			if (nodeOrToken.Kind() == SyntaxKind.NamespaceDeclaration)
			{
				identifier = ((NamespaceDeclarationSyntax)nodeOrToken).Name.ToString();
			}
			else
			{
				try
				{
					identifier = nodeOrToken.ChildNodesAndTokens().First(
						x => x.Kind() == SyntaxKind.IdentifierToken
						|| x.Kind() == SyntaxKind.IdentifierName).ToString();
				}
				catch (InvalidOperationException)
				{
					try
					{
						identifier = nodeOrToken.ChildNodesAndTokens().First(
						x => x.Kind() == SyntaxKind.QualifiedName).ToString();
					}
					catch (InvalidOperationException)
					{
						throw new ArgumentException("Syntax node does not have identifier"
							+ " token child.", "nodeOrToken");
					}
				}
			}
			return identifier;
		}

		/// <summary>
		/// Gets last line node spans in a source text.
		/// </summary>
		/// <param name="nodeOrToken">Node to get line number for.</param>
		/// <returns>Number of last line this node is placed.</returns>
		public static uint GetLastLineNumber(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}

			var tree = nodeOrToken.SyntaxTree;
			return (uint)(tree.GetText().Lines
				.GetLineFromPosition(nodeOrToken.Span.End).LineNumber + 1);
		}

		/// <summary>
		/// Gets line number from any position from text.
		/// </summary>
		/// <param name="tree">SyntaxTree corresponding to source.</param>
		/// <param name="position">Position to get line number for.</param>
		/// <returns>Line number corresponding to position.</returns>
		public static uint GetLineFromPosition(SyntaxTree tree, int position)
		{
			if (position < 0)
			{
				throw new ArgumentException("Position must be non-negative", "position");
			}
			return (uint)tree.GetText().Lines.GetLineFromPosition(position).LineNumber + 1;
		}

		/// <summary>
		/// Checks whether line with its tab expanded is below the set length threshold.
		/// </summary>
		/// <param name="line">Line to check. Any tabs within will be expanded.</param>
		/// <returns>True if line's length is below threshold, false otherwise.</returns>
		public static bool IsLineLengthValid(string line)
		{
			return TabsToSpaces(line, _tabLength).Length <= _maxLength;
		}

		// see https://stackoverflow.com/a/41963144

		/// <summary>
		/// Gets nearest forward tab stop position.
		/// </summary>
		/// <param name="currentPosition">Current column number.</param>
		/// <param name="tabLength">Tab length in spaces.</param>
		/// <returns></returns>
		private static int GetNearestTabStop(int currentPosition, int tabLength)
		{
			if (tabLength <= 0)
			{
				throw new ArgumentException("Tab width must be positive", "tabLength");
			}
			if (currentPosition <= 0)
			{
				throw new ArgumentException("Column position must be positive", "currentPosition");
			}

			// if already at the tab stop, jump to the next tab stop.
			if ((currentPosition % tabLength) == 1)
			{
				currentPosition += tabLength;
			}
			else
			{
				// if in the middle of two tab stops, move forward to the nearest.
				for (var i = 0; i < tabLength; i++, currentPosition++)
				{
					if ((currentPosition % tabLength) == 1)
					{
						break;
					}
				}
			}

			return currentPosition;
		}

		/// <summary>
		/// Converts string with tabs to string with spaces.
		/// </summary>
		/// <param name="input">String to convert tabs to spaces in.</param>
		/// <param name="tabLength">Tab length in spaces.</param>
		/// <returns>Converted string</returns>
		private static string TabsToSpaces(string input, int tabLength)
		{
			if (tabLength <= 0)
			{
				throw new ArgumentException("Tab width must be positive", "tabLength");
			}

			if (string.IsNullOrEmpty(input))
			{
				return input;
			}

			var output = new StringBuilder();

			int positionInOutput = 1;
			foreach (var c in input)
			{
				switch (c)
				{
					case '\t':
						int spacesToAdd = GetNearestTabStop(positionInOutput, tabLength)
							- positionInOutput;
						output.Append(new string(' ', spacesToAdd));
						positionInOutput += spacesToAdd;
						break;
					case '\n':
						output.Append(c);
						positionInOutput = 1;
						break;
					default:
						output.Append(c);
						positionInOutput++;
						break;
				}
			}
			return output.ToString();
		}
	}
}
