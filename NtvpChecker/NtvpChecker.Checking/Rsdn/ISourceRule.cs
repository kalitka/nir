﻿using System.Collections.Generic;
using Microsoft.CodeAnalysis;
using NtvpChecker.DataStructures;

namespace NtvpChecker.Checking.Rsdn
{
	/// <summary>
	/// Interface for all rules for RSDN checking.
	/// </summary>
	internal interface ISourceRule
	{
		/// <summary>
		/// Abstract method to check for specific rule violation.
		/// </summary>
		/// <param name="nodeOrToken">Syntax tree node to check,
		/// all nodes of a tree are passed.</param>
		/// <returns>List of detected errors.</returns>
		List<Error> Check(SyntaxNodeOrToken nodeOrToken);
	}
}