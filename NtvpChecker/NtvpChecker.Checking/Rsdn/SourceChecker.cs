﻿using System;
using System.Collections.Generic;

using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;

using NtvpChecker.DataStructures;
using NtvpChecker.Checking.Rsdn.Rules;

namespace NtvpChecker.Checking.Rsdn
{
	/// <summary>
	/// Class that manages RSDN check on sources
	/// by dispatching a conveyor of SourceRule descendants.
	/// </summary>
	internal class SourceChecker
	{
		/// <summary>
		/// List of rules that do the checking.
		/// </summary>
		private readonly List<ISourceRule> _rules;

		// NOOOOO
		/// <summary>
		/// That one rule that needs to recieve source's filename to do its checking.
		/// </summary>
		private readonly NamespaceDeclarationsRule _specialSnowflakeRule
			= new NamespaceDeclarationsRule();

		/// <summary>
		/// Constructor.
		/// </summary>
		public SourceChecker()
		{
			_rules = new List<ISourceRule>()
			{
				new LineLengthRule(),
				new NamespaceNestingRule(),
				new SingleDeclarationRule(),
				new EnumLinesRule(),
				new DelimiterWhitespaceRule(),
				new EnumOrderRule(),
				new AccessModifierRule(),
				new NamingRule(),
				new XmlCommentRule(),
				new UsingsOrderRule(),
				_specialSnowflakeRule,
				new SingleLineCommentRule(),
				new BlockUsageRule(),
				new StatementLinesRule(),
				new DeclarationSpacingRule(),
				new DeclarationNestingRule(),
				new OperatorSpacingRule(),
				new BaseListRule(),
				new IndentationRule(),
				new TabulationUsageRule(),
				new TrailingWhitespaceRule()
			}; 
			//   ^ rules go here
		}

		/// <summary>
		/// Method to check Source instance via stored rules.
		/// </summary>
		/// <param name="source">Source to check.</param>
		/// <returns>List of Errors detected in Source.</returns>
		public List<Error> Check(Source source)
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}
			if (source.Content.Count == 0)
			{
				throw new ArgumentException("Provided Source has zero lines of code",
					"source");
			}
			if (String.IsNullOrWhiteSpace(source.Identifier))
			{
				throw new ArgumentException("Invalid Source identifier", "source");
			}
			// timeline lost
			_specialSnowflakeRule.SetSourceName(source.Identifier);

			var sourceText = String.Join(Environment.NewLine, source.Content);

			// enables parsing of '#if DEBUG''d code used throughout the GUI
			var options = new CSharpParseOptions(LanguageVersion.Default,
				DocumentationMode.Parse, SourceCodeKind.Regular,
				new string[] { "DEBUG" });

			var tree = CSharpSyntaxTree.ParseText(sourceText, options);
			var root = tree.GetRoot();

			var ret = InternalCheck(root);

			return ret;
		}

		/// <summary>
		/// Internal recursive method to check tree node and then its children, if present.
		/// </summary>
		/// <param name="nodeOrToken">Node to check.</param>
		/// <returns>List of Error from node and all its children.</returns>
		private List<Error> InternalCheck(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}

			var ret = new List<Error>();

			foreach (var rule in _rules)
			{
				var result = rule.Check(nodeOrToken);
				ret.AddRange(result);
			}

			foreach (var child in nodeOrToken.ChildNodesAndTokens())
			{
				var deeperResult = InternalCheck(child);
				ret.AddRange(deeperResult);
			}
			return ret;
		}
	}
}