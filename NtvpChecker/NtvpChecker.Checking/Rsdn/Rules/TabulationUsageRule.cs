﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;

using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;

using NtvpChecker.DataStructures;

namespace NtvpChecker.Checking.Rsdn.Rules
{
	/// <summary>
	/// Class that checks that tabs are only used as code indentation and nothing else.
	/// </summary>
	internal class TabulationUsageRule : ISourceRule
	{

		/// <summary>
		/// Message to show when rule is violated.
		/// </summary>
		private readonly string _message
			= ConfigurationManager.AppSettings["TabulationUsageError"];

		/// <summary>
		/// Checks proper usage of tabs: any WhitespaceTrivia with tabs must have a
		/// newline before and start with a tab.
		/// </summary>
		/// <param name="nodeOrToken">Syntax tree node to check,
		/// all nodes of a tree are passed.</param>
		/// <returns>List of detected errors.</returns>
		public List<Error> Check(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}

			var ret = new List<Error>();

			var whitespaces = nodeOrToken.GetLeadingTrivia()
				.Where(x => x.Kind() == SyntaxKind.WhitespaceTrivia).ToList();

			whitespaces.AddRange(nodeOrToken.GetTrailingTrivia()
				.Where(x => x.Kind() == SyntaxKind.WhitespaceTrivia));

			foreach (var trivia in whitespaces)
			{
				if (Regex.IsMatch(trivia.ToString(), @"\t+"))
				{
					if (nodeOrToken.SyntaxTree.GetText()[trivia.SpanStart - 1] != '\n'
						|| !trivia.ToString().StartsWith("\t"))
					{
						var error = new Error(CheckingUtility
							.GetLineFromPosition(nodeOrToken.SyntaxTree, trivia.SpanStart),
							_message);
						ret.Add(error);
					}
				}
			}
			return ret;
		}
	}
}
