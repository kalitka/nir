﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using NtvpChecker.DataStructures;

namespace NtvpChecker.Checking.Rsdn.Rules
{
	/// <summary>
	/// Checks for correct namespace usage - that is, no nesting.
	/// </summary>
	internal class NamespaceNestingRule : ISourceRule
	{
		/// <summary>
		/// Message to show when rule is violated
		/// </summary>
		private readonly string _message
			= ConfigurationManager.AppSettings["NamespaceNestingError"];

		/// <summary>
		/// Checks every namespace declaration: it's parent must be a CompilationUnit,
		/// it is declared wrong otherwise.
		/// </summary>
		/// <param name="nodeOrToken">Syntax tree node to check,
		/// all nodes of a tree are passed.</param>
		/// <returns>List of detected errors.</returns>
		public  List<Error> Check(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}

			var ret = new List<Error>();
			if (nodeOrToken.Kind() != SyntaxKind.NamespaceDeclaration)
			{
				return ret;
			}

			if (nodeOrToken.Parent == null
				|| nodeOrToken.Parent.Kind() != SyntaxKind.CompilationUnit)
			{
				var error = new Error(CheckingUtility.GetLineNumber(nodeOrToken), _message);
				ret.Add(error);
			}

			return ret;
		}
	}
}