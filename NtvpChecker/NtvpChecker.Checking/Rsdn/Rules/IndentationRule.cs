﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Linq;

using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.Text;

using NtvpChecker.DataStructures;

namespace NtvpChecker.Checking.Rsdn.Rules
{
	/// <summary>
	/// Class that does indentation and braces formatting checks.
	/// </summary>
	internal class IndentationRule : ISourceRule
	{
		/// <summary>
		/// Message to show when wrong level of indentation was detected.
		/// </summary>
		private readonly string _message
			= ConfigurationManager.AppSettings["IndentationError"];

		/// <summary>
		/// Message to show when brace is not on its own line.
		/// </summary>
		private readonly string _messageNotOnLine
			= ConfigurationManager.AppSettings["IndentationLineError"];

		/// <summary>
		/// Tokens that are considered to end the expression.
		/// </summary>
		private readonly List<SyntaxKind> _lineStops = new List<SyntaxKind>()
		{
			SyntaxKind.SemicolonToken,
			SyntaxKind.OpenBraceToken,
			SyntaxKind.CloseBraceToken
		};

		/// <summary>
		/// Kinds of comma-separated lists that should have extra indents on second line
		/// and beyond.
		/// </summary>
		private readonly List<SyntaxKind> _expressionContinues = new List<SyntaxKind>()
		{
			SyntaxKind.ArgumentList,
			SyntaxKind.BaseList,
			SyntaxKind.ParameterList
		};

		/// <summary>
		/// Method that checks indentation and placement of braces.
		/// </summary>
		/// <param name="nodeOrToken">Syntax tree node to check,
		/// all nodes of a tree are passed.</param>
		/// <returns>List of detected errors.</returns>
		public List<Error> Check(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}

			var ret = new List<Error>();
			if (nodeOrToken.Kind() != SyntaxKind.CompilationUnit)
			{
				return ret;
			}

			var lines = nodeOrToken.SyntaxTree.GetText().Lines;
			var root = nodeOrToken.SyntaxTree.GetRoot();

			// indent level couniting
			int indentLevel = 0;
			// continuation of a non-finished line must have an extra level of indentation
			bool isContinuedLine = false;

			for (int i = 0; i < lines.Count; i++)
			{
				var line = lines[i];

				// why bother with empty lines
				if (String.IsNullOrWhiteSpace(line.ToString()))
				{
					continue;
				}

				// here's the tricky part
				// non-matched } on the line decrease indent level before the check
				//  as they have one less indent level than expressions inside the block
				// non-matched { increase indent for the next line
				// if there's { and } in line, their effect is cancelled by each other


				// here we extract all the tokens from the line
				var lineTokens = new List<SyntaxNodeOrToken>();
				var index = line.Span.Start;
				// used to keep track of { } on single line cancelling each other
				int openBraceCount = 0;

				while (index <= line.SpanIncludingLineBreak.End - 1)
				{
					var token = root.FindToken(index);
					lineTokens.Add(token);
					index = token.FullSpan.End;

					// braces also cancel extra tab from continued line

					if (token.Kind() == SyntaxKind.OpenBraceToken)
					{
						openBraceCount++;
						isContinuedLine = false;
					}

					if (token.Kind() == SyntaxKind.CloseBraceToken)
					{
						openBraceCount--;
						isContinuedLine = false;

						if (openBraceCount < 0)
						{
							indentLevel--;
						}
					}
				}

				// dirty hack: switch case's content has one extra tab,
				// so here we check explicitly for that
				var node = root.FindNode(line.Span);
				var isInsideSwitch = false;
				while (node.Parent != null)
				{
					if (node.Parent.Kind() == SyntaxKind.SwitchSection)
					{
						isInsideSwitch = true;
						break;
					}
					node = node.Parent;
				}

				var actualIndent = indentLevel;
				// line is part of switch-case or is a continuation from previous line,
				// there's extra indent
				if (isContinuedLine || (isInsideSwitch
					&& !node.Kind().ToString().EndsWith("SwitchLabel")))
				{
					actualIndent++;
				}

				// actual check
				if (GetIndentLevel(line) != actualIndent)
				{
					var error = new Error((uint)i + 1, _message);
					ret.Add(error);
				}


				// now check for any { with no matching } and increase indent
				openBraceCount = 0;
				// roles of { } are reversed
				var lineTokensReverse = new List<SyntaxNodeOrToken>(lineTokens);
				lineTokensReverse.Reverse();
				foreach (var token in lineTokensReverse)
				{
					if (token.Kind() == SyntaxKind.OpenBraceToken)
					{
						openBraceCount++;

						if (openBraceCount > 0)
						{
							indentLevel++;
						}
						// also checks it should be on its own line
						if (lineTokensReverse.Count > 1)
						{
							var error = new Error((uint)i + 1, _messageNotOnLine);
							ret.Add(error);
						}

					}

					if (token.Kind() == SyntaxKind.CloseBraceToken)
					{
						openBraceCount--;
						// also checks it should be on its own line,
						// maybe with a following semicolon
						if (!(lineTokensReverse.Count == 1 ||
							(lineTokensReverse.First().Kind() != SyntaxKind.SemicolonToken
								|| lineTokensReverse.Count == 2)))
						{
							var error = new Error((uint)i + 1, _messageNotOnLine);
							ret.Add(error);
						}
					}
				}

				// monstrous check whether this line has incomplete statement
				// if this line ends with {, } or ;, or is a comment or an attribute list
				// or ends with comma and is not kind of stuff that should be continued
				// after a comma (like method arguments),
				// then there should be no extra tab on nextline
				if (_lineStops.Contains(lineTokens.Last().Kind()) || IsComment(line, root)
					|| (lineTokens.Last().Kind() == SyntaxKind.CommaToken
						&& !_expressionContinues.Contains(lineTokens.Last().Parent.Kind()))
					|| lineTokens.Last().Parent.Kind() == SyntaxKind.AttributeList)
				{
					isContinuedLine = false;
				}
				else
				{
					isContinuedLine = true;
				}
			}

			return ret;
		}

		/// <summary>
		/// Gets indentation level as amount of tabs in the beginning of a line.
		/// </summary>
		/// <param name="line">Line to check.</param>
		/// <returns>Number of tabs.</returns>
		private uint GetIndentLevel(TextLine line)
		{
			if (line == null)
			{
				throw new ArgumentNullException("line");
			}

			var str = line.ToString();
			var regex = new Regex(@"^(\t*)", RegexOptions.Compiled);

			var match = regex.Match(str);
			// regex contains *, there's always a match (maybe of zero lenght, but that's ok)
			return (uint)(match.Groups[1].Value.Length);
		}

		/// <summary>
		/// Checks whether line contains a comment or an if preprocessor directive
		/// that is treated like a comment anyway.
		/// </summary>
		/// <param name="line">Line to check.</param>
		/// <param name="root">Root of a syntax tree that is currently processed.</param>
		/// <returns>True is line looks like a comment, false otherwise.</returns>
		private bool IsComment(TextLine line, SyntaxNode root)
		{

			if (line == null)
			{
				throw new ArgumentNullException("line");
			}

			if (root == null)
			{
				throw new ArgumentNullException("root");
			}

			var node = root.FindToken(line.Span.Start).Parent;
			foreach (var leadingTrivia in node.GetLeadingTrivia())
			{
				if (leadingTrivia.Kind().ToString().IndexOf("CommentTrivia") != -1
					|| leadingTrivia.Kind().ToString().IndexOf("IfDirectiveTrivia") != -1)
				{
					var first = CheckingUtility.GetLineFromPosition(node.SyntaxTree, 
						leadingTrivia.FullSpan.Start);
					var last = CheckingUtility.GetLineFromPosition(node.SyntaxTree,
						leadingTrivia.FullSpan.End);
					var oneIndexedLineNumber = line.LineNumber + 1;
					if (oneIndexedLineNumber >= first && oneIndexedLineNumber <= last)
					{
						return true;
					}
				}
			}
			return false;
		}
	}
}
