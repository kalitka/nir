﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;

using NtvpChecker.DataStructures;

namespace NtvpChecker.Checking.Rsdn.Rules
{
	/// <summary>
	/// Class that encapsulates operator spacing checks.
	/// </summary>
	internal class OperatorSpacingRule : ISourceRule
	{

		// still a mess, but less so than its first iteration

		/// <summary>
		/// Message to show when binary operator is not spaced from its operands.
		/// </summary>
		private readonly string _messageBinary
			= ConfigurationManager.AppSettings["OperatorSpacingError"];

		/// <summary>
		/// Message to show when unary operator is spaced from its operand.
		/// </summary>
		private readonly string _messageUnary
			= ConfigurationManager.AppSettings["OperatorSpacingUnaryError"];

		/// <summary>
		/// Message to show when this rule detected that less than and/or greater than
		/// tokens in generic declaration are spaced incorrectly.
		/// </summary>
		private readonly string _messageGeneric
			= ConfigurationManager.AppSettings["OperatorSpacingGenericError"];

		/// <summary>
		/// Message to show when spacing around parentheses is incorrect.
		/// </summary>
		private readonly string _messageParens
			= ConfigurationManager.AppSettings["ParenthesesSpacingError"];

		/// <summary>
		/// Token kinds of binary operators. Includes some unary (like minus) as well.
		/// </summary>
		private readonly List<SyntaxKind> _binaryTokens = new List<SyntaxKind>()
		{
			SyntaxKind.EqualsToken,
			// also unary pointer derefencing in unsafe
			SyntaxKind.AsteriskToken,
			SyntaxKind.SlashToken,
			SyntaxKind.PercentToken,
			// also unary for whatever reason
			SyntaxKind.PlusToken,
			// also unary
			SyntaxKind.MinusToken,
			SyntaxKind.LessThanLessThanToken,
			SyntaxKind.GreaterThanGreaterThanToken,
			SyntaxKind.LessThanToken,
			SyntaxKind.GreaterThanToken,
			SyntaxKind.LessThanEqualsToken,
			SyntaxKind.GreaterThanEqualsToken,
			SyntaxKind.EqualsEqualsToken,
			SyntaxKind.ExclamationEqualsToken,
			// also unary 'address of' operation in unsafe
			SyntaxKind.AmpersandToken,
			SyntaxKind.CaretToken,
			SyntaxKind.BarToken,
			SyntaxKind.AmpersandAmpersandToken,
			SyntaxKind.BarBarToken,
			SyntaxKind.QuestionQuestionToken,
			SyntaxKind.PlusEqualsToken,
			SyntaxKind.MinusEqualsToken,
			SyntaxKind.AsteriskEqualsToken,
			SyntaxKind.SlashEqualsToken,
			SyntaxKind.PercentEqualsToken,
			SyntaxKind.AmpersandEqualsToken,
			SyntaxKind.BarEqualsToken,
			SyntaxKind.CaretEqualsToken,
			SyntaxKind.LessThanLessThanEqualsToken,
			SyntaxKind.GreaterThanGreaterThanEqualsToken,
			SyntaxKind.EqualsGreaterThanToken
		};

		/// <summary>
		/// Token kinds of pure unary operators.
		/// </summary>
		private readonly List<SyntaxKind> _unaryTokens = new List<SyntaxKind>()
		{
			SyntaxKind.PlusPlusToken,
			SyntaxKind.MinusMinusToken,
			SyntaxKind.ExclamationToken,
			SyntaxKind.TildeToken
		};

		/// <summary>
		/// (Most common) chars that do not form an identifier or keyword.
		/// </summary>
		private readonly List<char> _delimeters = new List<char>()
		{
			' ',
			'\t',
			'\r',
			'\n',
			';'
		};

		/// <summary>
		/// Maps tokens that can be both unary and binary operators to parent unary
		/// expressions. If token's parent is this, it's an unary token, binary otherwise.
		/// </summary>
		private readonly Dictionary<SyntaxKind, SyntaxKind> _unaryCounterparts
			= new Dictionary<SyntaxKind, SyntaxKind>()
		{
			{ SyntaxKind.MinusToken, SyntaxKind.UnaryMinusExpression },
			{ SyntaxKind.PlusToken, SyntaxKind.UnaryPlusExpression },
			{ SyntaxKind.AmpersandToken, SyntaxKind.AddressOfExpression },
			{ SyntaxKind.AsteriskToken, SyntaxKind.PointerIndirectionExpression }
		};


		/// <summary>
		/// Checks operator token for proper spacing.
		/// </summary>
		/// <param name="nodeOrToken">Syntax tree node to check,
		/// all nodes of a tree are passed.</param>
		/// <returns>List of detected errors.</returns>
		public  List<Error> Check(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}

			var ret = new List<Error>();

			Error result = null;

			if (IsBinaryOperator(nodeOrToken))
			{
				result = CheckBinary(nodeOrToken);
			}
			else if (IsUnaryOperator(nodeOrToken))
			{
				result = CheckUnary(nodeOrToken);
			}
			else if (IsGenericDeclaration(nodeOrToken))
			{
				result = CheckTemplate(nodeOrToken);
			}
			// parentheses are always parentheses
			else if (nodeOrToken.Kind() == SyntaxKind.OpenParenToken ||
				nodeOrToken.Kind() == SyntaxKind.CloseParenToken)
			{
				result = CheckParentheses(nodeOrToken);
			}

			if (result != null)
			{
				ret.Add(result);
			}

			return ret;
		}

		/// <summary>
		/// Checks whether provided node is a binary operator token.
		/// </summary>
		/// <param name="nodeOrToken">Node to check.</param>
		/// <returns>True if the node is a binary operator, false otherwise.</returns>
		private bool IsBinaryOperator(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}
			if (_binaryTokens.Contains(nodeOrToken.Kind()))
			{
				// unary operations that share same tokens!
				if (_unaryCounterparts.Keys.Contains(nodeOrToken.Kind())
					&& _unaryCounterparts[nodeOrToken.Kind()] == nodeOrToken.Parent.Kind())
				{
					return false;
				}
				return !IsGenericDeclaration(nodeOrToken);
			}
			return false;
		}

		/// <summary>
		/// Checks whether provided node is either less than or greater than token
		/// in a generic declaration.
		/// </summary>
		/// <param name="nodeOrToken">Node to check.</param>
		/// <returns>True if the node is a generic declaration part, false otherwise.</returns>
		private bool IsGenericDeclaration(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}
			if (nodeOrToken.Kind() == SyntaxKind.GreaterThanToken
				|| nodeOrToken.Kind() == SyntaxKind.LessThanToken)
			{
				if (nodeOrToken.Parent.Kind() == SyntaxKind.TypeArgumentList)
				{
					return true;
				}
			}
			return false;
		}

		/// <summary>
		/// Checks whether provided node is a unary operator token.
		/// </summary>
		/// <param name="nodeOrToken">Node to check.</param>
		/// <returns>True if the node is a unary operator, false otherwise.</returns>
		private bool IsUnaryOperator(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}
			var kind = nodeOrToken.Kind();
			return _unaryTokens.Contains(kind) || (_unaryCounterparts.Keys.Contains(kind)
				&& nodeOrToken.Parent.Kind() == _unaryCounterparts[kind]);
		}

		/// <summary>
		/// Checks binary operators to have delimiters on both sides.
		/// </summary>
		/// <param name="nodeOrToken">Node to check.</param>
		/// <returns>Detected error or null.</returns>
		private Error CheckBinary(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}

			if (!(HasLeftDelimiter(nodeOrToken) && HasRightDelimiter(nodeOrToken)))
			{
				return new Error(CheckingUtility.GetLineNumber(nodeOrToken), _messageBinary);
			}
			return null;
		}

		/// <summary>
		/// Checks unary operators to have operand at one side.
		/// </summary>
		/// <param name="nodeOrToken">Node to check.</param>
		/// <returns>Detected error or null.</returns>
		private Error CheckUnary(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}

			var error = new Error(CheckingUtility.GetLineNumber(nodeOrToken), _messageUnary);

			// these can be either prefix of postfix
			if (nodeOrToken.Kind() == SyntaxKind.PlusPlusToken
				|| nodeOrToken.Kind() == SyntaxKind.MinusMinusToken)
			{
				if (HasLeftDelimiter(nodeOrToken) && HasRightDelimiter(nodeOrToken))
				{
					return error;
				}
			}
			// rest is prefix
			else
			{
				if (!HasRightDelimiter(nodeOrToken))
				{
					return error;
				}
			}
			return null;
		}

		/// <summary>
		/// Checks either of delimiters of a generic declaration: left less than must not
		/// have any delimiters, right greater than must have a parameter type to the left.
		/// </summary>
		/// <param name="nodeOrToken">Node to check.</param>
		/// <returns>Detected error or null.</returns>
		private Error CheckTemplate(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}

			bool isRight;

			switch (nodeOrToken.Kind())
			{
				case SyntaxKind.LessThanToken:
					isRight = !HasLeftDelimiter(nodeOrToken) && !HasRightDelimiter(nodeOrToken);
					break;
				case SyntaxKind.GreaterThanToken:
					isRight = !HasLeftDelimiter(nodeOrToken);
					break;
				default:
					throw new ArgumentException("Kind of token here must be either LessThanToken "
						+ "or GreaterThanToken", "nodeOrToken");
			}
			return isRight ? null : new Error(CheckingUtility.GetLineNumber(nodeOrToken),
				_messageGeneric);
		}

		/// <summary>
		/// Checks either of parentheses: content inside must not be separated from them,
		/// left ( can only have space to the left, if token to the left is either
		/// , or ; or binary operator or a keyword (except base() and typeof() which are
		/// used like methods despite being a keywords)
		/// </summary>
		/// <param name="nodeOrToken">Node to check.</param>
		/// <returns>Error detected or null.</returns>
		private Error CheckParentheses(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}

			bool isRight;

			switch (nodeOrToken.Kind())
			{
				case SyntaxKind.OpenParenToken:
					var prev = GetPreviousToken(nodeOrToken);
					var kind = prev.Kind();
					// what am i doing with my life
					var shouldBeSpaced = IsBinaryOperator(prev)
						|| kind == SyntaxKind.SemicolonToken
						|| kind == SyntaxKind.CommaToken
						// base() is called like a method, but 'base' itself is a keyword
						// same with typeof
						|| (kind.ToString().EndsWith("Keyword") 
							&& (kind.ToString() != "BaseKeyword" && kind.ToString() != "TypeOfKeyword"));
					isRight = (shouldBeSpaced == HasLeftDelimiter(nodeOrToken)) 
						&& !HasRightDelimiter(nodeOrToken);
					break;
				case SyntaxKind.CloseParenToken:
					// just make sure it's not pushed away from parens contents
					isRight = !HasLeftDelimiter(nodeOrToken);
					break;
				default:
					throw new ArgumentException("Kind of node token here must be either "
						+ "OpenParenToken or CloseParenToken", "nodeOrToken");
			}
			return isRight ? null : new Error(CheckingUtility.GetLineNumber(nodeOrToken),
				_messageParens);
		}

		// these two below can be merged into one method with an enum, sure
		// but why

		/// <summary>
		/// Checks whether the char directly left of node's chars is some kind of
		/// delimiter.
		/// </summary>
		/// <param name="nodeOrToken">Node to check.</param>
		/// <returns>True if it is a delimiter to the left of nodeOrToken,
		/// false otherwise.</returns>
		private bool HasLeftDelimiter(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}

			var leftChar = nodeOrToken.SyntaxTree.GetText()[nodeOrToken.SpanStart - 1];

			return _delimeters.Contains(leftChar);
		}

		/// <summary>
		/// Checks whether the char directly right of node's chars is some kind of
		/// delimiter.
		/// </summary>
		/// <param name="nodeOrToken">Node to check.</param>
		/// <returns>True if it is a delimiter to the right of nodeOrToken,
		/// false otherwise.</returns>
		private bool HasRightDelimiter(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}
			// end is non-inclusive
			var rightChar = nodeOrToken.SyntaxTree.GetText()[nodeOrToken.Span.End];

			return _delimeters.Contains(rightChar);
		}

		/// <summary>
		/// Returns token that directly precedes given node. All trivia is ignored.
		/// </summary>
		/// <param name="nodeOrToken">Node to get previous token from.</param>
		/// <returns>Previous token.</returns>
		private SyntaxNodeOrToken GetPreviousToken(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}
			var text = nodeOrToken.SyntaxTree.GetText();
			var root = nodeOrToken.SyntaxTree.GetRoot();
			if (nodeOrToken == root.GetFirstToken())
			{
				throw new ArgumentException("Cannot get the previous token from the"
					+ " first token in a tree");
			}
			var index = nodeOrToken.FullSpan.Start - 1;
			// easier than i thought since trivia is a part of a previous token
			return root.FindToken(index);
		}
	}
}
