﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.Text;
using Microsoft.CodeAnalysis.CSharp;

using NtvpChecker.DataStructures;

namespace NtvpChecker.Checking.Rsdn.Rules
{
	/// <summary>
	/// Class that checks base lists of class declarations and their constructors for
	/// their RSDN compliance: newlines before colon only if base lists go beyond the
	/// 78th column.
	/// </summary>
	internal class BaseListRule : ISourceRule
	{
		/// <summary>
		/// Message to show when this rule is violated.
		/// </summary>
		private readonly string _message
			= ConfigurationManager.AppSettings["BaseListError"];

		/// <summary>
		/// Characters to remove from either side of second string end beyond
		/// when gluing to determine whether a newline was necessary.
		/// </summary>
		private readonly char[] _trimmers = new char[] { '\t', ' ', '\r', '\n' };

		// haha this is ugly

		/// <summary>
		/// Dictionary that maps checked kinds: class and constructor declarations
		/// to tuples of kinds used in checking: Item1 is the kind that holds the base
		/// list, if it's absent, there's no point to use this rule. Item2 is the first
		/// node that is *not* a part of declaration to limit string gluing. Item3 is the
		/// node that should have a newline in leading trivia if a newline is required.
		/// </summary>
		private readonly Dictionary<SyntaxKind, Tuple<SyntaxKind, SyntaxKind, SyntaxKind>>
			_checkConditions = new Dictionary<SyntaxKind, Tuple<SyntaxKind, SyntaxKind, SyntaxKind>>()
		{
			{
				SyntaxKind.ClassDeclaration,
				Tuple.Create(SyntaxKind.BaseList, SyntaxKind.OpenBraceToken,
					SyntaxKind.IdentifierToken)
			},
			{
				SyntaxKind.ConstructorDeclaration,
				Tuple.Create(SyntaxKind.BaseConstructorInitializer, SyntaxKind.Block,
					SyntaxKind.ParameterList)
			}
		};

		/// <summary>
		/// Method that actually checks usage of base lists.
		/// </summary>
		/// <param name="nodeOrToken">Syntax tree node to check,
		/// all nodes of a tree are passed.</param>
		/// <returns>List of detected errors.</returns>
		public List<Error> Check(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}

			var ret = new List<Error>();

			if (!_checkConditions.Keys.Contains(nodeOrToken.Kind())
				|| !nodeOrToken.ChildNodesAndTokens()
					.Any(x => x.Kind() == _checkConditions[nodeOrToken.Kind()].Item1))
			{
				return ret;
			}

			// that went way messier than i expected
			// should probably refactor that some day

			/*
			 * tl;dr: this takes relevant parts of a declaration with base list
			 * glues it to one string and checks whether it is above threshold
			 * to pass the check it should have either both length above 78 and a newline
			 * before colon, or none of these
			 */

			var text = nodeOrToken.SyntaxTree.GetText();
			// line number returned here is 1-indexed
			var index = CheckingUtility.GetLineNumber(nodeOrToken);
			// we grab everything from the first line, since there's probably some tabs
			var start = text.Lines[(int)index - 1].Span.Start;
			// and stop at a given node
			var end = nodeOrToken.ChildNodesAndTokens()
				.First(x => x.Kind() == _checkConditions[nodeOrToken.Kind()].Item2).SpanStart;

			var span = new TextSpan(start, end - start);
			var lines = text.GetSubText(span);

			var sb = new StringBuilder(lines.Lines[0].ToString());
			foreach (var line in lines.Lines.Skip(1))
			{
				var trimmed = line.ToString().Trim(_trimmers);
				sb.Append(trimmed);
			}

			var shouldBeMultiline = !CheckingUtility.IsLineLengthValid(sb.ToString());
			var isMultiLine = nodeOrToken.ChildNodesAndTokens()
				.First(x => x.Kind() == _checkConditions[nodeOrToken.Kind()].Item3)
				.GetTrailingTrivia().Any(x => x.Kind() == SyntaxKind.EndOfLineTrivia);

			if (shouldBeMultiline != isMultiLine)
			{
				var error = new Error(CheckingUtility.GetLineNumber(nodeOrToken), _message);
				ret.Add(error);
			}
			return ret;
		}
	}
}
