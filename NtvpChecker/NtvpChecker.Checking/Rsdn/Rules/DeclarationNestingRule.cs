﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;

using NtvpChecker.DataStructures;


namespace NtvpChecker.Checking.Rsdn.Rules
{
	/// <summary>
	/// Class that checks that nested declarations are only one level deep, and all
	/// declaration of nested types come first in a parent declaration with two lines
	/// separation. (Any comment counts as one line (probably))
	/// </summary>
	internal class DeclarationNestingRule : ISourceRule
	{
		/// <summary>
		/// Message to show when any condition of rule is violated.
		/// </summary>
		private readonly string _message
			= ConfigurationManager.AppSettings["DeclarationNestingError"];

		/// <summary>
		/// Kinds of declarations that can be nested inside class or struct.
		/// </summary>
		private readonly List<SyntaxKind> _nestedDeclarationKinds = new List<SyntaxKind>()
		{
			SyntaxKind.ClassDeclaration,
			SyntaxKind.StructDeclaration,
			SyntaxKind.EnumDeclaration,
			SyntaxKind.DelegateDeclaration,
			SyntaxKind.InterfaceDeclaration
		};

		/// <summary>
		/// Checks for rule violations.
		/// </summary>
		/// <param name="nodeOrToken">Syntax tree node to check,
		/// all nodes of a tree are passed.</param>
		/// <returns>List of detected errors.</returns>
		public  List<Error> Check(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}

			var ret = new List<Error>();
			// skip it if it's not a declaration or directly in a namespace
			if (!_nestedDeclarationKinds.Contains(nodeOrToken.Kind())
				|| nodeOrToken.Parent.Kind() == SyntaxKind.NamespaceDeclaration)
			{
				return ret;
			}

			var error = new Error(CheckingUtility.GetLineNumber(nodeOrToken), _message);

			// else if it's more than one level nesting, that's an error
			if (nodeOrToken.Parent.Parent != null
				&& nodeOrToken.Parent.Parent.Kind() != SyntaxKind.NamespaceDeclaration)
			{
				ret.Add(error);
				return ret;
			}
			// else check for double line separation
			var next = nodeOrToken.GetNextSibling();
			// if next node isn't two lines away, that's an error
			if (!HasEnoughLinesBefore(next, 2))
			{
				ret.Add(error);
				return ret;
			}
			// if any previous up to '{' isn't a declaration with two leading lines,
			// that's also an error
			var prev = nodeOrToken;
			while (prev.Kind() != SyntaxKind.OpenBraceToken)
			{
				if (!HasEnoughLinesBefore(prev, 2)
					|| !_nestedDeclarationKinds.Contains(prev.Kind()))
				{
					ret.Add(error);
					return ret;
				}
				prev = prev.GetPreviousSibling();
			}

			return ret;
		}

		/// <summary>
		/// Checks that a node has at least count of line breaks before it.
		/// </summary>
		/// <param name="nodeOrToken">Node to check.</param>
		/// <param name="count">Count of newlines to seek for.</param>
		/// <returns>True if there are at least count empty lines, false otherwise.</returns>
		private bool HasEnoughLinesBefore(SyntaxNodeOrToken nodeOrToken, uint count)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}
			return nodeOrToken.GetLeadingTrivia().Count(
				x => x.Kind() == SyntaxKind.EndOfLineTrivia) >= count;
		}
	}
}
