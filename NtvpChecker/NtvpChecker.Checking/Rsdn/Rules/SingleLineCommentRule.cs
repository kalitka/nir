﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;

using NtvpChecker.DataStructures;

namespace NtvpChecker.Checking.Rsdn.Rules
{
	/// <summary>
	/// Class that checks that single-line comments are on their own lines
	/// and start with "// ".
	/// </summary>
	internal class SingleLineCommentRule : ISourceRule
	{
		/// <summary>
		/// Message to show whe comment is badly formatted.
		/// </summary>
		private readonly string _message
			= ConfigurationManager.AppSettings["SingleLineCommentError"];


		/// <summary>
		/// Checks formatting of single-line comments.
		/// </summary>
		/// <param name="nodeOrToken">Syntax tree node to check,
		/// all nodes of a tree are passed.</param>
		/// <returns>List of detected errors.</returns>
		public  List<Error> Check(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}

			var ret = new List<Error>();

			if (!nodeOrToken.HasLeadingTrivia && !nodeOrToken.HasTrailingTrivia)
			{
				return ret;
			}

			// every comment on its own line belongs to next node's LeadingTrivia
			// everything in TrailingTrivia is an error
			if (nodeOrToken.HasTrailingTrivia)
			{
				var trailingComments = nodeOrToken.GetTrailingTrivia()
					.Where(x => x.Kind() == SyntaxKind.SingleLineCommentTrivia);
				foreach (var comment in trailingComments)
				{
					var error = new Error(CheckingUtility.GetLastLineNumber(nodeOrToken), _message);
					ret.Add(error);
				}
			}

			if (nodeOrToken.HasLeadingTrivia)
			{
				// then we just check whether it has a space after double slashes
				// because if it's in LeadingTrivia, it's already on a line of its own
				var comments = nodeOrToken.GetLeadingTrivia()
					.Where(x => x.Kind() == SyntaxKind.SingleLineCommentTrivia);

				foreach (var comment in comments)
				{
					if (!comment.ToFullString().StartsWith("// "))
					{
						var error = new Error(
							CheckingUtility.GetLineFromPosition(nodeOrToken.SyntaxTree,
							comment.SpanStart),
							_message);
						ret.Add(error);
					}
				}

			}
			return ret;
		}

	}
}
