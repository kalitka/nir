﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;

using NtvpChecker.DataStructures;

namespace NtvpChecker.Checking.Rsdn.Rules
{
	/// <summary>
	/// Class that detects whitespace chars at the end of the lines and files
	/// errors on them.
	/// </summary>
	internal class TrailingWhitespaceRule : ISourceRule
	{
		/// <summary>
		/// Message to show when rule is violated.
		/// </summary>
		private readonly string _message
			= ConfigurationManager.AppSettings["TrailingWhitespaceError"];

		/// <summary>
		/// Checks for redundant whitespace chars at the lines' ends.
		/// </summary>
		/// <param name="nodeOrToken">Syntax tree node to check,
		/// all nodes of a tree are passed.</param>
		/// <returns>List of detected errors.</returns>
		public List<Error> Check(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}

			var ret = new List<Error>();
			if (nodeOrToken.Kind() != SyntaxKind.CompilationUnit)
			{
				return ret;
			}

			var lines = nodeOrToken.SyntaxTree.GetText().Lines;

			for (int i = 0; i < lines.Count; i++)
			{
				var line = lines[i];

				// why bother with empty lines
				if (String.IsNullOrWhiteSpace(line.ToString()))
				{
					continue;
				}

				if (line.ToString().TrimEnd().Length < line.ToString().Length)
				{
					var error = new Error((uint)i + 1, _message);
					ret.Add(error);
				}
			}
			return ret;
		}
	}
}
