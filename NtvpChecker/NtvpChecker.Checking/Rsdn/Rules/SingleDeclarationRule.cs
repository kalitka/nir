﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using NtvpChecker.DataStructures;

namespace NtvpChecker.Checking.Rsdn.Rules
{
	/// <summary>
	/// Enforces declaration of a single variable with a single declaration statement.
	/// </summary>
	internal class SingleDeclarationRule : ISourceRule
	{
		/// <summary>
		/// Message to show when rule is violated.
		/// </summary>
		private readonly string _message
			= ConfigurationManager.AppSettings["SingleDeclarationError"];

		/// <summary>
		/// Checks every variable declaration: it must contain only VariableDeclarator.
		/// </summary>
		/// <param name="nodeOrToken">Syntax tree node to check,
		/// all nodes of a tree are passed.</param>
		/// <returns>List of detected errors.</returns>
		public  List<Error> Check(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}

			var ret = new List<Error>();
			if (nodeOrToken.Kind() != SyntaxKind.VariableDeclaration)
			{
				return ret;
			}

			if (nodeOrToken.ChildNodesAndTokens()
					.Count(x => x.Kind() == SyntaxKind.VariableDeclarator) != 1)
			{
				var error = new Error(CheckingUtility.GetLineNumber(nodeOrToken), _message);
				ret.Add(error);
			}

			return ret;
		}
	}
}