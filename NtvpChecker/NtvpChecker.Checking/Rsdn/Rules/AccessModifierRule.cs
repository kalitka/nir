﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using NtvpChecker.DataStructures;

namespace NtvpChecker.Checking.Rsdn.Rules
{
	/// <summary>
	/// Enforces rule that access modifiers are mandatory and go first in declaration.
	/// </summary>
	internal class AccessModifierRule : ISourceRule
	{
		/// <summary>
		/// Message to show when rule is violated.
		/// </summary>
		private readonly string _message
			= ConfigurationManager.AppSettings["AccessModifierError"];

		/// <summary>
		/// List of kinds of top-level declarations to check modifiers.
		/// </summary>
		private readonly List<SyntaxKind> _kindsToCheck = new List<SyntaxKind>()
		{
			SyntaxKind.ClassDeclaration,
			SyntaxKind.InterfaceDeclaration,
			SyntaxKind.StructDeclaration,
			SyntaxKind.DelegateDeclaration,
			SyntaxKind.EventDeclaration
		};

		/// <summary>
		/// List of access modifiers that can be first in declaration.
		/// </summary>
		private readonly List<SyntaxKind> _acceptedModifiers = new List<SyntaxKind>()
		{
			SyntaxKind.PublicKeyword,
			SyntaxKind.PrivateKeyword,
			SyntaxKind.ProtectedKeyword,
			SyntaxKind.InternalKeyword
		};

		/// <summary>
		/// Checks class, struct, interface, delegate, event declaration
		/// for access modifier as first modifier. Class and struct members are also checked.
		/// </summary>
		/// <param name="nodeOrToken">Syntax tree node to check,
		/// all nodes of a tree are passed.</param>
		/// <returns>List of detected errors.</returns>
		public List<Error> Check(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}

			var ret = new List<Error>();

			if (!_kindsToCheck.Contains(nodeOrToken.Kind()))
			{
				return ret;
			}

			// checks the top-level declaration
			ret.AddRange(CheckModifers(nodeOrToken));

			// if the node contains members with access modifiers
			// (e.g. it is a class or a struct), also check them
			if (nodeOrToken.Kind() == SyntaxKind.ClassDeclaration ||
				nodeOrToken.Kind() == SyntaxKind.StructDeclaration)
			{
				bool insideBraces = false;

				foreach (var childNode in nodeOrToken.ChildNodesAndTokens())
				{
					// skip everything until right after the { token
					if (!insideBraces)
					{
						if (childNode.Kind() == SyntaxKind.OpenBraceToken)
						{
							insideBraces = true;
						}

						continue;
					}

					// check everything after but the } itself
					if (childNode.Kind() != SyntaxKind.CloseBraceToken)
					{
						ret.AddRange(CheckModifers(childNode));
					}
				}
			}

			return ret;
		}

		/// <summary>
		/// Checks that node has an access modifier as its first modifier.
		/// </summary>
		/// <param name="nodeOrToken">Node to check.</param>
		/// <returns>List of Errors: empty if there is a modifier,
		/// one error if there isn't.</returns>
		private List<Error> CheckModifers(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}
			var ret = new List<Error>();
			if (!_acceptedModifiers.Contains(nodeOrToken.ChildNodesAndTokens()
				.SkipWhile(x => x.Kind() == SyntaxKind.AttributeList).ElementAt(0).Kind()))
			{
				var error = new Error(CheckingUtility.GetLineNumber(nodeOrToken), _message);
				ret.Add(error);
			}

			return ret;
		}
	}
}