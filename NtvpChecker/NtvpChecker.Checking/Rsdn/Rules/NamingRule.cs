﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;

using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

using NtvpChecker.DataStructures;

namespace NtvpChecker.Checking.Rsdn.Rules
{
	/// <summary>
	/// Incapsulates various naming checks, as defined throughout RSDN.
	/// </summary>
	internal class NamingRule : ISourceRule
	{
		/// <summary>
		/// Enum representing different case styles allowed by RSDN.
		/// </summary>
		private enum CaseType
		{
			/// <summary>
			/// PascalCase is every word from capital letter.
			/// </summary>
			PascalCase,
			/// <summary>
			/// camelCase is every word except first from capital letter.
			/// </summary>
			CamelCase
			// In either case, no non-letter delimeters allowed.
		}


		/// <summary>
		/// Message to show when this rule is violated.
		/// </summary>
		private readonly string _message =
			ConfigurationManager.AppSettings["NamingError"];

		/// <summary>
		/// List of C# (and VB) keywords that are 'not recommended' as identifiers
		/// accroding to RSDN. 
		/// </summary>
		private readonly List<String> _keywords = new List<string>
		{
			// converted to lowercase so this isn't done every single comparison
			"addhandler",
			"addressof",
			"alias",
			"and",
			"ansi",
			"as",
			"assembly",
			"auto",
			"base",
			"boolean",
			"byref",
			"byte",
			"byval",
			"call",
			"case",
			"catch",
			"cbool",
			"cbyte",
			"cchar",
			"cdate",
			"cdec",
			"cdbl",
			"char",
			"cint",
			"class",
			"clng",
			"cobj",
			"const",
			"cshort",
			"csng",
			"cstr",
			"ctype",
			"date",
			"decimal",
			"declare",
			"default",
			"delegate",
			"dim",
			"do",
			"double",
			"each",
			"else",
			"elseif",
			"end",
			"enum",
			"erase",
			"error",
			"eval",
			"event",
			"exit",
			"extends",
			"externalsource",
			"false",
			"finalize",
			"finally",
			"float",
			"for",
			"friend",
			"function",
			"get",
			"gettype",
			"goto",
			"handles",
			"if",
			"implements",
			"imports",
			"in",
			"inherits",
			"instanceof",
			"integer",
			"interface",
			"is",
			"let",
			"lib",
			"like",
			"long",
			"loop",
			"me",
			"mod",
			"module",
			"mustinherit",
			"must",
			"mybase",
			"myclass",
			"namespace",
			"new",
			"next",
			"not",
			"nothing",
			"notinheritable",
			"notoverridable",
			"object",
			"on",
			"option",
			"optional",
			"or",
			"overloads",
			"overridable",
			"package",
			"paramarray",
			"preserve",
			"private",
			"property",
			"protected",
			"public",
			"raiseevent",
			"readonly",
			"redim",
			"region",
			"rem",
			"removehandler",
			"resume",
			"return",
			"select",
			"set",
			"shadows",
			"shared",
			"short",
			"single",
			"static",
			"step",
			"stop",
			"string",
			"structure",
			"sub",
			"synclock",
			"then",
			"throw",
			"to",
			"true",
			"try",
			"typeof",
			"unicode",
			"until",
			"var",
			"volatile",
			"when",
			"while",
			"with",
			"withevents",
			"writeonly",
			"xor"
		};

		/// <summary>
		/// Checks whether supplied string is in provided case.
		/// </summary>
		/// <param name="caseType">Case type to look for.</param>
		/// <param name="identifier">String to check.</param>
		/// <returns>True if string is in required case, false otherwise.</returns>
		private static bool CheckCase(CaseType caseType, string identifier)
		{
			if (identifier == null)
			{
				throw new ArgumentNullException("identifier");
			}

			string regex;
			switch (caseType)
			{
				case CaseType.PascalCase:
					regex = "^[A-Z][a-zA-Z]*$";
					break;
				case CaseType.CamelCase:
					regex = "^[a-z][a-zA-Z]*$";
					break;
				// there's two types, but whatever makes msbuild happy
				default:
					throw new ApplicationException("This should literally never happen.");
			}
			return Regex.IsMatch(identifier, regex, RegexOptions.Compiled);
		}

		/// <summary>
		/// Checks whether provided identifier is a keyword from a defined list.
		/// </summary>
		/// <param name="identifer">Identifier to check.</param>
		/// <returns>True if it is a keyword, false otherwise.</returns>
		private bool IsKeyword(string identifer)
		{
			return _keywords.Contains(identifer.ToLower());
		}

		/// <summary>
		/// Checks whether provided strings contains too many (4+)
		/// capital letters in a row.
		/// </summary>
		/// <param name="identifier">String to check.</param>
		/// <returns>True if there is too many capital letters, false otherwise.</returns>
		private static bool IsTooCapital(string identifier)
		{
			if (identifier == null)
			{
				throw new ArgumentNullException("identifier");
			}
			var regex = new Regex("[A-Z]{4,}", RegexOptions.Compiled);
			return regex.IsMatch(identifier);
		}

		/// <summary>
		/// Calls for backend function to check node for various naming rules.
		/// </summary>
		/// <param name="nodeOrToken">Syntax tree node to check, all nodes of a tree
		/// are passed.</param>
		/// <returns>List of detected errors.</returns>
		public  List<Error> Check(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}

			var ret = new List<Error>();
			// namespaces being namespaces
			if (nodeOrToken.Kind() == SyntaxKind.NamespaceDeclaration)
			{
				ret.AddRange(CheckNamespaceDeclaration(nodeOrToken));
			}
			else
			{
				var result = InternalCheck(nodeOrToken);
				if (result != null)
				{
					ret.Add(result);
				}
			}
			return ret;
		}

		/// <summary>
		/// Internal function to check for naming rules.
		/// </summary>
		/// <param name="nodeOrToken">Node to check.</param>
		/// <returns>Null if there no errors, one Error per node if there is.</returns>
		private Error InternalCheck(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}

			// that's no longer worse than bad
			// i rate this 'could be better out of ten'

			// identifier extraction, other node kinds filtering
			switch (nodeOrToken.Kind())
			{
				case SyntaxKind.ClassDeclaration:
					return CheckClassDeclaration(nodeOrToken);
				case SyntaxKind.InterfaceDeclaration:
					return CheckInterfaceDeclaration(nodeOrToken);
				case SyntaxKind.EnumDeclaration:
					return CheckEnumDeclaration(nodeOrToken);
				case SyntaxKind.EnumMemberDeclaration:
					return CheckEnumMemeberDeclaration(nodeOrToken);
				case SyntaxKind.VariableDeclarator:
					return CheckVariableDeclarator(nodeOrToken);
				case SyntaxKind.Parameter:
					return CheckParameter(nodeOrToken);
				// this is an intentional fallthrough
				case SyntaxKind.DelegateDeclaration:
				case SyntaxKind.PropertyDeclaration:
				case SyntaxKind.MethodDeclaration:
				case SyntaxKind.StructDeclaration:
					return CheckGeneric(nodeOrToken);
				default:
					return null;
			}
		}

		/// <summary>
		/// Checks for three identifier criteria: it should not:
		/// be a keyword; contain 4+ capital letters in a row;
		/// be not in case specified.
		/// </summary>
		/// <param name="caseType">Case type identifier should be in.</param>
		/// <param name="identifier">Identifier to check.</param>
		/// <returns>True if any of criteria is failed, false otherwise.</returns>
		private bool IsIdentifierBad(CaseType caseType, string identifier)
		{
			return IsKeyword(identifier) || IsTooCapital(identifier) ||
				!CheckCase(caseType, identifier);
		}

		/// <summary>
		/// Checks parameters for camel case and other generic criteria.
		/// </summary>
		/// <param name="nodeOrToken">Node to check. Should have kind of
		/// SyntaxKind.Parameter.</param>
		/// <returns>Error detected or null.</returns>
		private Error CheckParameter(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}
			if (nodeOrToken.Kind() != SyntaxKind.Parameter)
			{
				throw new ArgumentException("Syntax node is of wrong kind, should be"
					+ " SyntaxKind.Parameter", "nodeOrToken");
			}

			var parameter = (ParameterSyntax)nodeOrToken;
			var identifier = parameter.Identifier.ToString();

			if (IsIdentifierBad(CaseType.CamelCase, identifier))
			{
				return GetError(nodeOrToken);
			}
			return null;
		}

		/// <summary>
		/// Check variable declarators for various creteria
		/// depending on its position in the source tree
		/// </summary>
		/// <param name="nodeOrToken">Node to check. Should have kind of
		/// SyntaxKind.VariableDeclarator.</param>
		/// <returns>Error detected or null.</returns>
		private Error CheckVariableDeclarator(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}
			if (nodeOrToken.Kind() != SyntaxKind.VariableDeclarator)
			{
				throw new ArgumentException("Syntax node is of wrong kind, should be"
					+ " SyntaxKind.VariableDeclarator", "nodeOrToken");
			}

			var varDecl = (VariableDeclaratorSyntax)nodeOrToken;
			var identifier = varDecl.Identifier.ToString();

			var error = GetError(nodeOrToken);

			var grandparent = varDecl.Parent.Parent;

			CaseType checker;
			// type is determined from granparent node
			switch (grandparent.Kind())
			{
				// local variables are camel case
				case SyntaxKind.LocalDeclarationStatement:
					checker = CaseType.CamelCase;
					break;
				// event fields are pascal case
				case SyntaxKind.EventFieldDeclaration:
					checker = CaseType.PascalCase;
					break;
				// public fields are pascal case, private are camel case starting with '_'
				case SyntaxKind.FieldDeclaration:
					var fieldDecl = (FieldDeclarationSyntax)grandparent;

					var isPublic = fieldDecl.Modifiers.Count(x => x.ToString() == "public"
						|| x.ToString() == "internal") > 0;

					checker = isPublic ? CaseType.PascalCase : CaseType.CamelCase;

					if (!isPublic)
					{
						// check for first '_' and discard it to check the rest later
						if (identifier[0] != '_')
						{
							return error;
						}
						identifier = identifier.Substring(1);
					}
					break;
				// skip since it is reusing already defined name
				default:
					return null;
			}

			return IsIdentifierBad(checker, identifier) ? error : null;
		}

		/// <summary>
		/// Checks enum declaration for pascal case and not containing "Enum" in its end.
		/// </summary>
		/// <param name="nodeOrToken">Node to check. Should have kind of
		/// SyntaxKind.EnumDeclaration.</param>
		/// <returns>Error detected or null.</returns>
		private Error CheckEnumDeclaration(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}
			if (nodeOrToken.Kind() != SyntaxKind.EnumDeclaration)
			{
				throw new ArgumentException("Syntax node is of wrong kind, should be"
					+ " SyntaxKind.EnumDeclaration", "nodeOrToken");
			}

			var enumDecl = (EnumDeclarationSyntax)nodeOrToken;
			var identifier = enumDecl.Identifier.ToString();

			if (IsIdentifierBad(CaseType.PascalCase, identifier)
				|| identifier.EndsWith("Enum"))
			{
				return GetError(nodeOrToken);
			}
			return null;
		}

		/// <summary>
		/// Checks enum members for pascal case and not containing parent enum's name.
		/// </summary>
		/// <param name="nodeOrToken">Node to check. Should have kind of
		/// SyntaxKind.EnumMemberDeclaration.</param>
		/// <returns>Error detected or null.</returns>
		private Error CheckEnumMemeberDeclaration(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}
			if (nodeOrToken.Kind() != SyntaxKind.EnumMemberDeclaration)
			{
				throw new ArgumentException("Syntax node is of wrong kind, should be"
					+ " SyntaxKind.EnumMemberDeclaration", "nodeOrToken");
			}

			var enumMemberDecl = (EnumMemberDeclarationSyntax)nodeOrToken;
			var identifier = enumMemberDecl.Identifier.ToString();

			var parentIdentifer = ((EnumDeclarationSyntax)enumMemberDecl.Parent)
				.Identifier.ToString();

			if (IsIdentifierBad(CaseType.PascalCase, identifier)
				|| identifier.IndexOf(parentIdentifer) != -1)
			{
				return GetError(nodeOrToken);
			}
			return null;
		}

		/// <summary>
		/// Checks interfaces for pascal case and first 'I' char.
		/// </summary>
		/// <param name="nodeOrToken">Node to check. Should have kind of
		/// SyntaxKind.InterfaceDeclaration.</param>
		/// <returns>Error detected or null.</returns>
		private Error CheckInterfaceDeclaration(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}
			if (nodeOrToken.Kind() != SyntaxKind.InterfaceDeclaration)
			{
				throw new ArgumentException("Syntax node is of wrong kind, should be"
					+ " SyntaxKind.InterfaceDeclaration", "nodeOrToken");
			}

			var interfaceDecl = (InterfaceDeclarationSyntax)nodeOrToken;
			var identifier = interfaceDecl.Identifier.ToString();

			if (IsIdentifierBad(CaseType.PascalCase, identifier)
				|| identifier[0] != 'I')
			{
				return GetError(nodeOrToken);
			}
			return null;
		}

		/// <summary>
		/// Checks classes for pascal case and not having a 'C' prefix.
		/// Abstract classes should end with 'Base' also.
		/// </summary>
		/// <param name="nodeOrToken">Node to check. Should have kind of
		/// SyntaxKind.ClassDeclaration.</param>
		/// <returns>Error detected or null.</returns>
		private Error CheckClassDeclaration(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}
			if (nodeOrToken.Kind() != SyntaxKind.ClassDeclaration)
			{
				throw new ArgumentException("Syntax node is of wrong kind, should be"
					+ " SyntaxKind.ClassDeclaration", "nodeOrToken");
			}

			var classDecl = (ClassDeclarationSyntax)nodeOrToken;
			var identifier = classDecl.Identifier.ToString();

			var error = GetError(nodeOrToken);

			var isAbstract = classDecl.Modifiers.Count(x => x.ToString() == "abstract") > 0;

			if (isAbstract && !identifier.EndsWith("Base"))
			{
				return error;
			}
			if (identifier[0] == 'C' && Char.IsUpper(identifier[1]))
			{
				return error;
			}
			if (IsIdentifierBad(CaseType.PascalCase, identifier))
			{
				return error;
			}
			return null;
		}

		/// <summary>
		/// Checks all part of a namespace declaration for pascal case.
		/// Also checks that child declarations do not reuse its name.
		/// </summary>
		/// <param name="nodeOrToken">Node to check. Should have kind of
		/// SyntaxKind.NamespaceDeclaration.</param>
		/// <returns>List of errors detected. Might be empty.</returns>
		private List<Error> CheckNamespaceDeclaration(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}
			if (nodeOrToken.Kind() != SyntaxKind.NamespaceDeclaration)
			{
				throw new ArgumentException("Syntax node is of wrong kind, should be"
					+ " SyntaxKind.NamespaceDeclaration", "nodeOrToken");
			}

			var ret = new List<Error>();

			var namespaceDecl = (NamespaceDeclarationSyntax)nodeOrToken;
			var identifiers = namespaceDecl.Name.ToString().Split('.');

			foreach (var identifier in identifiers)
			{
				if (IsIdentifierBad(CaseType.PascalCase, identifier))
				{
					ret.Add(GetError(nodeOrToken));
				}
			}

			var declarations = CheckingUtility.GetInnerDeclarations(nodeOrToken);
			foreach (var decl in declarations)
			{
				var identifier = CheckingUtility.GetIdentifier(decl);
				foreach (var namespaceNamePart in identifiers)
				{
					if (identifier == namespaceNamePart)
					{
						ret.Add(GetError(decl));
						break;
					}
				}
			}
			return ret;
		}

		/// <summary>
		/// Checks other declarations of various kinds for pascal case.
		/// </summary>
		/// <param name="nodeOrToken">Node to check.</param>
		/// <returns>Error detected or null.</returns>
		private Error CheckGeneric(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}
			var identifier = CheckingUtility.GetIdentifier(nodeOrToken);
			if (IsIdentifierBad(CaseType.PascalCase, identifier))
			{
				return GetError(nodeOrToken);
			}
			return null;
		}

		/// <summary>
		/// Helper method to simplify Error creation, since message is always the same.
		/// </summary>
		/// <param name="nodeOrToken">Node to get line number from.</param>
		/// <returns>Error with nodeOrTokens's line number and class' message.</returns>
		private Error GetError(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}
			return new Error(CheckingUtility.GetLineNumber(nodeOrToken), _message);
		}
	}
}