﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;

using NtvpChecker.DataStructures;

namespace NtvpChecker.Checking.Rsdn.Rules
{
	/// <summary>
	/// Class that check that statements are placed on separate lines.
	/// </summary>
	internal class StatementLinesRule : ISourceRule
	{
		/// <summary>
		/// Message to show when this rule is violated.
		/// </summary>
		private readonly string _message
			= ConfigurationManager.AppSettings["StatementLinesError"];

		/// <summary>
		/// List of kind this rule applies to. Probably something is missing.
		/// </summary>
		private readonly List<SyntaxKind> _kindsToCheck = new List<SyntaxKind>()
		{
			SyntaxKind.ReturnStatement,
			SyntaxKind.LocalDeclarationStatement,
			SyntaxKind.ExpressionStatement,
			SyntaxKind.BreakStatement,
			SyntaxKind.ContinueStatement,
			SyntaxKind.GotoStatement,
			SyntaxKind.ThrowStatement,
			SyntaxKind.CaseSwitchLabel
		};

		/// <summary>
		/// Checks whether nodes of provided kinds have newline both before and after them.
		/// </summary>
		/// <param name="nodeOrToken">Syntax tree node to check,
		/// all nodes of a tree are passed.</param>
		/// <returns>List of detected errors.</returns>
		public  List<Error> Check(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}

			var ret = new List<Error>();
			if (!_kindsToCheck.Contains(nodeOrToken.Kind()))
			{
				return ret;
			}

			// CaseSwitchLabel is a special snowflake that is a first child of its parent
			// and therefore has no previous sibling, which is by the way indicated not
			// by null but by a SyntaxNodeOrToken with everything set to 0 or null
			// so we need to check previous sibling of its parent
			var otherNode = nodeOrToken.Kind() == SyntaxKind.CaseSwitchLabel ?
				((SyntaxNodeOrToken)nodeOrToken.Parent).GetPreviousSibling() :
				nodeOrToken.GetPreviousSibling();

			// both before and after to be sure
			if (!(HasTrailingNewline(nodeOrToken) &&
				HasTrailingNewline(otherNode)))
			{
				var error = new Error(CheckingUtility.GetLineNumber(nodeOrToken), _message);
				ret.Add(error);
			}

			return ret;
		}

		/// <summary>
		/// Returns whether given node or token has newlines after it.
		/// </summary>
		/// <param name="nodeOrToken">Node to check.</param>
		/// <returns>True if there is at least one newline after the node or token,
		/// false otherwise.</returns>
		private bool HasTrailingNewline(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}
			return nodeOrToken.GetTrailingTrivia()
				.Count(x => x.Kind() == SyntaxKind.EndOfLineTrivia) > 0;
		}
	}
}
