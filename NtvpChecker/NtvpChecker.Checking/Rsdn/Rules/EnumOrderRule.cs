﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using NtvpChecker.DataStructures;

namespace NtvpChecker.Checking.Rsdn.Rules
{
	/// <summary>
	/// Description of EnumOrderRule.
	/// </summary>
	internal class EnumOrderRule : ISourceRule
	{
		/// <summary>
		/// Message to show when rule is violated.
		/// </summary>
		private readonly string _message
			= ConfigurationManager.AppSettings["EnumOrderError"];

		/// <summary>
		/// Checks that members in enum are in alphabetical order.
		/// </summary>
		/// <param name="nodeOrToken">Syntax tree node to check,
		/// all nodes of a tree are passed.</param>
		/// <returns>List of detected errors.</returns>
		public  List<Error> Check(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}

			var ret = new List<Error>();
			if (nodeOrToken.Kind() != SyntaxKind.EnumDeclaration)
			{
				return ret;
			}

			var rawList = nodeOrToken.ChildNodesAndTokens()
				.Where(x => x.Kind() == SyntaxKind.EnumMemberDeclaration);

			var unsorted = new List<string>();
			foreach (var member in rawList)
			{
				unsorted.Add(((EnumMemberDeclarationSyntax) member).Identifier.ToString());
			}

			var sorted = new List<string>(unsorted);
			sorted.Sort();

			for (int i = 0; i < sorted.Count; i++)
			{
				if (unsorted[i] != sorted[i])
				{
					var error = new Error(CheckingUtility.GetLineNumber(nodeOrToken), _message);
					ret.Add(error);
					break;
				}
			}

			return ret;
		}
	}
}