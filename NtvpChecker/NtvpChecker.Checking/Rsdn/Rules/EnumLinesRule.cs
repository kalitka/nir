﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using NtvpChecker.DataStructures;

namespace NtvpChecker.Checking.Rsdn.Rules
{
	/// <summary>
	/// Enforces rule that every enum item must be on its own string.
	/// </summary>
	internal class EnumLinesRule : ISourceRule
	{
		/// <summary>
		/// Message to show when rule is violated.
		/// </summary>
		private readonly string _message = ConfigurationManager.AppSettings["EnumLinesError"];

		/// <summary>
		/// Checks every enum declaration: every comma inside bust be followed by a newline.
		/// </summary>
		/// <param name="nodeOrToken">Syntax tree node to check,
		/// all nodes of a tree are passed.</param>
		/// <returns>List of detected errors.</returns>
		public  List<Error> Check(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}

			var ret = new List<Error>();
			if (nodeOrToken.Kind() != SyntaxKind.EnumDeclaration)
			{
				return ret;
			}

			foreach (var commaToken in nodeOrToken.ChildNodesAndTokens()
				.Where(x => x.Kind() == SyntaxKind.CommaToken))
			{
				if (commaToken.GetTrailingTrivia()
						.Count(x => x.Kind() == SyntaxKind.EndOfLineTrivia) != 1)
				{
					var error = new Error(CheckingUtility.GetLineNumber(commaToken), _message);
					ret.Add(error);
				}
			}

			return ret;
		}
	}
}