﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;

using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

using NtvpChecker.DataStructures;

namespace NtvpChecker.Checking.Rsdn.Rules
{
	/// <summary>
	/// Incapsulates various XML-comment related checks. tl;dr: everything declared
	/// must have summary tag, methods, delegates and constructors may have param tags
	/// and/or returns on non-constructor non-void stuff.
	/// </summary>
	internal class XmlCommentRule: ISourceRule
	{
		/// <summary>
		/// Message to show when a XML-comment is missing or malformed.
		/// </summary>
		private readonly string _message =
			ConfigurationManager.AppSettings["XmlCommentError"];
		
		/// <summary>
		/// List of syntax nodes whose kinds end in "Declaration" yet they shouldn't
		/// be checked. Most likely not final.
		/// </summary>
		private readonly List<SyntaxKind> _declarationsToSkip = new List<SyntaxKind>()
		{
			SyntaxKind.NamespaceDeclaration, SyntaxKind.VariableDeclaration,
			SyntaxKind.SetAccessorDeclaration, SyntaxKind.GetAccessorDeclaration,
			SyntaxKind.CatchDeclaration, SyntaxKind.AddAccessorDeclaration
		};
		
		/// <summary>
		/// Symbols that are not comment text but are in summary text.
		/// </summary>
		private readonly char[] _trimmers = new char[] {'\t', ' ', '/', '\r', '\n'};
		
		/// <summary>
		/// Calls for backend function to check node for various XML commenting rules.
		/// </summary>
		/// <param name="nodeOrToken">Syntax tree node to check, all nodes of a tree
		/// are passed.</param>
		/// <returns>List of detected errors.</returns>
		public  List<Error> Check(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}
			// copy-paste is bad
			var ret = new List<Error>();
			var result = InternalCheck(nodeOrToken);
			if (result != null)
			{
				ret.Add(result);
			}
			return ret;
		}
		
		/// <summary>
		/// Backend method to check for XML comment rules.
		/// Calls three separate helper methods.
		/// </summary>
		/// <param name="nodeOrToken">Node to check.</param>
		/// <returns>Null if there no errors, one Error per node if there is.</returns>
		private Error InternalCheck(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}
			// this is something
			if (nodeOrToken.Kind().ToString().EndsWith("Declaration"))
			{
				// some declarations are exempt from checking
				if (_declarationsToSkip.Contains(nodeOrToken.Kind()))
				{
					return null;
				}
				// all checks must be passed
				if (CheckSummary(nodeOrToken) && CheckReturns(nodeOrToken)
					&& CheckParameters(nodeOrToken))
				{
					return null;
				}
				return new Error(CheckingUtility.GetLineNumber(nodeOrToken), _message);
			}
			return null;
		}

		/// <summary>
		/// Gets XML elements represeting elements in a XML comment from a node.
		/// </summary>
		/// <param name="nodeOrToken">Node to get XML comment elements from.</param>
		/// <returns>Possibly empty enumerable of XmlElementSyntax.</returns>
		private IEnumerable<XmlElementSyntax> GetXmlElements(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}

			SyntaxNodeOrToken commentNode;
			try
			{
				commentNode = nodeOrToken.GetLeadingTrivia()
					.First(x => x.Kind() == SyntaxKind.SingleLineDocumentationCommentTrivia)
					.GetStructure();
			}
			catch (InvalidOperationException)
			{
				return new List<XmlElementSyntax>().AsEnumerable();
			}
			var elementNodes = commentNode.ChildNodesAndTokens()
				.Where(x => x.Kind() == SyntaxKind.XmlElement);

			var ret = new List<XmlElementSyntax>();

			foreach (var node in elementNodes)
			{
				ret.Add((XmlElementSyntax)node);
			}

			return ret.AsEnumerable();
		}

		/// <summary>
		/// Retrieves text from a tag that is unique to the comment
		/// like summary or returns.
		/// </summary>
		/// <param name="nodeOrToken">Node that has comments.</param>
		/// <param name="tag">Name of tag to retrieve text from.</param>
		/// <returns>Text from the tag or null if it's not present
		/// or is found more than one.</returns>
		private string GetUnqiueTagValue(SyntaxNodeOrToken nodeOrToken, string tag)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}
			if (tag == null)
			{
				throw new ArgumentNullException("tag");
			}
			if (tag.Length == 0)
			{
				throw new ArgumentException("Tag name should not be empty", "tag");
			}

			string ret = null;

			foreach (var xmlElement in GetXmlElements(nodeOrToken))
			{
				if (xmlElement.StartTag.Name.ToString() == tag)
				{
					if (ret == null)
					{
						ret = xmlElement.Content.ToString().Trim(_trimmers);
					}
					// multiple occurences are not ok
					else
					{
						return null;
					}
				}
			}
			return ret;
		}
		
		/// <summary>
		/// Returns a list of names of parameters that are documented in node's
		/// XML comment.
		/// </summary>
		/// <param name="nodeOrToken">Node to get paramters for.</param>
		/// <returns>Possibly empty list of docmented parameter names.</returns>
		private List<string> GetCommentedParameters(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}

			var ret = new List<string>();
			
			foreach (var xmlElement in GetXmlElements(nodeOrToken))
			{
				if (xmlElement.StartTag.Name.ToString() == "param")
				{
					// this is kinda dumb, but it works
					var match = Regex.Match(xmlElement.StartTag.Attributes.ToString(),
						"name=\"(.*)\"");
					// if there is non-empty param tag with a name,
					// we consider that name documented
					if (match.Success && match.Groups.Count == 2
						&& xmlElement.Content.ToString().Trim(_trimmers).Length > 0)
					{
						// zero group is the entire matched string
						ret.Add(match.Groups[1].Value);
					}
				}
			}
			return ret;
		}

		/// <summary>
		/// Gets actual return type of method or delegate declaration as a string.
		/// Empty string is returned for other node kinds.
		/// </summary>
		/// <param name="nodeOrToken">Node to check.</param>
		/// <returns>Return type as a string or empty string
		/// if node doesn't have one</returns>
		private string GetActualReturnType(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}

			dynamic nodeWithReturnType;
			switch(nodeOrToken.Kind())
			{
				case SyntaxKind.MethodDeclaration:
					nodeWithReturnType = (MethodDeclarationSyntax)nodeOrToken;
					break;
				case SyntaxKind.DelegateDeclaration:
					nodeWithReturnType = (DelegateDeclarationSyntax)nodeOrToken;
					break;
				default:
					return String.Empty;
			}
			return nodeWithReturnType.ReturnType.ToString();
		}

		/// <summary>
		/// Gets actual parameters list from method, delegate, or constructor declaration.
		/// Empty list is returned for other node kinds.
		/// </summary>
		/// <param name="nodeOrToken">Node to check.</param>
		/// <returns>Possible empty list of actually defined paramters as strings.</returns>
		private List<string> GetActualParameters(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}

			var ret = new List<string>();
			dynamic nodeWithParams;
			switch (nodeOrToken.Kind())
			{
				case SyntaxKind.MethodDeclaration:
					nodeWithParams = (MethodDeclarationSyntax)nodeOrToken;
					break;
				case SyntaxKind.DelegateDeclaration:
					nodeWithParams = (DelegateDeclarationSyntax)nodeOrToken;
					break;
				case SyntaxKind.ConstructorDeclaration:
					nodeWithParams = (ConstructorDeclarationSyntax)nodeOrToken;
					break;
				default:
					return ret;
			}
			foreach (var param in nodeWithParams.ParameterList.Parameters)
			{
				ret.Add(param.Identifier.ToString());
			}
			return ret;
		}

		/// <summary>
		/// Checks that summary is present and contains text.
		/// </summary>
		/// <param name="nodeOrToken">Node to check.</param>
		/// <returns>True is summary is present with a text, false otherwise.</returns>
		private bool CheckSummary(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}

			var s = GetUnqiueTagValue(nodeOrToken, "summary");
			if (s == null || s.Length == 0)
			{
				return false;
			}
			return true;
		}

		/// <summary>
		/// Checks returns tag for correctness.
		/// </summary>
		/// <param name="nodeOrToken">Node to check.</param>
		/// <returns>True if check passed and everything is ok, false otherwise.</returns>
		private bool CheckReturns(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}

			var returns = GetUnqiueTagValue(nodeOrToken, "returns");
			var hasReturns = (returns != null && returns.Length > 0);
			switch (nodeOrToken.Kind())
			{
				case SyntaxKind.MethodDeclaration:
				case SyntaxKind.DelegateDeclaration:
					var isVoid = GetActualReturnType(nodeOrToken) == "void";
					return hasReturns != isVoid;
				default:
					return !hasReturns;
			}
		}

		/// <summary>
		/// Checks that param list from comments matches actual list,
		/// also checks whether params should be there.
		/// </summary>
		/// <param name="nodeOrToken">Node to check.</param>
		/// <returns>True if check passed, false otherwise.</returns>
		private bool CheckParameters(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}

			var pars = GetCommentedParameters(nodeOrToken);
			switch (nodeOrToken.Kind())
			{
				case SyntaxKind.MethodDeclaration:
				case SyntaxKind.DelegateDeclaration:
				case SyntaxKind.ConstructorDeclaration:
					var actual = GetActualParameters(nodeOrToken);
					// this is a check for equality between two lists
					return actual.All(pars.Contains) && actual.Count == pars.Count;
				default:
					return pars.Count == 0;
			}
		}
	}
}
