﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;

using NtvpChecker.DataStructures;

namespace NtvpChecker.Checking.Rsdn.Rules
{
	/// <summary>
	/// Class that checks that every single declaration inside class of struct is
	/// separated from others by a newline and/or comment of any kind.
	/// </summary>
	internal class DeclarationSpacingRule : ISourceRule
	{
		/// <summary>
		/// Message to show when the rule is violated.
		/// </summary>
		private readonly string _message
			= ConfigurationManager.AppSettings["DeclarationSpacingError"];

		/// <summary>
		/// Checks that everything in class or struct declaration between { and }
		/// tokens has newline and/or comment in its LeadingTrivia.
		/// </summary>
		/// <param name="nodeOrToken">Syntax tree node to check,
		/// all nodes of a tree are passed.</param>
		/// <returns>List of detected errors.</returns>
		public  List<Error> Check(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}

			var ret = new List<Error>();
			if (! (nodeOrToken.Kind() == SyntaxKind.ClassDeclaration
				|| nodeOrToken.Kind() == SyntaxKind.StructDeclaration))
			{
				return ret;
			}

			// all decarations are between { and } tokens
			var declarations = CheckingUtility.GetInnerDeclarations(nodeOrToken);
			// first one is exempt from checking
			foreach (var declaration in declarations.Skip(1))
			{
				// wow
				if (declaration.GetLeadingTrivia()
					.Count(x => x.Kind() == SyntaxKind.EndOfLineTrivia
						|| x.Kind() == SyntaxKind.SingleLineDocumentationCommentTrivia
						|| x.Kind() == SyntaxKind.SingleLineCommentTrivia) == 0)
				{
					var error = new Error(CheckingUtility.GetLineNumber(declaration), _message);
					ret.Add(error);
				}
			}
			return ret;
		}


	}
}
