﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;

using NtvpChecker.DataStructures;

namespace NtvpChecker.Checking.Rsdn.Rules
{
	/// <summary>
	/// Class that checks for usage of blocks of code even when they're
	/// optional by design.
	/// </summary>
	internal class BlockUsageRule : ISourceRule
	{
		/// <summary>
		/// Message to show when block was not detected.
		/// </summary>
		private readonly string _message
			= ConfigurationManager.AppSettings["BlockUsageError"];

		/// <summary>
		/// List of SyntaxKinds this rule applies to.
		/// </summary>
		private readonly List<SyntaxKind> _kindsToCheck = new List<SyntaxKind>()
		{
			SyntaxKind.ForStatement,
			SyntaxKind.ForEachStatement,
			SyntaxKind.IfStatement,
			SyntaxKind.DoStatement,
			SyntaxKind.WhileStatement,
			SyntaxKind.UsingStatement,
			SyntaxKind.ElseClause
		};

		/// <summary>
		/// Checks whether nodes of kinds stated in _kindsToCheck have a {} block of code
		/// after a ')' token (or 'else' keyword) that denotes end of its condition or whatever.
		/// Also usings can be nested.
		/// </summary>
		/// <param name="nodeOrToken">Syntax tree node to check,
		/// all nodes of a tree are passed.</param>
		/// <returns>List of detected errors.</returns>
		public List<Error> Check(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}

			var ret = new List<Error>();
			if (!_kindsToCheck.Contains(nodeOrToken.Kind()))
			{
				return ret;
			}

			// haha this is ugly

			var kindToLook = nodeOrToken.Kind() == SyntaxKind.ElseClause ?
				SyntaxKind.ElseKeyword :
				SyntaxKind.CloseParenToken;

			var paren = nodeOrToken.ChildNodesAndTokens()
				.First(x => x.Kind() == kindToLook);
			var parenIndex = nodeOrToken.ChildNodesAndTokens().ToList().IndexOf(paren);

			var nested = nodeOrToken.ChildNodesAndTokens().ElementAt(parenIndex + 1);

			if (!(nested.Kind() == SyntaxKind.Block
				// usings can be nested
				|| (nodeOrToken.Kind() == SyntaxKind.UsingStatement
					&& nested.Kind() == SyntaxKind.UsingStatement)
				// else might be followed by another if statement
				|| nodeOrToken.Kind() == SyntaxKind.ElseClause
					&& nested.Kind() == SyntaxKind.IfStatement))
			{
				var error = new Error(CheckingUtility.GetLineNumber(nodeOrToken), _message);
				ret.Add(error);
			}

			return ret;
		}
	}
}
