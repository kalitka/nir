﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using NtvpChecker.DataStructures;

namespace NtvpChecker.Checking.Rsdn.Rules
{
	/// <summary>
	/// Enforces proper usage of whitespaces and newlines around both commas and semicolons.
	/// </summary>
	internal class DelimiterWhitespaceRule : ISourceRule
	{
		/// <summary>
		/// Message to show when rule is violated on a comma token.
		/// </summary>
		private readonly string _commaMessage =
			ConfigurationManager.AppSettings["CommaWhitespaceError"];

		/// <summary>
		/// Message to show when rule is violated.
		/// </summary>
		private readonly string _semicolonMessage =
			ConfigurationManager.AppSettings["SemicolonWhitespaceError"];

		/// <summary>
		/// Checks every ',' or ';' token: it must have a space, or a newline,
		/// or both of these AND a comment after it to pass the test.
		/// </summary>
		/// <param name="nodeOrToken">Syntax tree node to check, all nodes of a tree
		/// are passed.</param>
		/// <returns>List of detected errors.</returns>
		public  List<Error> Check(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}
			var ret = new List<Error>();
			if (nodeOrToken.Kind() != SyntaxKind.CommaToken &&
				nodeOrToken.Kind() != SyntaxKind.SemicolonToken)
			{
				return ret;
			}

			// whitespace or newline, or both with comment
			// everything else is wrong, i guess
			var spaceAfter = HasTrailingTrivia(nodeOrToken, SyntaxKind.WhitespaceTrivia, 1);
			var newlineAfter = HasTrailingTrivia(nodeOrToken, SyntaxKind.EndOfLineTrivia, 1);
			var commentsAfter = HasTrailingTrivia(nodeOrToken, SyntaxKind.SingleLineCommentTrivia);

			var validAfter = (spaceAfter && !newlineAfter && !commentsAfter)
						|| (!spaceAfter && newlineAfter && !commentsAfter)
						|| (spaceAfter && newlineAfter && commentsAfter);

			if (HasLeadingWhitespace(nodeOrToken) || !validAfter)
			{
				var message = nodeOrToken.Kind() == SyntaxKind.CommaToken 
					? _commaMessage 
					: _semicolonMessage;
				var error = new Error(CheckingUtility.GetLineNumber(nodeOrToken), message);
				ret.Add(error);
			}

			return ret;
		}

		/// <summary>
		/// Checks whether there is a whitespcae before specfied syntax node.
		/// </summary>
		/// <param name="nodeOrToken">Node to check.</param>
		/// <returns>True if there is at least one whitespace, false otherwise.</returns>
		private bool HasLeadingWhitespace(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}
			return nodeOrToken.GetPreviousSibling().GetTrailingTrivia()
				.Count(x => x.Kind() == SyntaxKind.WhitespaceTrivia) > 0;
		}

		/// <summary>
		/// Checks wheter specified node has any or a specific amount of trivia
		/// in its TrailingTrivia collection.
		/// </summary>
		/// <param name="nodeOrToken">Node to check.</param>
		/// <param name="triviaKind">Kind of trivia to look for.</param>
		/// <param name="exactCount">(Optional) required count of trivia.
		/// If left as default zero, any positive amount will do.</param>
		/// <returns>True if there are exactly non-default exact count of specified trivia
		/// or there is any if exact count is zero. False in other cases.</returns>
		private bool HasTrailingTrivia(SyntaxNodeOrToken nodeOrToken,
			SyntaxKind triviaKind, uint exactCount = 0)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}
			var count = nodeOrToken.GetTrailingTrivia().Count(x => x.Kind() == triviaKind);
			return exactCount == 0 ? count > 0 : count == exactCount;
		}
	}
}