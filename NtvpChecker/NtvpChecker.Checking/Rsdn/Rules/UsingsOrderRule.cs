﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;

using NtvpChecker.DataStructures;

namespace NtvpChecker.Checking.Rsdn.Rules
{
	/// <summary>
	/// Enforces rule that import directives within each group (denoted by newline
	/// separator) are lexicographically sorted in ascending order.
	/// </summary>
	internal class UsingsOrderRule : ISourceRule
	{
		/// <summary>
		/// Message to show when rule is violated.
		/// </summary>
		private readonly string _message
			= ConfigurationManager.AppSettings["UsingsOrderError"];

		/// <summary>
		/// Method to check for usings groups and their orders.
		/// These are extracted from CompilationUnit nodes, rest is skipped.
		/// </summary>
		/// <param name="nodeOrToken">Syntax tree node to check,
		/// all nodes of a tree are passed.</param>
		/// <returns>List of detected errors.</returns>
		public  List<Error> Check(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}

			var ret = new List<Error>();

			if (nodeOrToken.Kind() != SyntaxKind.CompilationUnit)
			{
				return ret;
			}
			// maps line numbers of group starts to their contents
			var groups = new Dictionary<uint, List<string>>();
			var currentGroup = new List<string>();

			var usings = nodeOrToken.ChildNodesAndTokens()
				.Where(x => x.Kind() == SyntaxKind.UsingDirective);

			if (!usings.Any())
			{
				return ret;
			}

			// first key is the first group start, obtained separately
			var currentGroupStart = CheckingUtility.GetLineNumber(usings.First());

			// populates the groups
			foreach (var usingDirective in usings)
			{

				var name = GetImportName(usingDirective);
				// if there's a newline before, that's a new group
				if (usingDirective.GetLeadingTrivia().Any(x => x.Kind() == SyntaxKind.EndOfLineTrivia))
				{
					groups[currentGroupStart] = currentGroup;
					currentGroup = new List<string> { name };
					currentGroupStart = CheckingUtility.GetLineNumber(usingDirective);
				}
				else
				{
					currentGroup.Add(name);
				}
			}
			// the last group wasn't added
			groups[currentGroupStart] = currentGroup;

			//and now the checks
			foreach (var key in groups.Keys)
			{
				var unsorted = groups[key];
				var sorted = new List<string>(unsorted);
				sorted.Sort();
				for (int i = 0; i < sorted.Count; i++)
				{
					if (unsorted[i] != sorted[i])
					{
						var error = new Error(key, _message);
						ret.Add(error);
						// one error per group
						break;
					}
				}
			}

			return ret;
		}

		private string GetImportName(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}
			if (nodeOrToken.Kind() != SyntaxKind.UsingDirective)
			{
				throw new ArgumentException("Node kind should be UsingDirective", "nodeOrToken");
			}
			// it's either QualifiedName or IdentifierName at second place in child nodes
			return nodeOrToken.ChildNodesAndTokens().ElementAt(1).ToString();
		}
	}
}
