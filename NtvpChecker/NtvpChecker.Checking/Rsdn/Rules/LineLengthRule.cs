﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Text;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using NtvpChecker.DataStructures;

namespace NtvpChecker.Checking.Rsdn.Rules
{
	/// <summary>
	/// Checks for line length, with proper tabs-to-spaces convertation.
	/// </summary>
	internal class LineLengthRule : ISourceRule
	{
		/// <summary>
		/// Error message.
		/// </summary>
		private readonly string _message =
			ConfigurationManager.AppSettings["LineLengthError"];

		/// <summary>
		/// Override method. Checks CompilationUnit's full text line by line.
		/// </summary>
		/// <param name="nodeOrToken">Tree node.</param>
		/// <returns>Empty list if nodeOrToken is not CompilationUnit,
		/// list of errors otherwise.</returns>
		public List<Error> Check(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}

			var ret = new List<Error>();
			if (nodeOrToken.Kind() != SyntaxKind.CompilationUnit)
			{
				return ret;
			}

			uint i = 1;
			// see https://stackoverflow.com/a/6873727
			// why it can't be just \n everywhere
			using (var sr = new StringReader(nodeOrToken.ToFullString()))
			{
				string line;
				while ((line = sr.ReadLine()) != null)
				{
					if (!CheckingUtility.IsLineLengthValid(line))
					{
						var error = new Error(i, _message);
						ret.Add(error);
					}
					i++;
				}
			}

			return ret;
		}
	}
}