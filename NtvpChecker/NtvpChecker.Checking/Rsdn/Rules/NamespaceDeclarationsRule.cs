﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

using NtvpChecker.DataStructures;

namespace NtvpChecker.Checking.Rsdn.Rules
{
	/// <summary>
	/// Checks that only one entity is declared in namespace's root and its name
	/// matches with source file's name.
	/// Only delegate declaration may be declared in one file.
	/// </summary>
	internal class NamespaceDeclarationsRule : ISourceRule
	{
		/// <summary>
		/// Message to show when this rule is violated and there are multiple
		/// non-delegate declarations in a given namespace.
		/// </summary>
		private readonly string _messageCount
			= ConfigurationManager.AppSettings["DeclarationCountError"];

		/// <summary>
		/// Message to show when declaration name does not match filename.
		/// </summary>
		private readonly string _messageNaming
			= ConfigurationManager.AppSettings["DeclarationFilenameError"];

		/// <summary>
		/// String to hold current source name. Is set up whenever a new source check
		/// is triggered.
		/// </summary>
		private string _sourceName;

		/// <summary>
		/// Chars that separate various parts of a Source.Identifier.
		/// </summary>
		private readonly char[] _splitters = new char[] { '>', '\\', '.' };

		/// <summary>
		/// Check for number of declarations and their namings.
		/// </summary>
		/// <param name="nodeOrToken">Syntax tree node to check,
		/// all nodes of a tree are passed.</param>
		/// <returns>List of detected errors.</returns>
		public  List<Error> Check(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}

			var ret = new List<Error>();
			if (nodeOrToken.Kind() != SyntaxKind.NamespaceDeclaration)
			{
				return ret;
			}
			// all decarations are between { and } tokens
			var declarations = CheckingUtility.GetInnerDeclarations(nodeOrToken);

			if (declarations.Count() != 1 &&
				!declarations.All(x => x.Kind() == SyntaxKind.DelegateDeclaration))
			{
				// one error for each extra declaration
				foreach (var extraDecl in declarations.Skip(1))
				{
					var error = new Error(CheckingUtility.GetLineNumber(extraDecl), _messageCount);
					ret.Add(error);
				}
			}
			// multiple delegates cannot have the same name
			if (!declarations.All(x => x.Kind() == SyntaxKind.DelegateDeclaration)
				&& declarations.Count() > 1)
			{
				foreach (var decl in declarations)
				{
					if (CheckingUtility.GetIdentifier(decl) != _sourceName)
					{
						var error = new Error(CheckingUtility.GetLineNumber(decl), _messageNaming);
						ret.Add(error);
					}
				}
			}
			return ret;
		}

		/// <summary>
		/// Method to set a Source filename being checked.
		/// </summary>
		/// <param name="identifier">Name to set. Must at least have 'name.ext' format.</param>
		public void SetSourceName(string identifier)
		{
			var parts = identifier.Split(_splitters);
			if (parts.Count() < 2)
			{
				throw new ArgumentException($"Invalid source identifier '{identifier}'"
					+ ", unable to get a filename", "identifier");
			}
			// second to last, last one is 'cs' from dot splitting
			_sourceName = parts[parts.Count() - 2];
		}
	}
}
