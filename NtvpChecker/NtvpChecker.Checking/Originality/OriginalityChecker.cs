﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using NtvpChecker.DataStructures;

namespace NtvpChecker.Checking.Originality
{
	/// <summary>
	/// Class that encapsulates originality checking.
	/// </summary>
	internal class OriginalityChecker
	{
		/// <summary>
		/// Minimum subtree depth to use it in comparisons.
		/// </summary>
		private const uint _depthThreshold = 10;

		/// <summary>
		/// Minimum affinity of subtrees that are considered suspiciously similar.
		/// </summary>
		private const double _affinityThreshold = 0.835d;

		/// <summary>
		/// Checks two sources for similarities.
		/// </summary>
		/// <param name="source1">First source to check.</param>
		/// <param name="source2">Second source to check.</param>
		/// <returns>List of pairs of detected copies: first element is text from source1,
		/// second from source2.</returns>
		public List<Tuple<string, string>> CheckSources(Source source1, Source source2)
		{

			if (source1 == null)
			{
				throw new ArgumentNullException("source1");
			}
			if (source2 == null)
			{
				throw new ArgumentNullException("source2");
			}

			var ret = new List<Tuple<string, string>>();
			// so here's the idea: we store all the nodes that are triggered as pasted,
			// and if either of nodes in a pair have a parent that was already detected,
			// we discard the match. This worked to remove multiple detects, no idea on the
			// side effects though
			var alreadyAdded = new List<SyntaxNodeOrToken>();
			foreach (var firstSubtree in EnumerateSubtrees(source1))
			{
				foreach (var secondSubtree in EnumerateSubtrees(source2))
				{
					// find if any of the parents was added earlier
					var firstParentsAdded = alreadyAdded.Intersect(GetParents(firstSubtree));
					var secondParentsAdded = alreadyAdded.Intersect(GetParents(secondSubtree));

					// discard if there is at least one parent
					if (!(firstParentsAdded.Any() || secondParentsAdded.Any()))
					{
						var a = GetAffinity(firstSubtree, secondSubtree);
						if (a >= _affinityThreshold)
						{
							// add them anyway to prevent orignal detect's grandchildren popping up
							alreadyAdded.Add(firstSubtree);
							alreadyAdded.Add(secondSubtree);
							// this is less temporary crap
							var firstText = source1.Identifier + Environment.NewLine +
											firstSubtree.ToFullString();
							var secondText = source2.Identifier + Environment.NewLine +
											 secondSubtree.ToFullString();
							var tuple = Tuple.Create(firstText, secondText);
							ret.Add(tuple);
						}
					}
				}
			}

			return ret;
		}

		/// <summary>
		/// Returns parents of the node all the way back to root of the tree.
		/// </summary>
		/// <param name="nodeOrToken">Node to get parents for.</param>
		/// <returns>List of node's parents</returns>
		private List<SyntaxNodeOrToken> GetParents(SyntaxNodeOrToken nodeOrToken)
		{
			if (nodeOrToken == null)
			{
				throw new ArgumentNullException("nodeOrToken");
			}

			var ret = new List<SyntaxNodeOrToken>();
			var iter = nodeOrToken;
			while (iter != null)
			{
				ret.Add(iter.Parent);
				iter = iter.Parent;
			}

			return ret;
		}

		/// <summary>
		/// Returns syntax (sub)tree nodes in a flat list. Used in GetAffinity.
		/// </summary>
		/// <param name="subtreeRoot">Root node of a tree.</param>
		/// <returns>All nodes of a tree (including root) in a List.</returns>
		private List<SyntaxNodeOrToken> GetNodesFlat(SyntaxNodeOrToken subtreeRoot)
		{

			if (subtreeRoot == null)
			{
				throw new ArgumentNullException("subtreeRoot");
			}

			var ret = new List<SyntaxNodeOrToken>
			{
				subtreeRoot
			};
			
			foreach (var childNode in subtreeRoot.ChildNodesAndTokens())
			{
				ret.AddRange(GetNodesFlat(childNode));
			}

			return ret;
		}

		/// <summary>
		/// Calculates affinity between two syntax (sub)trees.
		/// </summary>
		/// <param name="subtreeL">Root node of first tree.</param>
		/// <param name="subtreeR">Root node of second tree.</param>
		/// <returns>Affinity value between 0 and 1, both inclusive.</returns>
		private double GetAffinity(SyntaxNodeOrToken subtreeL, SyntaxNodeOrToken subtreeR)
		{
			// s, l, and r stand for shared, left and right respectively

			if (subtreeL == null)
			{
				throw new ArgumentNullException("subtreeL");
			}
			if (subtreeR == null)
			{
				throw new ArgumentNullException("subtreeR");
			}

			var lFlat = GetNodesFlat(subtreeL);
			var rFlat = GetNodesFlat(subtreeR);
			// counts matching nodes
			var counter = 0;

			foreach (var lNode in lFlat)
			{
				foreach (var rNode in rFlat)
				{
					if (lNode.Kind() == rNode.Kind())
					{
						// we can't remove the matching node from the outer list,
						//  foreach is called once and will throw an exception if list
						//  is modified, so here's the counter of nodes from outer list
						//  that got counterparts in inner list.
						// third list to store shared nodes become victim of the optimization.
						counter++;
						rFlat.Remove(rNode);
						break;
					}
				}
			}

			// a bit tricky here: counter stores number of shared nodes,
			// but they were removed in the right list
			var lCount = (double) (lFlat.Count - counter);
			var rCount = (double) rFlat.Count;
			var sCount = (double) counter;

			return (2 * sCount) / (2 * sCount + lCount + rCount);
		}

		/// <summary>
		/// Calculates number of child sublevels for every node in a tree.
		/// </summary>
		/// <param name="treeRoot">Root of tree.</param>
		/// <returns>Dictionary: tree node -> number of its sublevels. 1 means no childen.</returns>
		private Dictionary<SyntaxNodeOrToken, uint> GetWeights(SyntaxNodeOrToken treeRoot)
		{
			if (treeRoot == null)
			{
				throw new ArgumentNullException("treeRoot");
			}

			var ret = new Dictionary<SyntaxNodeOrToken, uint>();

			foreach (var node in GetNodesFlat(treeRoot))
			{
				ret[node] = 0;
			}

			// first we find leaves to assign them value of 1
			uint value = 1;
			var toSet = ret.Keys.Where(x => x.ChildNodesAndTokens().Count == 0).ToList();

			// sharpdevelop told me to use Any instead of Count() > 0 for performance reasons
			while (toSet.Any())
			{
				foreach (var node in toSet)
				{
					ret[node] = Math.Max(ret[node], value);
				}

				value++;
				// new list contains non-null parents of nodes updated this iteration
				var newToSet = new List<SyntaxNodeOrToken>();
				foreach (var node in toSet)
				{
					var parent = node.Parent;
					if (parent != null && !newToSet.Contains(parent))
					{
						newToSet.Add(parent);
					}
				}

				// continue to the new list
				toSet = newToSet;
			}

			return ret;
		}

		/// <summary>
		/// Returns enumerable of all nodes which are subtree roots with depth
		/// above the threshold.
		/// </summary>
		/// <param name="source">Source to get subtrees from.</param>
		/// <returns>Enumerable of roots of deep enough subtrees.</returns>
		private IEnumerable<SyntaxNodeOrToken> EnumerateSubtrees(Source source)
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}
			if (source.Content.Count == 0)
			{
				throw new ArgumentException("Provided Source has zero source code lines",
					"source");
			}
			var sourceText = String.Join(Environment.NewLine, source.Content);
			var tree = CSharpSyntaxTree.ParseText(sourceText);
			var root = tree.GetRoot();

			var weights = GetWeights(root);

			return weights.Keys.Where(x => weights[x] >= _depthThreshold);
		}
	}
}