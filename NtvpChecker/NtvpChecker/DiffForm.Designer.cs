﻿
namespace NtvpChecker
{
	partial class DiffForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private SyncedRichTextBox leftRichTextBox;
		private SyncedRichTextBox rightRichTextBox;
		private System.Windows.Forms.Button prevButton;
		private System.Windows.Forms.Button nextButton;
		private System.Windows.Forms.Label navigationLabel;
		private System.Windows.Forms.Label leftHeaderLabel;
		private System.Windows.Forms.Label rightHeaderLabel;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.leftRichTextBox = new NtvpChecker.SyncedRichTextBox();
			this.rightRichTextBox = new NtvpChecker.SyncedRichTextBox();
			this.prevButton = new System.Windows.Forms.Button();
			this.nextButton = new System.Windows.Forms.Button();
			this.navigationLabel = new System.Windows.Forms.Label();
			this.leftHeaderLabel = new System.Windows.Forms.Label();
			this.rightHeaderLabel = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// leftRichTextBox
			// 
			this.leftRichTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.leftRichTextBox.BackColor = System.Drawing.SystemColors.Window;
			this.leftRichTextBox.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.leftRichTextBox.Location = new System.Drawing.Point(8, 48);
			this.leftRichTextBox.Margin = new System.Windows.Forms.Padding(0);
			this.leftRichTextBox.MaxLength = 100000000;
			this.leftRichTextBox.Name = "leftRichTextBox";
			this.leftRichTextBox.ReadOnly = true;
			this.leftRichTextBox.Size = new System.Drawing.Size(376, 442);
			this.leftRichTextBox.TabIndex = 1;
			this.leftRichTextBox.Text = "";
			this.leftRichTextBox.WordWrap = false;
			// 
			// rightRichTextBox
			// 
			this.rightRichTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
			this.rightRichTextBox.BackColor = System.Drawing.SystemColors.Window;
			this.rightRichTextBox.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.rightRichTextBox.Location = new System.Drawing.Point(400, 48);
			this.rightRichTextBox.Margin = new System.Windows.Forms.Padding(0);
			this.rightRichTextBox.MaxLength = 100000000;
			this.rightRichTextBox.Name = "rightRichTextBox";
			this.rightRichTextBox.ReadOnly = true;
			this.rightRichTextBox.Size = new System.Drawing.Size(376, 442);
			this.rightRichTextBox.TabIndex = 2;
			this.rightRichTextBox.Text = "";
			this.rightRichTextBox.WordWrap = false;
			// 
			// prevButton
			// 
			this.prevButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.prevButton.Location = new System.Drawing.Point(8, 496);
			this.prevButton.Name = "prevButton";
			this.prevButton.Size = new System.Drawing.Size(160, 56);
			this.prevButton.TabIndex = 3;
			this.prevButton.Text = "<- Предыдущее";
			this.prevButton.UseVisualStyleBackColor = true;
			this.prevButton.Click += new System.EventHandler(this.PrevButtonClick);
			// 
			// nextButton
			// 
			this.nextButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.nextButton.Location = new System.Drawing.Point(616, 496);
			this.nextButton.Name = "nextButton";
			this.nextButton.Size = new System.Drawing.Size(160, 56);
			this.nextButton.TabIndex = 4;
			this.nextButton.Text = "Следующее ->";
			this.nextButton.UseVisualStyleBackColor = true;
			this.nextButton.Click += new System.EventHandler(this.NextButtonClick);
			// 
			// navigationLabel
			// 
			this.navigationLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.navigationLabel.Location = new System.Drawing.Point(176, 496);
			this.navigationLabel.Name = "navigationLabel";
			this.navigationLabel.Size = new System.Drawing.Size(432, 56);
			this.navigationLabel.TabIndex = 5;
			this.navigationLabel.Text = "inhale my dong enragement child";
			this.navigationLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// leftHeaderLabel
			// 
			this.leftHeaderLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.leftHeaderLabel.Location = new System.Drawing.Point(8, 8);
			this.leftHeaderLabel.Name = "leftHeaderLabel";
			this.leftHeaderLabel.Size = new System.Drawing.Size(376, 32);
			this.leftHeaderLabel.TabIndex = 6;
			this.leftHeaderLabel.Text = "first title";
			this.leftHeaderLabel.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
			// 
			// rightHeaderLabel
			// 
			this.rightHeaderLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.rightHeaderLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.rightHeaderLabel.Location = new System.Drawing.Point(400, 8);
			this.rightHeaderLabel.Name = "rightHeaderLabel";
			this.rightHeaderLabel.Size = new System.Drawing.Size(376, 32);
			this.rightHeaderLabel.TabIndex = 7;
			this.rightHeaderLabel.Text = "second title";
			this.rightHeaderLabel.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
			// 
			// DiffForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(784, 561);
			this.Controls.Add(this.rightHeaderLabel);
			this.Controls.Add(this.leftHeaderLabel);
			this.Controls.Add(this.navigationLabel);
			this.Controls.Add(this.nextButton);
			this.Controls.Add(this.prevButton);
			this.Controls.Add(this.rightRichTextBox);
			this.Controls.Add(this.leftRichTextBox);
			this.MinimumSize = new System.Drawing.Size(800, 600);
			this.Name = "DiffForm";
			this.ShowIcon = false;
			this.Text = "Просмотр заимствований";
			this.Resize += new System.EventHandler(this.DiffFormResize);
			this.ResumeLayout(false);

		}
	}
}
