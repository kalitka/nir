﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace NtvpChecker
{
	/// <summary>
	/// RichTextBox that syncs all messages got to another SyncedRichTextBox.
	/// Messages are filtered to include possible scroll-inducing ones, e.g.
	/// the scrolling itself via scrollbars, mousewheel scrolling, and key downs:
	/// page up and page down work, but home and end do not :(
	/// </summary>
	internal class SyncedRichTextBox: RichTextBox
	{
		
		/// <summary>
		/// Winapi function to send messages. Used there to set tab width on textboxes.
		/// </summary>
		/// <param name="h">Handle to send message to.</param>
		/// <param name="msg">Message kind.</param>
		/// <param name="wParam">First parameter to send.</param>
		/// <param name="lParam">Second parameter to send.</param>
		/// <returns>Result of message processing.</returns>
		[DllImport("User32.dll", CharSet = CharSet.Auto)]
		private static extern IntPtr SendMessage(IntPtr h, int msg, int wParam, int[] lParam);
		
		/// <summary>
		/// Delegate to event sync.
		/// </summary>
		/// <param name="m">Message to resend to other SyncedRichTextBox.</param>
		public delegate void VScrollEventHandler(ref Message m);
		
		/// <summary>
		/// Event to resend events.
		/// </summary>
		public event VScrollEventHandler MyVScroll;
		
		/// <summary>
		/// Tab width in chars to set.
		/// </summary>
		private const int _tabWidth = 2;
		
		/// <summary>
		/// Winapi constant to signify horizontal scroll event.
		/// </summary>
		private const int WM_HSCROLL = 0x114;
		
		/// <summary>
		/// Winapi constant to signify vertical scroll event.
		/// </summary>
		private const int WM_VSRCOLL = 0x115;
		
		/// <summary>
		/// Winapi constant to signify key down event.
		/// </summary>
		private const int WM_KEYDOWN = 0x100;
		
		/// <summary>
		/// Winapi constant to signify mouse wheel scroll event.
		/// </summary>
		private const int WM_MOUSEWHEEL = 0x20a;
		
		/// <summary>
		/// Winapi constant to set tab witdh.
		/// </summary>
		private const int EM_SETTABSTOPS = 0xcb;
		
		/// <summary>
		/// Constructor that sets tab width.
		/// </summary>
		public SyncedRichTextBox(): base()
		{
			SetTabWidth(_tabWidth);
		}
		
		/// <summary>
		/// Method to directly call base class' WndProc to avoid endless loops.
		/// </summary>
		/// <param name="m">Message to process.</param>
		public void DirectWndProc(ref Message m)
		{
			base.WndProc(ref m);
		}
		
		/// <summary>
		/// Override that also copies message to any MyVScroll handlers present.
		/// </summary>
		/// <param name="m">Message to process and resend.</param>
		protected override void WndProc(ref Message m)
		{
			if (m.Msg == WM_VSRCOLL || m.Msg == WM_HSCROLL || m.Msg == WM_KEYDOWN
				|| m.Msg == WM_MOUSEWHEEL)
			{
				MyVScroll?.Invoke(ref m);
			}
			base.WndProc(ref m);
		}
		
		/// <summary>
		/// Method to set tab width.
		/// </summary>
		/// <param name="width">New tab width in symbols.</param>
		private void SetTabWidth(int width)
		{
			// see https://stackoverflow.com/questions/1298406/
			// one is the size of array with widths,
			// this config sets every single tab stop to be the specified width
			SendMessage(this.Handle, EM_SETTABSTOPS, 1, new int[] {width * 4});
		}
	}
}
