﻿
namespace NtvpChecker
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.Button openButton;
		private System.Windows.Forms.OpenFileDialog openFileDialog1;
		private System.Windows.Forms.Button runButton;
		private System.Windows.Forms.Button reviewButton;
		private System.Windows.Forms.Button pasteCheckButton;
		private System.Windows.Forms.ProgressBar originalityProgressBar;
		private System.Windows.Forms.Label authorDataLabel;
		private System.Windows.Forms.Label buildErrorsLabel;
		private System.Windows.Forms.Label rsdnErrorsLabel;
		private System.Windows.Forms.Label pastesDetectedLabel;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.openButton = new System.Windows.Forms.Button();
			this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
			this.runButton = new System.Windows.Forms.Button();
			this.reviewButton = new System.Windows.Forms.Button();
			this.pasteCheckButton = new System.Windows.Forms.Button();
			this.originalityProgressBar = new System.Windows.Forms.ProgressBar();
			this.authorDataLabel = new System.Windows.Forms.Label();
			this.buildErrorsLabel = new System.Windows.Forms.Label();
			this.rsdnErrorsLabel = new System.Windows.Forms.Label();
			this.pastesDetectedLabel = new System.Windows.Forms.Label();
			this.aboutLinkLabel = new System.Windows.Forms.LinkLabel();
			this.SuspendLayout();
			// 
			// openButton
			// 
			this.openButton.Location = new System.Drawing.Point(8, 8);
			this.openButton.Name = "openButton";
			this.openButton.Size = new System.Drawing.Size(120, 40);
			this.openButton.TabIndex = 1;
			this.openButton.Text = "Проверить архив";
			this.openButton.UseVisualStyleBackColor = true;
			this.openButton.Click += new System.EventHandler(this.TempOpenButtonClick);
			// 
			// openFileDialog1
			// 
			this.openFileDialog1.Filter = "Archives|*.zip;*.7z;*.rar";
			// 
			// runButton
			// 
			this.runButton.Enabled = false;
			this.runButton.Location = new System.Drawing.Point(8, 56);
			this.runButton.Name = "runButton";
			this.runButton.Size = new System.Drawing.Size(120, 40);
			this.runButton.TabIndex = 2;
			this.runButton.Text = "Запустить";
			this.runButton.UseVisualStyleBackColor = true;
			this.runButton.Click += new System.EventHandler(this.TempRunButtonClick);
			// 
			// reviewButton
			// 
			this.reviewButton.Enabled = false;
			this.reviewButton.Location = new System.Drawing.Point(8, 104);
			this.reviewButton.Name = "reviewButton";
			this.reviewButton.Size = new System.Drawing.Size(120, 40);
			this.reviewButton.TabIndex = 3;
			this.reviewButton.Text = "Открыть рецензию";
			this.reviewButton.UseVisualStyleBackColor = true;
			this.reviewButton.Click += new System.EventHandler(this.TempReviewButtonClick);
			// 
			// pasteCheckButton
			// 
			this.pasteCheckButton.Enabled = false;
			this.pasteCheckButton.Location = new System.Drawing.Point(8, 152);
			this.pasteCheckButton.Name = "pasteCheckButton";
			this.pasteCheckButton.Size = new System.Drawing.Size(120, 40);
			this.pasteCheckButton.TabIndex = 4;
			this.pasteCheckButton.Text = "Проверить оригинальность";
			this.pasteCheckButton.UseVisualStyleBackColor = true;
			this.pasteCheckButton.Click += new System.EventHandler(this.TempPasteButtonClick);
			// 
			// originalityProgressBar
			// 
			this.originalityProgressBar.Location = new System.Drawing.Point(136, 152);
			this.originalityProgressBar.Name = "originalityProgressBar";
			this.originalityProgressBar.Size = new System.Drawing.Size(352, 40);
			this.originalityProgressBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
			this.originalityProgressBar.TabIndex = 5;
			this.originalityProgressBar.Visible = false;
			// 
			// authorDataLabel
			// 
			this.authorDataLabel.Location = new System.Drawing.Point(136, 8);
			this.authorDataLabel.Name = "authorDataLabel";
			this.authorDataLabel.Size = new System.Drawing.Size(352, 40);
			this.authorDataLabel.TabIndex = 6;
			this.authorDataLabel.Text = "впусти обратно";
			this.authorDataLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.authorDataLabel.Visible = false;
			// 
			// buildErrorsLabel
			// 
			this.buildErrorsLabel.Location = new System.Drawing.Point(136, 56);
			this.buildErrorsLabel.Name = "buildErrorsLabel";
			this.buildErrorsLabel.Size = new System.Drawing.Size(352, 40);
			this.buildErrorsLabel.TabIndex = 7;
			this.buildErrorsLabel.Text = "//TODO: Input argument testing!";
			this.buildErrorsLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.buildErrorsLabel.Visible = false;
			// 
			// rsdnErrorsLabel
			// 
			this.rsdnErrorsLabel.Location = new System.Drawing.Point(136, 104);
			this.rsdnErrorsLabel.Name = "rsdnErrorsLabel";
			this.rsdnErrorsLabel.Size = new System.Drawing.Size(352, 40);
			this.rsdnErrorsLabel.TabIndex = 8;
			this.rsdnErrorsLabel.Text = "Timeline lost";
			this.rsdnErrorsLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.rsdnErrorsLabel.Visible = false;
			// 
			// pastesDetectedLabel
			// 
			this.pastesDetectedLabel.Location = new System.Drawing.Point(136, 152);
			this.pastesDetectedLabel.Name = "pastesDetectedLabel";
			this.pastesDetectedLabel.Size = new System.Drawing.Size(352, 40);
			this.pastesDetectedLabel.TabIndex = 9;
			this.pastesDetectedLabel.Text = "Hey Red. We\'re not going to get away with this, are we?";
			this.pastesDetectedLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.pastesDetectedLabel.Visible = false;
			// 
			// aboutLinkLabel
			// 
			this.aboutLinkLabel.AutoSize = true;
			this.aboutLinkLabel.Location = new System.Drawing.Point(409, 195);
			this.aboutLinkLabel.Name = "aboutLinkLabel";
			this.aboutLinkLabel.Size = new System.Drawing.Size(75, 13);
			this.aboutLinkLabel.TabIndex = 10;
			this.aboutLinkLabel.TabStop = true;
			this.aboutLinkLabel.Text = "О программе";
			this.aboutLinkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.aboutLinkLabel_LinkClicked);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(496, 217);
			this.Controls.Add(this.aboutLinkLabel);
			this.Controls.Add(this.pastesDetectedLabel);
			this.Controls.Add(this.rsdnErrorsLabel);
			this.Controls.Add(this.buildErrorsLabel);
			this.Controls.Add(this.authorDataLabel);
			this.Controls.Add(this.originalityProgressBar);
			this.Controls.Add(this.pasteCheckButton);
			this.Controls.Add(this.reviewButton);
			this.Controls.Add(this.runButton);
			this.Controls.Add(this.openButton);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.MinimumSize = new System.Drawing.Size(512, 256);
			this.Name = "MainForm";
			this.ShowIcon = false;
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.Text = "NtvpChecker";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		private System.Windows.Forms.LinkLabel aboutLinkLabel;
	}
}
