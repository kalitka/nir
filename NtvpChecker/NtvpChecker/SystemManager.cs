﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Windows.Forms;

using NtvpChecker.Checking;
using NtvpChecker.Database;
using NtvpChecker.DataStructures;
using NtvpChecker.Review;
using NtvpChecker.Utility;

namespace NtvpChecker
{
	/// <summary>
	/// Class that manages all the stuff happening.
	/// </summary>
	internal class SystemManager
	{
		
		/// <summary>
		/// Class to represent argument of percentage changing event.
		/// Holds one property: int Percentage.
		/// </summary>
		internal class PercentageChangedEventArgs: EventArgs
		{
			/// <summary>
			/// Property to store percentage value.
			/// </summary>
			public int Percentage { get; private set;}
			
			/// <summary>
			/// Constructor.
			/// </summary>
			/// <param name="percentage">Percentage value to store.</param>
			public PercentageChangedEventArgs(int percentage)
			{
				if (percentage < 0 || percentage > 100)
				{
					throw new ArgumentException("Percentage must be in 0..100 range", "percentage");
				}
				Percentage = percentage;
			}
		}
		
		/// <summary>
		/// Delegate for percentage changing event handler.
		/// </summary>
		/// <param name="sender">Event sender, an instance of SystemManager.</param>
		/// <param name="e">Argument that contains new percentage.</param>
		public delegate void PercentageChangedEventHandler(object sender, PercentageChangedEventArgs e);
		
		/// <summary>
		/// Event that is fired on percentage changing to inform MainForm about that.
		/// </summary>
		public event PercentageChangedEventHandler OnPercentageChanged;
		
		/// <summary>
		/// Delegate for check completing event handler.
		/// </summary>
		/// <param name="sender">Event sender, an instance of SystemManager.</param>
		/// <param name="e">Empty argument.</param>
		public delegate void WorkDoneEventHandler(object sender, EventArgs e);
		
		/// <summary>
		/// Event that is fired when check is complete to send results to MainForm.
		/// </summary>
		public event WorkDoneEventHandler OnWorkDone;
		
		/// <summary>
		/// (Hardcoded) path to unpack archive to.
		/// </summary>
		private readonly string _tempPath = Environment.GetEnvironmentVariable("TEMP")
			+ "\\NtvpChecker";
		
		/// <summary>
		/// Holds data about currently processed lab.
		/// </summary>
		private AnswerData _currentAnswer;
		
		/// <summary>
		/// Holds path to solution in unpacked folder.
		/// Used to pass it on to the SolutionCompiler.
		/// </summary>
		private string _solutionPath;
		
		/// <summary>
		/// CodeCheckManager instance, all error checking is passed on it.
		/// </summary>
		private readonly CodeCheckManager _checkManager = new CodeCheckManager();
		
		/// <summary>
		/// Instance of IReviewGenerator to use. Currently only RTF generator is implemented.
		/// </summary>
		private readonly IReviewGenerator _reviewGenerator = new RtfReviewGenerator();

		/// <summary>
		/// List of build errors field, used to pass it from SolutionComplier to IReviewGenerator.
		/// </summary>
		private IReadOnlyList<BuildError> _buildErrors;
		
		/// <summary>
		/// Used to pass SourceChecker output to IReviewGenerator.
		/// </summary>
		private IReadOnlyDictionary<Source, List<Error>> _errors;

		/// <summary>
		/// Used to store list of potential borrowings of source code.
		/// </summary>
		private IReadOnlyList<Tuple<string, string>> _potetinalPastes;

		/// <summary>
		/// Instance of IDBWrapper to use. MongoDB for now.
		/// </summary>
		private readonly IDBWrapper _dbWrapper = new MongoDBWrapper();
		
		/// <summary>
		/// Background worker to run slow originality checks async.
		/// </summary>
		private readonly BackgroundWorker _originalityWorker = new BackgroundWorker();
		
		/// <summary>
		/// Template to human-readable data about author.
		/// {0} is author's name, {1} is city, {2} is lab number, and {3} is variant.
		/// </summary>
		private readonly string _authorTemplate
			= ConfigurationManager.AppSettings["AuthorData"];
		
		/// <summary>
		/// Text to show when there are no build errors.
		/// </summary>
		private readonly string _noBuildErrorsTemplate
			= ConfigurationManager.AppSettings["NoBuildErrorsData"];
		
		/// <summary>
		/// Template for text to show when there are some build errors.
		/// {0} is the count of build errors.
		/// </summary>
		private readonly string _buildErrorsTemplate
			= ConfigurationManager.AppSettings["BuildErrorsData"];
		
		/// <summary>
		/// Text to show when there are no RSDN errors.
		/// </summary>
		private readonly string _noErrorsTemplate
			= ConfigurationManager.AppSettings["NoRSDNErrorsData"];
		
		/// <summary>
		/// Template for text to show when there are RSDN errors.
		/// </summary>
		private readonly string _errorsTemplate
			= ConfigurationManager.AppSettings["RSDNErrorsData"];
		
		/// <summary>
		/// Holds human-readble data about originality check results.
		/// </summary>
		private string _originalityResult = null;
		
		/// <summary>
		/// Text to show when originality check's result list is empty.
		/// </summary>
		private readonly string _noSimilaritiesTemplate
			= ConfigurationManager.AppSettings["NoPasteData"];
		
		/// <summary>
		/// Template for text to show when originality check's result list is not empty.
		/// </summary>
		private readonly string _similaritiesTemplate
			= ConfigurationManager.AppSettings["PasteData"];

		/// <summary>
		/// Template for text to show when there are no labs of same number and
		/// variant from different author in the DB.
		/// </summary>
		private readonly string _noSimilarLabsTemplate
			= ConfigurationManager.AppSettings["NoLabsData"];

		/// <summary>
		/// Template for text to show when there is at least one lab of samenumber
		/// and variant from different author in the DB.
		/// </summary>
		private readonly string _similarLabsTemplate
			= ConfigurationManager.AppSettings["LabsData"];

		/// <summary>
		/// An OpenFileDialog instance to manually locate solutions when search fails
		/// like it is in an archive inside of original archive.
		/// </summary>
		private readonly OpenFileDialog _openFileDialog
			= new OpenFileDialog();

		/// <summary>
		/// Human-readble basic lab info from Metadata to display on UI.
		/// </summary>
		public string AuthorData
		{
			get
			{
				if (_currentAnswer == null)
				{
					throw new ApplicationException("Load some data before fetching its " +
						"info pretty please.");
				}
				return String.Format(_authorTemplate, _currentAnswer.Data.Author,
					_currentAnswer.Data.City, _currentAnswer.Data.LabNumber,
					_currentAnswer.Data.Variant);
			}
		}
		
		/// <summary>
		/// Human-readable data about build check results.
		/// Message that there are no errors, or count of such.
		/// </summary>
		public string BuildData
		{
			get
			{

				if (IsEmptyLab)
				{
					return ConfigurationManager.AppSettings["NoSolution"];
				}

				if (_buildErrors == null)
				{
					throw new ApplicationException("Run a MSBuild check before trying " +
						"to get its results pretty please.");
				}
				if (_buildErrors.Count == 0)
				{
					return _noBuildErrorsTemplate;
				}
				return String.Format(_buildErrorsTemplate, _buildErrors.Count);
			}
		}
		
		/// <summary>
		/// Human-readable data about RSDN check results.
		/// Message that there are no errors, or a number of them.
		/// </summary>
		public string RsdnData
		{
			get
			{
				if (_errors == null)
				{
					throw new ApplicationException("Run a RSDN check before trying " +
						"to get its results pretty please.");
				}
				if (_errors.Count == 0)
				{
					return _noErrorsTemplate;
				}
				var errorCount = 0;
				foreach (var key in _errors.Keys)
				{
					errorCount += _errors[key].Count;
				}
				return String.Format(_errorsTemplate, errorCount);
			}
		}
		
		/// <summary>
		/// Human-readable data about number of possible code duplicates
		/// found in previously checked labs.
		/// </summary>
		public string OriginalityData
		{
			get
			{
				if (_originalityResult == null)
				{
					throw new ApplicationException("Run an originality check and wait " +
						"patiently for it to finish before trying to use " +
						"its results pretty please.");
				}
				// hm, something is different
				return _originalityResult;
			}
		}

		/// <summary>
		/// Human-readable data about number of labs with same number and variant in
		/// the DB.
		/// </summary>
		public string OriginalityPreviewData
		{
			get
			{
				if (_currentAnswer == null)
				{
					throw new ApplicationException("Load some data before trying to " +
						"determine number of related labs in the DB pretty please.");
				}
				var count = _dbWrapper.GetVaildForCheckAgainst(_currentAnswer).Count;
				if (count > 0)
				{
					return String.Format(_similarLabsTemplate, count);
				}
				return _noSimilarLabsTemplate;
			}
		}

		/// <summary>
		/// Field that indicates whether it is sane to do an originality check on a 
		/// lab that is currently loaded.
		/// </summary>
		public bool IsOriginalityCheckPossible
		{
			get
			{
				if (_currentAnswer == null)
				{
					throw new ApplicationException("Load some data before trying to " +
						"determine whether it makes sense to run an originality check " +
						"pretty please.");
				}
				return _dbWrapper.GetVaildForCheckAgainst(_currentAnswer).Count > 0
					&& !IsEmptyLab;
			}
		}

		/// <summary>
		/// Indicates if lab was loaded without solution.
		/// </summary>
		private bool IsEmptyLab => _solutionPath == null || _currentAnswer.Sources.Count == 0;

		/// <summary>
		/// Constructor, sets up BackgroundWorker instance.
		/// </summary>
		public SystemManager()
		{
			// configure our background worker
			_originalityWorker.WorkerReportsProgress = true;
			_originalityWorker.WorkerSupportsCancellation = true;
			_originalityWorker.DoWork += _checkManager.CheckOriginalityDoWork;
			_originalityWorker.ProgressChanged += WorkerProgressChanged;
			_originalityWorker.RunWorkerCompleted += WorkerCompleted;
			// and the open file dialog too
			_openFileDialog.Filter = "Все файлы|*.*";
		}
		
		/// <summary>
		/// Load lab data from archive.
		/// </summary>
		/// <param name="archiveFilename">Path to archive to load.</param>
		/// <returns>DataLoadResult indicating result of the operation.</returns>
		public DataLoadResult LoadData(string archiveFilename)
		{
			if (archiveFilename == null)
			{
				throw new ArgumentNullException("archiveFilename");
			}
			if (!File.Exists(archiveFilename))
			{
				throw new ArgumentException($"File {archiveFilename} does not exists",
					"archiveFilename");
			}
			// reset everything
			_solutionPath = String.Empty;
			_buildErrors = null;
			_errors = null;
			_potetinalPastes = null;
			_originalityResult = null;
			_originalityWorker.CancelAsync();
			
			// tries to empty the folder from the previously unpacked lab
			// if folder is not present, does nothing
			try
			{
				Directory.Delete(_tempPath, true);
			}
			catch (DirectoryNotFoundException)
			{
				// it wasn't there, there's nothing to do
			}
			
			// works, unless anything else is trying to use the same path
			// some handle magic
			Directory.CreateDirectory(_tempPath);
			ArchiveManager.Unpack(archiveFilename, _tempPath);
			
			var inputForm = new NumberInputForm();
			
			if (inputForm.ShowDialog() == DialogResult.OK)
			{
				// things may go horribly wrong here
				try
				{
					_currentAnswer = new AnswerData(_tempPath, inputForm.Number, null);
				}
				catch (DataLoadException ex)
				{
					// i hate my life
					if (ex.Result == DataLoadResult.NoSolution)
					{
						return SearchSolutionManually(inputForm.Number);
					}
					else
					{
						_currentAnswer = null;
						return ex.Result;
					}

				}
				return FinalizeLoading();
			}
			return DataLoadResult.Cancelled;
		}

		/// <summary>
		/// Shorthand for loading a solutionless lab. Used more than one time, so is
		/// move to its own method.
		/// </summary>
		/// <param name="labNumber">Lab number as provided by user.</param>
		/// <returns>DataLoadResult.OK</returns>
		private DataLoadResult LoadWithNoSolution(uint labNumber)
		{
			_currentAnswer = new AnswerData(_tempPath, labNumber);
			return DataLoadResult.OK;
		}

		/// <summary>
		/// Shorthand for finalizing a properly loaded lab by setting _solutionPath
		/// value and saving the lab content to DB.
		/// </summary>
		/// <returns>DataLoadResult.OK</returns>
		private DataLoadResult FinalizeLoading()
		{
			_solutionPath = _currentAnswer.SolutionPath;
			_dbWrapper.SaveToDB(_currentAnswer);
			return DataLoadResult.OK;
		}

		/// <summary>
		/// Method to allow user to manually search for solution path when automatic search failed.
		/// Most common cause is an archive inside of original archive.
		/// </summary>
		/// <param name="labNumber">Lab number as provided by user.</param>
		/// <returns>DataLoadResult of manual search.</returns>
		private DataLoadResult SearchSolutionManually(uint labNumber)
		{
			try
			{
				_openFileDialog.InitialDirectory = _tempPath;
				if (!(MessageBox.Show(ConfigurationManager.AppSettings["ManualSearchPrompt"],
					"Решение не найдено", MessageBoxButtons.YesNo,
					MessageBoxIcon.Question) == DialogResult.Yes && _openFileDialog.ShowDialog() == DialogResult.OK))
				{
					return LoadWithNoSolution(labNumber);
				}
				else
				{
					// sanity check
					while (!_openFileDialog.FileName.EndsWith(".sln"))
					{
						MessageBox.Show(ConfigurationManager.AppSettings["NotASolution"],
							"Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
						if (_openFileDialog.ShowDialog() != DialogResult.OK)
						{
							return LoadWithNoSolution(labNumber);
						}
					}
					_currentAnswer = new AnswerData(_tempPath, labNumber,
						_openFileDialog.FileName);
				}
			}
			catch (DataLoadException ex)
			{
				_currentAnswer = null;
				return ex.Result;
			}

			return FinalizeLoading();
		}

		/// <summary>
		/// Tries to compile previously loaded solution.
		/// </summary>
		/// <returns>Null if compilation failed, full path to built binary otherwise.</returns>
		public string TryCompile()
		{
			if (IsEmptyLab)
			{
				return null;
			}

			_buildErrors = new List<BuildError>();
			if (_checkManager.Compile(_solutionPath))
			{
				return _checkManager.BinaryPath;
			}

			_buildErrors = _checkManager.BuildErrors;
			return null;
		}
		
		/// <summary>
		/// Calls necessary methods to check current lab for RSDN rules.
		/// </summary>
		public void CheckRsdn()
		{
			_errors = _checkManager.CheckRsdn(_currentAnswer.Sources);
		}
		
		/// <summary>
		/// Generates review, if possible.
		/// </summary>
		/// <returns>Path to generated review. Null if review cannot be generated
		/// (e.g 'name' not found).</returns>
		public string GenerateReview()
		{
			if (_currentAnswer != null)
			{
				if (_currentAnswer.Sources.Count == 0)
				{
					return _reviewGenerator.GenerateReviewNoSolution(_tempPath,
						 _currentAnswer.Data);
				}
				return _reviewGenerator.GenerateReview(_tempPath, _currentAnswer.Data,
					_buildErrors, _errors);
			}
			return null;
		}
		
		/// <summary>
		/// Initializes background check of current lab for originality
		/// against labs of same variant and number. Progress and result reports are done
		/// via events that's basically re-fired to the MainForm.
		/// </summary>
		public void CheckOriginality()
		{
			var toCheck = _dbWrapper.GetVaildForCheckAgainst(_currentAnswer);
			var argument = Tuple.Create(_currentAnswer, toCheck);
			_originalityWorker.RunWorkerAsync(argument);
		}
		
		/// <summary>
		/// Fires an event of percentage changing.
		/// </summary>
		/// <param name="percentage">Reported percentage to re-send.</param>
		private void ChangePercentage(int percentage)
		{
			if (percentage < 0 || percentage > 100)
			{
				throw new ArgumentException("Percentage must be in 0..100 range", "percentage");
			}
			var arg = new PercentageChangedEventArgs(percentage);
			OnPercentageChanged?.Invoke(this, arg);
		}
		
		/// <summary>
		/// Handles BackgroundWorker's native ProgressChanged event,
		/// re-fires it as it's own event.
		/// </summary>
		/// <param name="sender">Event sender.</param>
		/// <param name="e">Event arguments, ProgressPercentage is extracted to re-send.</param>
		private void WorkerProgressChanged(object sender, ProgressChangedEventArgs e)
		{
			// raises another event to inform MainForm about that
			// i cannot into multithreading, this might be cancer
			ChangePercentage(e.ProgressPercentage);
		}
		
		/// <summary>
		/// Fires an event of work completed.
		/// </summary>
		/// <param name="result">Reported result to re-send.</param>
		private void ReportResult()
		{
			// just notify we're done, sending results ruined incapsulation
			var arg = new EventArgs();
			OnWorkDone?.Invoke(this, arg);
		}
		
		/// <summary>
		/// Handles BackgroundWorker's native completed event,
		/// re-fires it as it's own event.
		/// Also sets human-readble summary of the check.
		/// </summary>
		/// <param name="sender">Event sender.</param>
		/// <param name="e">Event arguments, Result is extracted to re-send.</param>
		private void WorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			// null check there fires up another exception if work was cancelled

			if (e.Error == null && !e.Cancelled)
			{
				_potetinalPastes = e.Result as IReadOnlyList<Tuple<string, string>>;
				
				if (_potetinalPastes.Count == 0)
				{
					_originalityResult = _noSimilaritiesTemplate;
				}
				else
				{
					_originalityResult = String.Format(_similaritiesTemplate,
						_potetinalPastes.Count);
				}
				ReportResult();
			}
		}

		/// <summary>
		/// Shows found potential borrowings on a DiffForm.
		/// </summary>
		public void ShowDiff()
		{
			if (_potetinalPastes == null)
			{
				throw new ApplicationException("Run an originality check" +
					" before trying to show its results pretty please");
			}
			new DiffForm(_potetinalPastes).Show();
		}

		/// <summary>
		/// Method to cancel running oribinality check from outside of SystemManager.
		/// </summary>
		public void CancelOriginalityCheck()
		{
			if (_originalityWorker.IsBusy)
			{
				_originalityWorker.CancelAsync();
			}
		}
	}
}
