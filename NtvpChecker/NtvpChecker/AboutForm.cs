﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace NtvpChecker
{
	/// <summary>
	/// Class that is a fancy (no) form about this glorious (no) program.
	/// </summary>
	public partial class AboutForm : Form
	{
		/// <summary>
		/// Dict that maps library link labels to actual links to reuse code.
		/// </summary>
		private readonly Dictionary<LinkLabel, string> _libUrls;

		/// <summary>
		/// List of dumb quotes to randomly display below the kitty.
		/// </summary>
		private readonly List<string> _trivias = new List<string>()
		{
			"//TODO: Input parameter testing!",
			"Proofread then push /)_-",
			"Timeline lost. Go back in time and try again",
			"fix the sky a little",
			"when she shines for me at night",
			":thinking:"
		};

		/// <summary>
		/// Random generator to choose the quote.
		/// </summary>
		private Random _random = new Random();

		/// <summary>
		/// Constructor that also populates links dict.
		/// </summary>
		public AboutForm()
		{
			InitializeComponent();

			_libUrls = new Dictionary<LinkLabel, string>()
			{
				{ diffLinkLabel, "https://www.nuget.org/packages/DiffPlex/" },
				{ roslynLinkLabel, "https://www.nuget.org/packages/Microsoft.CodeAnalysis/"},
				{ mongoLinkLabel, "https://www.nuget.org/packages/MongoDB.Driver/"},
				{ compressLinkLabel, "https://www.nuget.org/packages/SharpCompress/"}
			};
		}

		/// <summary>
		/// Updates version number and trivia text on form load.
		/// </summary>
		/// <param name="sender">Event sender.</param>
		/// <param name="e">Event argument.</param>
		private void AboutForm_Shown(object sender, EventArgs e)
		{
			versionLabel.Text = $"версия {Application.ProductVersion}";
			triviaLabel.Text = _trivias[_random.Next(0, _trivias.Count)];
		}

		/// <summary>
		/// Handles click to link with repo URL.
		/// </summary>
		/// <param name="sender">Event sender.</param>
		/// <param name="e">Event argument.</param>
		private void repoLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			Process.Start(repoLinkLabel.Text);
		}

		/// <summary>
		/// Handles click to link with feedback e-mail.
		/// </summary>
		/// <param name="sender">Event sender.</param>
		/// <param name="e">Event argument.</param>
		private void mailLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			Process.Start($"mailto://{mailLinkLabel.Text}");
		}

		/// <summary>
		/// Handles clicks on all libs' links.
		/// </summary>
		/// <param name="sender">Event sender.</param>
		/// <param name="e">Event argument.</param>
		private void diffLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			var linkLabel = sender as LinkLabel;
			Process.Start(_libUrls[linkLabel]);
		}

		/// <summary>
		/// Handles clicks on useless quotes.
		/// </summary>
		/// <param name="sender">Event sender.</param>
		/// <param name="e">Event argument.</param>
		private void triviaLabel_Click(object sender, EventArgs e)
		{
			triviaLabel.Text = @"¯\_(ツ)_/¯";
			triviaLabel.ForeColor = SystemColors.ControlText;
		}
	}
}
