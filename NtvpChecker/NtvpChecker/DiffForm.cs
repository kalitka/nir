﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

using DiffPlex.DiffBuilder;
using DiffPlex.DiffBuilder.Model;

namespace NtvpChecker
{
	/// <summary>
	/// Description of DiffForm.
	/// </summary>
	public partial class DiffForm : Form
	{
		/// <summary>
		/// String to show when there are no entries in matches list.
		/// </summary>
		private const string _zeroCopies = "Совпадений не обнаружено!";
		
		/// <summary>
		/// String to show navigation in matches list. {0} is current number,
		/// {1} is total count.
		/// </summary>
		private const string _navigationFormat = "Совпадение {0} из {1}.";
		
		/// <summary>
		/// Field to store list of matches in.
		/// </summary>
		private readonly IReadOnlyList<Tuple<string, string>> _matches;
		
		/// <summary>
		/// SideBySideDiffBuilder instance to generate side-by-side diffs.
		/// </summary>
		private readonly ISideBySideDiffBuilder _differ
			= new SideBySideDiffBuilder(new DiffPlex.Differ());
		
		/// <summary>
		/// Internal zero-based representation of currently selected match.
		/// </summary>
		private int _currentRaw;
		
		/// <summary>
		/// Property to change current match. Changes everything relevant on this form.
		/// </summary>
		private int Current
		{
			get => _currentRaw;
			set
			{
				if (value < 0)
				{
					_currentRaw = _matches.Count - 1;
				}
				else if (value >= _matches.Count)
				{
					_currentRaw = 0;
				}
				else
				{
					_currentRaw = value;
				}
				
				if (_matches.Count > 0)
				{
					navigationLabel.Text = String.Format(_navigationFormat,
						_currentRaw + 1, _matches.Count);
					SetTexts(_matches[_currentRaw].Item1, _matches[_currentRaw].Item2);
				}
			}
		}
		
		/// <summary>
		/// Form constructor.
		/// </summary>
		/// <param name="matches">List of matches to display on this form.</param>
		public DiffForm(IReadOnlyList<Tuple<string, string>> matches)
		{
			if (matches == null)
			{
				throw new ArgumentNullException("matches");
			}
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			this.leftRichTextBox.MyVScroll += HandleLeftScroll;
			this.rightRichTextBox.MyVScroll += HandleRightScroll;
			
			_matches = matches;
			Current = 0;
			
			if (_matches.Count < 2)
			{
				prevButton.Enabled = false;
				nextButton.Enabled = false;
			}
			
			if (_matches.Count == 0)
			{
				navigationLabel.Text = _zeroCopies;
				leftHeaderLabel.Text = String.Empty;
				rightHeaderLabel.Text = String.Empty;
			}
		}
		
		/// <summary>
		/// Sets the texts throughout the form: both headers and diff textboxes.
		/// </summary>
		/// <param name="text1">First text of diff.</param>
		/// <param name="text2">Second text of diff.</param>
		private void SetTexts(string text1, string text2)
		{
			if (text1 == null)
			{
				throw new ArgumentNullException("text1");
			}
			if (text1.IndexOf(Environment.NewLine) == -1)
			{
				throw new ArgumentException("Diff text must contain at least 2 lines",
					"text1");
			}
			if (text2 == null)
			{
				throw new ArgumentNullException("text2");
			}
			if (text2.IndexOf(Environment.NewLine) == -1)
			{
				throw new ArgumentException("Diff text must contain at least 2 lines",
					"text2");
			}

			var newline = new[] {Environment.NewLine};
			
			leftRichTextBox.Clear();
			rightRichTextBox.Clear();
			
			var leftSplit = text1.Split(newline, 2, StringSplitOptions.RemoveEmptyEntries);
			leftHeaderLabel.Text = leftSplit[0];
			var leftText = leftSplit[1];
			// copy-paste programming is bad
			var rightSplit = text2.Split(newline, 2, StringSplitOptions.RemoveEmptyEntries);
			rightHeaderLabel.Text = rightSplit[0];
			var rightText = rightSplit[1];
			
			var diffmodel = _differ.BuildDiffModel(rightText, leftText);
			
			SetTextboxText(leftRichTextBox, diffmodel.NewText);
			SetTextboxText(rightRichTextBox, diffmodel.OldText);
		}
		
		/// <summary>
		/// Handles form resizing. Moves and resizes textboxes.
		/// </summary>
		/// <param name="sender">Event sender.</param>
		/// <param name="e">Event arguments.</param>
		private void DiffFormResize(object sender, EventArgs e)
		{
			// hardcoded magic numbers for offset of 8 from each side of the textboxes
			
			var newWidth = (this.Width / 2) - 24;
			leftRichTextBox.Width = newWidth;
			rightRichTextBox.Width = newWidth;
			
			leftHeaderLabel.Width = newWidth;
			rightHeaderLabel.Width = newWidth;
			
			rightRichTextBox.Left = this.Width / 2;
			rightHeaderLabel.Left = this.Width / 2;
		}
		
		/// <summary>
		/// Handles click on 'previous' button.
		/// All the logic is actually in _current's setter.
		/// </summary>
		/// <param name="sender">Event sender.</param>
		/// <param name="e">Event arguments.</param>
		private void PrevButtonClick(object sender, EventArgs e)
		{
			Current--;
		}
		
		/// <summary>
		/// Handles click on 'next' button.
		/// All the logic is actually in _current's setter.
		/// </summary>
		/// <param name="sender">Event sender.</param>
		/// <param name="e">Event arguments.</param>
		private void NextButtonClick(object sender, EventArgs e)
		{
			Current++;
		}

		/// <summary>
		/// Adds text with background color specified to a RichTextBox or its descendants.
		/// </summary>
		/// <param name="richTextBox">RichTextBox to add text to.</param>
		/// <param name="text">Text to add.</param>
		/// <param name="color">Background color.</param>
		private void AddTextWithBackground(RichTextBox richTextBox, string text,
			Color color)
		{
			if (richTextBox == null)
			{
				throw new ArgumentNullException("richTextBox");
			}
			if (text == null)
			{
				throw new ArgumentNullException("text");
			}
			if (color == null)
			{
				throw new ArgumentNullException("color");
			}

			var start = richTextBox.TextLength;
			richTextBox.AppendText(text);
			richTextBox.Select(start, text.Length);
			richTextBox.SelectionBackColor = color;
			richTextBox.DeselectAll();
		}
		
		/// <summary>
		/// Gets color to corresponding diff status: green for added,
		/// red for removed, white for everything else.
		/// </summary>
		/// <param name="diffPiece">DiffPiece to get color for.</param>
		/// <returns>Color this piece should have as background.</returns>
		private static Color GetColorForDiff(DiffPiece diffPiece)
		{
			switch (diffPiece.Type)
			{
				case ChangeType.Inserted:
					return Color.LightGreen;
				case ChangeType.Deleted:
					return Color.LightSalmon;
				default:
					return Color.White;
			}
		}
		
		/// <summary>
		/// Sets colored diff text to RichTextBox.
		/// </summary>
		/// <param name="richTextBox">RichTextBox to set text to.</param>
		/// <param name="diffPaneModel">DiffPaneModel containing data about the diff.</param>
		private void SetTextboxText(RichTextBox richTextBox, DiffPaneModel diffPaneModel)
		{
			if (richTextBox == null)
			{
				throw new ArgumentNullException("richTextBox");
			}
			if (diffPaneModel == null)
			{
				throw new ArgumentNullException("diffPaneModel");
			}

			foreach (var line in diffPaneModel.Lines)
			{
				// nulls are crashing the code
				if (line.Text == null)
				{
					line.Text = String.Empty;
				}
				
				if (line.Type != ChangeType.Modified)
				{
					var textToAdd = line.Text + Environment.NewLine;
					AddTextWithBackground(richTextBox, textToAdd, GetColorForDiff(line));
				}
				// if line was modified, it may still contain unmodified parts
				else
				{
					foreach (var linePart in line.SubPieces)
					{
						// same as the above
						if (linePart.Text == null)
						{
							linePart.Text = String.Empty;
						}
						AddTextWithBackground(richTextBox, linePart.Text,
							GetColorForDiff(linePart));
					}
					// end the line
					AddTextWithBackground(richTextBox, Environment.NewLine, Color.White);
				}
			}
		}
		
		/// <summary>
		/// 'Event handler' for the left textbox. Resends message to the right textbox.
		/// </summary>
		/// <param name="message">Message to resend.</param>
		private void HandleLeftScroll(ref Message message)
		{
			if (message == null)
			{
				throw new ArgumentNullException("message");
			}

			var sentMessage = Message.Create(rightRichTextBox.Handle, message.Msg,
				message.WParam, message.LParam);
			leftRichTextBox.DirectWndProc(ref sentMessage);
		}
		
		/// <summary>
		/// 'Event handler' for the right textbox. Resends message to the left textbox.
		/// </summary>
		/// <param name="message">Message to resend.</param>
		private void HandleRightScroll(ref Message message)
		{
			if (message == null)
			{
				throw new ArgumentNullException("message");
			}

			var sentMessage = Message.Create(leftRichTextBox.Handle, message.Msg,
				message.WParam, message.LParam);
			rightRichTextBox.DirectWndProc(ref sentMessage);
		}
	}
}
