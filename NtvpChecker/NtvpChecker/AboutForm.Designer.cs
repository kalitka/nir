﻿namespace NtvpChecker
{
	partial class AboutForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AboutForm));
			this.imageLabel = new System.Windows.Forms.Label();
			this.titleLabel = new System.Windows.Forms.Label();
			this.versionLabel = new System.Windows.Forms.Label();
			this.descriptionLabel = new System.Windows.Forms.Label();
			this.fossLabel = new System.Windows.Forms.Label();
			this.repoLinkLabel = new System.Windows.Forms.LinkLabel();
			this.feedbackLabel = new System.Windows.Forms.Label();
			this.mailLinkLabel = new System.Windows.Forms.LinkLabel();
			this.libsLabel = new System.Windows.Forms.Label();
			this.diffLinkLabel = new System.Windows.Forms.LinkLabel();
			this.roslynLinkLabel = new System.Windows.Forms.LinkLabel();
			this.mongoLinkLabel = new System.Windows.Forms.LinkLabel();
			this.compressLinkLabel = new System.Windows.Forms.LinkLabel();
			this.timelinelostLabel = new System.Windows.Forms.Label();
			this.triviaLabel = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// imageLabel
			// 
			this.imageLabel.Image = ((System.Drawing.Image)(resources.GetObject("imageLabel.Image")));
			this.imageLabel.Location = new System.Drawing.Point(12, 25);
			this.imageLabel.Name = "imageLabel";
			this.imageLabel.Size = new System.Drawing.Size(253, 256);
			this.imageLabel.TabIndex = 0;
			// 
			// titleLabel
			// 
			this.titleLabel.AutoSize = true;
			this.titleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.titleLabel.Location = new System.Drawing.Point(271, 9);
			this.titleLabel.Name = "titleLabel";
			this.titleLabel.Size = new System.Drawing.Size(195, 33);
			this.titleLabel.TabIndex = 1;
			this.titleLabel.Text = "NtvpChecker";
			// 
			// versionLabel
			// 
			this.versionLabel.AutoSize = true;
			this.versionLabel.Location = new System.Drawing.Point(472, 25);
			this.versionLabel.Name = "versionLabel";
			this.versionLabel.Size = new System.Drawing.Size(159, 13);
			this.versionLabel.TabIndex = 2;
			this.versionLabel.Text = "я не могу в интерфейсы, халп";
			// 
			// descriptionLabel
			// 
			this.descriptionLabel.Location = new System.Drawing.Point(274, 51);
			this.descriptionLabel.Name = "descriptionLabel";
			this.descriptionLabel.Size = new System.Drawing.Size(398, 133);
			this.descriptionLabel.TabIndex = 3;
			this.descriptionLabel.Text = resources.GetString("descriptionLabel.Text");
			// 
			// fossLabel
			// 
			this.fossLabel.AutoSize = true;
			this.fossLabel.Location = new System.Drawing.Point(274, 192);
			this.fossLabel.Name = "fossLabel";
			this.fossLabel.Size = new System.Drawing.Size(99, 13);
			this.fossLabel.TabIndex = 4;
			this.fossLabel.Text = "Open-source! (MIT)";
			// 
			// repoLinkLabel
			// 
			this.repoLinkLabel.AutoSize = true;
			this.repoLinkLabel.Location = new System.Drawing.Point(376, 192);
			this.repoLinkLabel.Name = "repoLinkLabel";
			this.repoLinkLabel.Size = new System.Drawing.Size(143, 13);
			this.repoLinkLabel.TabIndex = 5;
			this.repoLinkLabel.TabStop = true;
			this.repoLinkLabel.Text = "https://gitlab.com/kalitka/nir";
			this.repoLinkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.repoLinkLabel_LinkClicked);
			// 
			// feedbackLabel
			// 
			this.feedbackLabel.AutoSize = true;
			this.feedbackLabel.Location = new System.Drawing.Point(274, 208);
			this.feedbackLabel.Name = "feedbackLabel";
			this.feedbackLabel.Size = new System.Drawing.Size(190, 13);
			this.feedbackLabel.TabIndex = 6;
			this.feedbackLabel.Text = "Вопросы, предложения, замечания:";
			// 
			// mailLinkLabel
			// 
			this.mailLinkLabel.AutoSize = true;
			this.mailLinkLabel.Location = new System.Drawing.Point(464, 208);
			this.mailLinkLabel.Name = "mailLinkLabel";
			this.mailLinkLabel.Size = new System.Drawing.Size(102, 13);
			this.mailLinkLabel.TabIndex = 7;
			this.mailLinkLabel.TabStop = true;
			this.mailLinkLabel.Text = "diploma@bnfour.net";
			this.mailLinkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.mailLinkLabel_LinkClicked);
			// 
			// libsLabel
			// 
			this.libsLabel.AutoSize = true;
			this.libsLabel.Location = new System.Drawing.Point(274, 232);
			this.libsLabel.Name = "libsLabel";
			this.libsLabel.Size = new System.Drawing.Size(305, 13);
			this.libsLabel.TabIndex = 8;
			this.libsLabel.Text = "Работает при помощи вот этих замечательных библиотек:";
			// 
			// diffLinkLabel
			// 
			this.diffLinkLabel.AutoSize = true;
			this.diffLinkLabel.Location = new System.Drawing.Point(274, 248);
			this.diffLinkLabel.Name = "diffLinkLabel";
			this.diffLinkLabel.Size = new System.Drawing.Size(43, 13);
			this.diffLinkLabel.TabIndex = 9;
			this.diffLinkLabel.TabStop = true;
			this.diffLinkLabel.Text = "DiffPlex";
			this.diffLinkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.diffLinkLabel_LinkClicked);
			// 
			// roslynLinkLabel
			// 
			this.roslynLinkLabel.AutoSize = true;
			this.roslynLinkLabel.Location = new System.Drawing.Point(274, 264);
			this.roslynLinkLabel.Name = "roslynLinkLabel";
			this.roslynLinkLabel.Size = new System.Drawing.Size(116, 13);
			this.roslynLinkLabel.TabIndex = 10;
			this.roslynLinkLabel.TabStop = true;
			this.roslynLinkLabel.Text = "Microsoft.CodeAnalysis";
			this.roslynLinkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.diffLinkLabel_LinkClicked);
			// 
			// mongoLinkLabel
			// 
			this.mongoLinkLabel.AutoSize = true;
			this.mongoLinkLabel.Location = new System.Drawing.Point(274, 280);
			this.mongoLinkLabel.Name = "mongoLinkLabel";
			this.mongoLinkLabel.Size = new System.Drawing.Size(86, 13);
			this.mongoLinkLabel.TabIndex = 11;
			this.mongoLinkLabel.TabStop = true;
			this.mongoLinkLabel.Text = "MongoDB.Driver";
			this.mongoLinkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.diffLinkLabel_LinkClicked);
			// 
			// compressLinkLabel
			// 
			this.compressLinkLabel.AutoSize = true;
			this.compressLinkLabel.Location = new System.Drawing.Point(274, 296);
			this.compressLinkLabel.Name = "compressLinkLabel";
			this.compressLinkLabel.Size = new System.Drawing.Size(81, 13);
			this.compressLinkLabel.TabIndex = 12;
			this.compressLinkLabel.TabStop = true;
			this.compressLinkLabel.Text = "SharpCompress";
			this.compressLinkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.diffLinkLabel_LinkClicked);
			// 
			// timelinelostLabel
			// 
			this.timelinelostLabel.AutoSize = true;
			this.timelinelostLabel.Location = new System.Drawing.Point(592, 304);
			this.timelinelostLabel.Name = "timelinelostLabel";
			this.timelinelostLabel.Size = new System.Drawing.Size(86, 13);
			this.timelinelostLabel.TabIndex = 13;
			this.timelinelostLabel.Text = "Май–июнь 2018";
			// 
			// triviaLabel
			// 
			this.triviaLabel.ForeColor = System.Drawing.SystemColors.ControlDark;
			this.triviaLabel.Location = new System.Drawing.Point(12, 288);
			this.triviaLabel.Name = "triviaLabel";
			this.triviaLabel.Size = new System.Drawing.Size(253, 23);
			this.triviaLabel.TabIndex = 14;
			this.triviaLabel.Text = "N <3";
			this.triviaLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.triviaLabel.Click += new System.EventHandler(this.triviaLabel_Click);
			// 
			// AboutForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(684, 320);
			this.Controls.Add(this.triviaLabel);
			this.Controls.Add(this.timelinelostLabel);
			this.Controls.Add(this.compressLinkLabel);
			this.Controls.Add(this.mongoLinkLabel);
			this.Controls.Add(this.roslynLinkLabel);
			this.Controls.Add(this.diffLinkLabel);
			this.Controls.Add(this.libsLabel);
			this.Controls.Add(this.mailLinkLabel);
			this.Controls.Add(this.feedbackLabel);
			this.Controls.Add(this.repoLinkLabel);
			this.Controls.Add(this.fossLabel);
			this.Controls.Add(this.descriptionLabel);
			this.Controls.Add(this.versionLabel);
			this.Controls.Add(this.titleLabel);
			this.Controls.Add(this.imageLabel);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "AboutForm";
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.Text = "О программе";
			this.Shown += new System.EventHandler(this.AboutForm_Shown);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label imageLabel;
		private System.Windows.Forms.Label titleLabel;
		private System.Windows.Forms.Label versionLabel;
		private System.Windows.Forms.Label descriptionLabel;
		private System.Windows.Forms.Label fossLabel;
		private System.Windows.Forms.LinkLabel repoLinkLabel;
		private System.Windows.Forms.Label feedbackLabel;
		private System.Windows.Forms.LinkLabel mailLinkLabel;
		private System.Windows.Forms.Label libsLabel;
		private System.Windows.Forms.LinkLabel diffLinkLabel;
		private System.Windows.Forms.LinkLabel roslynLinkLabel;
		private System.Windows.Forms.LinkLabel mongoLinkLabel;
		private System.Windows.Forms.LinkLabel compressLinkLabel;
		private System.Windows.Forms.Label timelinelostLabel;
		private System.Windows.Forms.Label triviaLabel;
	}
}