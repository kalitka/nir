﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;
using NtvpChecker.DataStructures;

namespace NtvpChecker
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{
		/// <summary>
		/// Represents various statuses that originality check can be in.
		/// </summary>
		private enum OriginalityCheckStatus
		{
			/// <summary>
			/// Completed. Click on related button should show results.
			/// </summary>
			Done,
			/// <summary>
			/// In progess. Progressbar should be visible.
			/// Click on button should cancel the check.
			/// </summary>
			InProgress,
			/// <summary>
			/// Default. Click on button should start the check.
			/// </summary>
			NotStartedOrCancelled
		}

		/// <summary>
		/// SystemManager instance used by app.
		/// </summary>
		private readonly SystemManager _manager = new SystemManager();
		
		/// <summary>
		/// Path to compiled exe to run it from here.
		/// </summary>
		private string _pathToRun;
		
		/// <summary>
		/// Path to generated review to open it in whatever program opens RTF by default.
		/// </summary>
		private string _pathToReview;

		/// <summary>
		/// Field to store this form's originality check status.
		/// </summary>
		private OriginalityCheckStatus _status = OriginalityCheckStatus.NotStartedOrCancelled;
		
		/// <summary>
		/// Dictionary that maps bad DataLoadResults entries to human-readable
		/// error messages. Cancelled is deliberately excluded: it was user's choice,
		/// no need to remind right after.
		/// </summary>
		private readonly Dictionary<DataLoadResult, string> _errorMessages
			= new Dictionary<DataLoadResult, string>
		{
			// maybe these should be in app.config as well
			{ DataLoadResult.BadName, "Файл 'name' содержит ошибки оформления." },
			{ DataLoadResult.NoName, "Не удалось найти файл 'name' в корне архива." },
			{ DataLoadResult.NoSolution, "Не удалось найти ни одного решения в архиве." }
		};
		
		/// <summary>
		/// Form constructor with initialization.
		/// </summary>
		public MainForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			_manager.OnPercentageChanged += ChangePercentage;
			_manager.OnWorkDone += GetResult;
		}
		
		/// <summary>
		/// Handles 'open archive' button click by dispatching SystemManager
		/// to work on a given archive name.
		/// </summary>
		/// <param name="sender">Event sender.</param>
		/// <param name="e">Event arguments.</param>
		private void TempOpenButtonClick(object sender, EventArgs e)
		{
			if (openFileDialog1.ShowDialog() == DialogResult.OK)
			{
				ResetControls();
				LoadAndCheck(openFileDialog1.FileName);
			}
		}
		
		/// <summary>
		/// Handles 'run' button click. If it's clicked, _pathToRun contains path to an exe.
		/// </summary>
		/// <param name="sender">Event sender.</param>
		/// <param name="e">Event arguments.</param>
		private void TempRunButtonClick(object sender, EventArgs e)
		{
			Process.Start(_pathToRun);
		}
		
		/// <summary>
		/// Handles 'open review' button click. Pretty much the TempRunButtonClick.
		/// but on different button and with different variable.
		/// </summary>
		/// <param name="sender">Event sender.</param>
		/// <param name="e">Event arguments.</param>
		private void TempReviewButtonClick(object sender, EventArgs e)
		{
			Process.Start(_pathToReview);
		}
		
		/// <summary>
		/// Handles 'open copypaste log' button click.
		/// </summary>
		/// <param name="sender">Event sender.</param>
		/// <param name="e">Event arguments.</param>
		private void TempPasteButtonClick(object sender, EventArgs e)
		{
			switch (_status)
			{
				case OriginalityCheckStatus.Done:
					_manager.ShowDiff();
					break;
				case OriginalityCheckStatus.InProgress:
					_manager.CancelOriginalityCheck();
					originalityProgressBar.Visible = false;
					pastesDetectedLabel.Visible = true;
					_status = OriginalityCheckStatus.NotStartedOrCancelled;
					pasteCheckButton.Text = "Проверить оригинальность";
					break;
				case OriginalityCheckStatus.NotStartedOrCancelled:
					_manager.CheckOriginality();
					originalityProgressBar.Visible = true;
					pastesDetectedLabel.Visible = false;
					_status = OriginalityCheckStatus.InProgress;
					pasteCheckButton.Text = "Отменить проверку";
					break;

			}
		}
		
		/// <summary>
		/// Handles event of percentage of originality check changing.
		/// </summary>
		/// <param name="sender">Event sender, an instance of SystemManager.</param>
		/// <param name="e">Event argument, its Percentage property is used.</param>
		private void ChangePercentage(object sender, SystemManager.PercentageChangedEventArgs e)
		{
			originalityProgressBar.Value = e.Percentage;
		}
		
		/// <summary>
		/// Handles event of originality check succeding by retrieving its results.
		/// </summary>
		/// <param name="sender">Event sender, an instance of SystemManager.</param>
		/// <param name="e">Event argument, its Result property is used.</param>
		private void GetResult(object sender, EventArgs e)
		{
			pasteCheckButton.Enabled = true;
			
			originalityProgressBar.Visible = false;
			
			pastesDetectedLabel.Text = _manager.OriginalityData;
			pastesDetectedLabel.Visible = true;

			pasteCheckButton.Text = "Просмотреть заимствования";

			_status = OriginalityCheckStatus.Done;
		}

		/// <summary>
		/// Resets form's controls to initial state:
		/// most buttons are locked, labels are hidden
		/// </summary>
		private void ResetControls()
		{
			runButton.Enabled = false;
			reviewButton.Enabled = false;
			pasteCheckButton.Enabled = false;
			// hardcoded for now
			pasteCheckButton.Text = "Проверить оригинальность";

			originalityProgressBar.Value = 0;
			originalityProgressBar.Visible = false;

			authorDataLabel.Visible = false;
			buildErrorsLabel.Visible = false;
			rsdnErrorsLabel.Visible = false;
			pastesDetectedLabel.Visible = false;
		}

		/// <summary>
		/// Uses various SystemManager's methods and properties to check the archive
		/// and display information about that on this form.
		/// Fires up messagebox with error messages if anything goes wrong.
		/// </summary>
		/// <param name="filename">Path to the archive to use.</param>
		private void LoadAndCheck(string filename)
		{
			// this is temporary crap promoted to release-worthy crap
			var result = _manager.LoadData(filename);
			if (result == DataLoadResult.OK)
			{

				authorDataLabel.Text = _manager.AuthorData;
				authorDataLabel.Visible = true;

				// should probably return exe's path instead of status
				// if data is here, review is generated whatever is in the rest of archive
				var runPath = _manager.TryCompile();
				if (runPath != null)
				{
					_pathToRun = runPath;
					runButton.Enabled = true;
				}

				buildErrorsLabel.Text = _manager.BuildData;
				buildErrorsLabel.Visible = true;

				_manager.CheckRsdn();

				rsdnErrorsLabel.Text = _manager.RsdnData;
				rsdnErrorsLabel.Visible = true;

				var reviewPath = _manager.GenerateReview();
				if (reviewPath != null)
				{
					_pathToReview = reviewPath;
					reviewButton.Enabled = true;
				}
				
				if (_manager.IsOriginalityCheckPossible)
				{
					pasteCheckButton.Enabled = true;
				}
				pastesDetectedLabel.Text = _manager.OriginalityPreviewData;
				pastesDetectedLabel.Visible = true;
			}
			else
			{
				if (result != DataLoadResult.Cancelled)
				{
					MessageBox.Show(_errorMessages[result], "Ошибка",
						MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void aboutLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			new AboutForm().ShowDialog();
		}
	}
}
