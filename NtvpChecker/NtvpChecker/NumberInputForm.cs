﻿using System;
using System.Windows.Forms;

namespace NtvpChecker
{
	/// <summary>
	/// Used to ask lab's number from user.
	/// </summary>
	public partial class NumberInputForm : Form
	{
		/// <summary>
		/// Property to access set value from outside.
		/// </summary>
		public uint Number => (uint)numericUpDown.Value;

	    /// <summary>
		/// Constructor.
		/// </summary>
		public NumberInputForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			// everything but clicking OK returns this
			DialogResult = DialogResult.Cancel;
		}
		
		/// <summary>
		/// Handles 'Cancel' button click by simply closing this form.
		/// </summary>
		/// <param name="sender">Event sender.</param>
		/// <param name="e">Event arguments.</param>
		private void CancelButtonClick(object sender, EventArgs e)
		{
			Close();
		}
		
		/// <summary>
		/// Handles 'OK' button click by setting dialog result to OK and the closing this form.
		/// </summary>
		/// <param name="sender">Event sender.</param>
		/// <param name="e">Event arguments.</param>
		private void OkButtonClick(object sender, EventArgs e)
		{
			DialogResult = DialogResult.OK;
			Close();
		}
	}
}
