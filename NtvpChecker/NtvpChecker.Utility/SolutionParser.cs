﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;

using Microsoft.Build.BuildEngine;
using Microsoft.Build.Evaluation;

using Project = Microsoft.Build.Evaluation.Project;

namespace NtvpChecker.Utility
{
	/// <summary>
	/// Class to get list of user-written source files from a solution.
	/// </summary>
	public static class SolutionParser
	{
		/// <summary>
		/// Gets relative names of sources from the solution
		/// and corresponding names from unpacked folder.
		/// </summary>
		/// <param name="solutionPath">Path to solution to get sources from.</param>
		/// <returns>Dictionary: relative name -> full path</returns>
		public static Dictionary<string, string> GetSourceFilenames(string solutionPath)
		{

			if (solutionPath == null)
			{
				throw new ArgumentNullException("solutionPath");
			}

			if (!File.Exists(solutionPath))
			{
				throw new ArgumentException($"Unable to locate solution file {solutionPath}",
					"solutionPath");
			}

			var ret = new Dictionary<string, string>();
			
			// this is so magic
			var swp = SolutionWrapperProject.Generate(solutionPath, null, null);
			var xmlReader = XmlReader.Create(new MemoryStream(Encoding.Unicode.GetBytes(swp)));

			var project = ProjectCollection.GlobalProjectCollection.LoadProject(xmlReader);
			
			foreach (var projectNode in GetAllProjectReferences(project))
			{
				var path = Path.GetFullPath(Path.GetDirectoryName(solutionPath) +
					"\\" + projectNode.EvaluatedInclude);
				var dir = Path.GetDirectoryName(path) + "\\";

				var currentProject = ProjectCollection.GlobalProjectCollection
					.LoadProject(path);
				
				foreach (var source in GetSourceFiles(currentProject))
				{
					// first condition checks for *.Designer.cs files
					if (source.GetMetadataValue("DependentUpon") == String.Empty && 
						source.EvaluatedInclude != "Properties\\AssemblyInfo.cs")
					{
						var sourcePath = Path.GetFullPath(dir + source.EvaluatedInclude);
						var key = projectNode.EvaluatedInclude + "->" + source.EvaluatedInclude;
						ret[key] = sourcePath;
					}
				}
			}
			return ret;
		}
		
		//NOTE: the following was copypasted from the internets, works, but no idea how

		/// <summary>
		/// Gets projects from all present build levels, statring from 0.
		/// </summary>
		/// <param name="solutionWrapperProject">Solution to get projects from.</param>
		/// <returns>IEnumerable of projects present in the solution.</returns>
		private static IEnumerable<ProjectItem> GetAllProjectReferences(
			Project solutionWrapperProject)
		{
			int buildLevel = 0;
			while (true)
			{
				var items = solutionWrapperProject
					.GetItems($"BuildLevel{buildLevel}");
				if (items.Count == 0)
				{
					yield break;
				}
				foreach (var item in items)
				{
					yield return item;
				}
				buildLevel++;
			}
		}
		
		/// <summary>
		/// Gets all cs sources from a project.
		/// </summary>
		/// <param name="project">Project to get sources from.</param>
		/// <returns>IEnumerable of all cs files present in project.</returns>
		private static IEnumerable<ProjectItem> GetSourceFiles(Project project)
		{
			return project.GetItems("Compile");
		}
		
	}
}
