﻿using System.IO;

using SharpCompress.Archives;
using SharpCompress.Readers;

namespace NtvpChecker.Utility
{
	/// <summary>
	/// Class to handle archives unpacking. Supports zip, 7z, and rar.
	/// </summary>
	public static class ArchiveManager
	{
		/// <summary>
		/// Represents Sharpcompress' extractions options to use with unpacking.
		/// </summary>
		private static readonly ExtractionOptions _extractionOptions
			= new ExtractionOptions()
		{
			// extract saving archive's folder structure
			ExtractFullPath = true
		};
		
		/// <summary>
		/// Method to extract archive of any supported format.
		/// </summary>
		/// <param name="archivePath">Path to archive to extract.</param>
		/// <param name="unpackPath">Path to folder to extract to.
		/// Files from archives's root will be extracted to this folder's root.</param>
		public static void Unpack(string archivePath, string unpackPath)
		{
			using(var stream = File.OpenRead(archivePath))
			using(var reader = ArchiveFactory.Open(stream))
			{
				reader.WriteToDirectory(unpackPath, _extractionOptions);
			}
		}
	}
}
