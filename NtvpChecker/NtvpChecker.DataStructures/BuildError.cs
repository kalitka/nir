﻿using System;

namespace NtvpChecker.DataStructures
{
	/// <summary>
	/// A class to store build errors that may occur on auto-generated files
	/// and so may not have corresponding Source instance
	/// in SystemManager._currentAnswer.Sources.
	/// </summary>
	public class BuildError: Error
	{
		/// <summary>
		/// String that looks like Source.Identifier. Not a reference to a Source because
		/// build error may occur in files untracked by the app.
		/// (Like SharpDevelop's own form editor that sometimes kills *.Designer.cs files).
		/// </summary>
		public string Identifier { get; private set; }
		
		public BuildError(string identifier, uint line, string message): base(line, message)
		{
			Identifier = identifier ?? throw new ArgumentNullException("identifier");
		}
	}
}
