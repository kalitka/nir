﻿using System;

namespace NtvpChecker.DataStructures
{
	/// <summary>
	/// Exception that is throwed when data loading encounters problems.
	/// </summary>
	public class DataLoadException : ApplicationException
	{
		/// <summary>
		/// Field to store message about what exactly went wrong.
		/// </summary>
		public DataLoadResult Result { get; private set; }
		
		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="result">Message to store in this instance.</param>
		public DataLoadException(DataLoadResult result)
		{
			// enums are non-nullable, any set value is valid
			Result = result;
		}
	}
}