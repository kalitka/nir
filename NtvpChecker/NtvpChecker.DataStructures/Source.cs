﻿using System;
using System.IO;
using System.Collections.Generic;

namespace NtvpChecker.DataStructures
{
	/// <summary>
	/// Represents single user-written source file that need to be checked by the system.
	/// </summary>
	public class Source
	{
		
		/// <summary>
		/// Source 'identifier', loosely represents its path.
		/// Looks like 'Folder\Project.csproj->Source.cs' for now.
		/// </summary>
		public string Identifier { get; private set; }

		/// <summary>
		/// File lines as read-only list.
		/// </summary>
		public IReadOnlyList<string> Content => _content.AsReadOnly();

		/// <summary>
		/// Actual list to store lines in.
		/// </summary>
		private List<string> _content = new List<string>();

		/// <summary>
		/// Class constructor.
		/// </summary>
		/// <param name="relativeName">Name of the file relative to solution,
		/// used throughout rest of processing.</param>
		/// <param name="pathToFile">Source file name to read.</param>
		public Source(string relativeName, string pathToFile)
		{
			Identifier = relativeName ?? throw new ArgumentNullException("relativeName");
			
			if (pathToFile == null)
			{
				throw new ArgumentNullException("pathToFile");
			}
			if (!File.Exists(pathToFile))
			{
				throw new ArgumentException($"File {pathToFile} does not exist", "pathToFile");
			}

			using (var file = new StreamReader(pathToFile))
			{
				while (!file.EndOfStream)
				{
					_content.Add(file.ReadLine());
				}
			}
		}
	}
}
