﻿using System;
using System.IO;

namespace NtvpChecker.DataStructures
{
	/// <summary>
	/// Holds information about laboratory work: author, variant, and lab's number.
	/// </summary>
	public class Metadata
	{
		/// <summary>
		/// Author property, contains data like "Иванов И. И.".
		/// </summary>
		public string Author { get; private set; }
		
		/// <summary>
		/// Variant property.
		/// </summary>
		public uint Variant { get; private set; }
		
		/// <summary>
		/// Author's city property to fill out the review.
		/// </summary>
		public string City { get; private set; }
		
		/// <summary>
		/// Lab's number stored as a property.
		/// </summary>
		public uint LabNumber { get; private set; }
		
		/// <summary>
		/// Contains whitespace symbol used to split data in one line.
		/// </summary>
		private const char _splitter = ' ';
		
		/// <summary>
		/// Number of variants of lab present.
		/// </summary>
		private const uint _variantNumber = 9;
		
		/// <summary>
		/// Class constructor.
		/// </summary>
		/// <param name="pathToName">Path to 'name' file to extract data from.</param>
		/// <param name="labNumber">Number of the current work.</param>
		public Metadata(string pathToName, uint labNumber)
		{
			LabNumber = labNumber > 0 ? labNumber
				: throw new ArgumentException("labNumber must be greater than zero", "labNumber");

			if (!File.Exists(pathToName))
			{
				throw new ArgumentException($"File {pathToName} does not exist", "pathToName");
			}
			if (pathToName == null)
			{
				throw new ArgumentNullException("pathToName");
			}

			// this is why we can't have nice things, not utf-8
			using (var file = new StreamReader(pathToName,
				System.Text.Encoding.GetEncoding(1251)))
			{
				file.ReadLine();
				var nameLine = file.ReadLine();
				var cityAndVariantLine = file.ReadLine();
				
				if (nameLine == null || cityAndVariantLine == null)
				{
					throw new DataLoadException(DataLoadResult.BadName);
				}
				
				var nameParts = nameLine.Split(_splitter);
				
				if (nameParts.Length < 3 || nameParts.Length > 4)
				{
					throw new DataLoadException(DataLoadResult.BadName);
				}
				// this is ugly
				var secondName = (nameParts.Length == 4) 
					? $"{nameParts[2][0]}."
					: String.Empty;
				
				Author = $"{nameParts[0]} {nameParts[1][0]}. {secondName}";

				// this is too
				var cityAndVariantParts = cityAndVariantLine.Split(_splitter);
				if (cityAndVariantParts.Length != 4)
				{
					throw new DataLoadException(DataLoadResult.BadName);
				}
				
				City = cityAndVariantParts[0].Replace('_', ' ');
				
				try
				{
					var rawVariant = uint.Parse(cityAndVariantParts[2]);
					var calculatedVariant = (rawVariant * _variantNumber) / 100;
					Variant = calculatedVariant > 0 
						? calculatedVariant 
						: _variantNumber;
				}
				catch (FormatException)
				{
					throw new DataLoadException(DataLoadResult.BadName);
				}
			}
		}
	}
}
