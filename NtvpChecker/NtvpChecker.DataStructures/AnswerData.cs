﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using NtvpChecker.Utility;

namespace NtvpChecker.DataStructures
{
	/// <summary>
	/// Represents information about laboratory work,
	/// including full text of all checked sources.
	/// </summary>
	public class AnswerData
	{
		/// <summary>
		/// Metadata property. Holds author, variant and lab's number.
		/// </summary>
		public Metadata Data { get; private set; }

		/// <summary>
		/// Sources from all projects of the work.
		/// </summary>
		public IReadOnlyList<Source> Sources => _sources.AsReadOnly();
		
		/// <summary>
		/// String to hold path to unpacked solution file.
		/// Used only on AnswerData generated from unpacked archive,
		/// not on those loaded from DB.
		/// </summary>
		public string SolutionPath { get; private set; }

		/// <summary>
		/// Private list to store project sources in.
		/// </summary>
		private List<Source> _sources = new List<Source>();

		/// <summary>
		/// Constructor that only loads MetaData, leaving sources blank.
		/// </summary>
		/// <param name="rootPath">Root path of unpacked folder.
		/// Should contain 'name' to get metadata in root,
		/// and a solution somewhere.</param>
		/// <param name="labNumber">Number of the current work.</param>
		public AnswerData(string rootPath, uint labNumber)
		{
			if (rootPath == null)
			{
				throw new ArgumentNullException("rootPath");
			}
			if (!Directory.Exists(rootPath))
			{
				throw new ArgumentException($"Folder {rootPath} does not exist", "rootPath");
			}
			if (labNumber == 0)
			{
				throw new ArgumentException("labNumber must be greater than zero", "labNumber");
			}

			var namePath = rootPath + "\\" + "name";
			if (!File.Exists(namePath))
			{
				throw new DataLoadException(DataLoadResult.NoName);
			}

			Data = new Metadata(namePath, labNumber);

			SolutionPath = null;
		}

		/// <summary>
		/// Constructor that loads full data.
		/// </summary>
		/// <param name="rootPath">Root path of unpacked folder.
		/// Should contain 'name' to get metadata in root,
		/// and a solution somewhere.</param>
		/// <param name="labNumber">Number of the current work.</param>
		/// <param name="explicitSolutionPath">(Optional) path to solution to be used
		/// in review process. If null, defaults to most top-level detected one.</param>
		public AnswerData(string rootPath, uint labNumber, string explicitSolutionPath)
			: this(rootPath, labNumber)
		{
			if (explicitSolutionPath == null)
			{
				var solutions = Directory.GetFiles(rootPath, "*.sln", SearchOption.AllDirectories);
				if (solutions.Length == 0)
				{
					throw new DataLoadException(DataLoadResult.NoSolution);
				}

				// there really should be only one solution, but i need to foolproof it anyway
				// for now it chooses the one with least backslashes on its path
				// e.g. the most highlevel one. Still not ideal.

				// hooray for linq making wierd one-liners like i love in python here
				SolutionPath = solutions.OrderBy(x => GetSymbolCount('\\', x)).First();
			}
			else
			{
				if (!File.Exists(explicitSolutionPath))
				{
					throw new DataLoadException(DataLoadResult.NoSolution);
				}
				SolutionPath = explicitSolutionPath;
			}

			try
			{
				var dict = SolutionParser.GetSourceFilenames(SolutionPath);
				foreach (var key in dict.Keys)
				{
					_sources.Add(new Source(key, dict[key]));
				}
			}
			// this is cool
			catch (Exception ex) when (ex is ArgumentNullException || ex is ArgumentException)
			{
				// can't make this null since it's wrapped as readonly property
				// length of zero will signify there's no sources
				_sources = new List<Source>();
			}
		}
		
		/// <summary>
		/// Get count of appearances of a given char in a string. 
		/// </summary>
		/// <param name="c">Char to search.</param>
		/// <param name="s">String to search in.</param>
		/// <returns>How many times char is in the string.</returns>
		private static uint GetSymbolCount(char c, string s)
		{
			// see https://stackoverflow.com/a/10391499
			return (uint)(s.Split(c).Length - 1);
		}
	}
}
