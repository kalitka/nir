﻿using System;

namespace NtvpChecker.DataStructures
{
	/// <summary>
	/// Class that holds data on RSDN check errors
	/// which are tied to Source instances in classes using it.
	/// </summary>
	public class Error
	{
		/// <summary>
		/// Line number corresponding to error in source.
		/// </summary>
		public uint Line { get; private set; }
		
		/// <summary>
		/// Textual representation of error message. Includes error code in build errors.
		/// </summary>
		public string Message { get; private set; }
		
		/// <summary>
		/// Constructor that sets values of properties.
		/// </summary>
		/// <param name="line">Error line number.</param>
		/// <param name="message">Error message.</param>
		public Error(uint line, string message)
		{
			Line = line > 0? line: throw new ArgumentException("line number must be greater"
				+ " than zero", "line");
			Message = message ?? throw new ArgumentNullException("message");
		}
	}
}
