﻿namespace NtvpChecker.DataStructures
{
    /// <summary>
    /// Enum to represent results of data loading.
    /// </summary>
    public enum DataLoadResult
	{
		/// <summary>
		/// Malformed name file.
		/// </summary>
		BadName,
		/// <summary>
		/// Operation was cancelled by user by not specifying lab number.
		/// </summary>
		Cancelled,
		/// <summary>
		/// No 'name' file was found in archive's root.
		/// </summary>
		NoName,
		/// <summary>
		/// No VS solution was found in archive.
		/// </summary>
		NoSolution,
		/// <summary>
		/// Everything went fine.
		/// </summary>
		OK
	}
}