﻿using System.Collections.Generic;

using NtvpChecker.DataStructures;

namespace NtvpChecker.Database
{
	/// <summary>
	/// Interface to database to store and retrieve instances of AnswerData from.
	/// </summary>
	public interface IDBWrapper
	{
		/// <summary>
		/// Saves AnswerData instance to DB. Should overwrite any present data
		/// with same Metadata.
		/// </summary>
		/// <param name="data">Data to save to the DB.</param>
		void SaveToDB(AnswerData data);
		
		/// <summary>
		/// Loads AnswerData of same variant from the DB, excluding one
		/// with the same Metadata.
		/// </summary>
		/// <param name="data">AnswerData to get variant from.</param>
		/// <returns>List of AnswerData of same variant from other authors.</returns>
		IReadOnlyList<AnswerData> GetVaildForCheckAgainst(AnswerData data);
	}
}
