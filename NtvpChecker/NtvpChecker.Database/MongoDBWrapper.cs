﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;

using MongoDB.Bson.Serialization;
using MongoDB.Driver;

using NtvpChecker.DataStructures;

namespace NtvpChecker.Database
{
	// BUG: crashes silently if something is wrong instead of an unhandled exception
	
	/// <summary>
	/// IDBWrapper implementation that uses MongoDB to store AnswerData.
	/// </summary>
	public class MongoDBWrapper: IDBWrapper
	{
		/// <summary>
		/// Port to launch mongod on. The random has spoken.
		/// </summary>
		private const uint _port = 47597;
		
		/// <summary>
		/// (Hardcoded) relative path to mongod executable.
		/// </summary>
		private const string _mongodPath = "mongodb\\mongod.exe";
		
		/// <summary>
		/// (Hardcoded) relative path to folder to store DB files in.
		/// Mongo generates 300 mb of journal files right of the bat, make this configurable?
		/// </summary>
		private const string _runInPath = "mongodb\\db\\";
		
		/// <summary>
		/// Process instance to control our mongod instance.
		/// </summary>
		private readonly Process _mongoProcess = new Process();
		
		// these are all needed to nicely quit the console app
		// see StopDB() for usage
		// documentation courtesy of msdn
		
		/// <summary>
		/// Corresponds to the Ctrl-C signal sent to console.
		/// See https://docs.microsoft.com/en-us/windows/console/handlerroutine for other codes.
		/// </summary>
		private const int _controlCEvent = 0;
		
		/// <summary>
		/// Sends a specified signal to a console process group that shares
		/// the console associated with the calling process.
		/// </summary>
		/// <param name="dwCtrlEvent">The type of signal to be generated. 0 for Ctrl-C.</param>
		/// <param name="dwProcessGroupId">The identifier of the process group
		/// to receive the signal. If this parameter is zero, the signal is generated
		/// in all processes that share the console of the calling process.</param>
		/// <returns>True is the function succeeds, false otherwise.</returns>
		[DllImport("kernel32.dll")]
		private static extern bool GenerateConsoleCtrlEvent(uint dwCtrlEvent,
			uint dwProcessGroupId);
		
		/// <summary>
		/// Attaches the calling process to the console of the specified process.
		/// </summary>
		/// <param name="dwProcessId">The identifier of the process whose console is to be used.
		/// PID of a process or -1 to use the console of the parent of the current process.</param>
		/// <returns>True is the function succeeds, false otherwise.</returns>
		[DllImport("kernel32.dll")]
		private static extern bool AttachConsole(uint dwProcessId);
		
		/// <summary>
		/// Detaches the calling process from its console.
		/// </summary>
		/// <returns>True is the function succeeds, false otherwise.</returns>
		[DllImport("kernel32.dll")]
		private static extern bool FreeConsole();

		/// <summary>
		/// Adds or removes an application-defined HandlerRoutine function from the list 
		/// of handler functions for the calling process.
		/// If no handler function is specified, the function sets an inheritable attribute
		/// that determines whether the calling process ignores CTRL+C signals.
		/// </summary>
		/// <param name="handlerRoutine">A pointer to the application-defined HandlerRoutine
		/// function to be added or removed. This parameter can be NULL.</param>
		/// <param name="Add">If this parameter is TRUE, the handler is added; if it is FALSE,
		/// the handler is removed. If the HandlerRoutine parameter is NULL, a TRUE value
		/// causes the calling process to ignore CTRL+C input,  and a FALSE value restores
		/// normal processing of CTRL+C input. This attribute of ignoring or processing
		/// CTRL+C is inherited by child processes.</param>
		/// <returns>True is the function succeeds, false otherwise.</returns>
		[DllImport("kernel32.dll")]
		private static extern bool SetConsoleCtrlHandler(ConsoleCtrlDelegate handlerRoutine,
			bool Add);

		/// <summary>
		/// An application-defined function used with the SetConsoleCtrlHandler function.
		/// A console process uses this function to handle control signals received by the process.
		/// When the signal is received, the system creates a new thread in the process
		/// to execute the function.
		/// </summary>
		/// <param name="ctrlType">The type of control signal received by the handler.
		/// 0 for Ctrl-C.</param>
		/// <returns>True if function handled the control signal, false otherwise.</returns>
		delegate bool ConsoleCtrlDelegate(uint ctrlType);
		
		/// <summary>
		/// Connection string for our pcket server, localhost:_port
		/// </summary>
		private readonly string _connectionString = $"mongodb://localhost:{_port}";

		/// <summary>
		/// Database name to store data in.
		/// </summary>
		private const string _dbName = "NtvpDatabase";
		
		/// <summary>
		/// Name of collection inside of the database.
		/// </summary>
		private const string _collectionName = "SavedLabs";
		
		/// <summary>
		/// Client instance to use in DB operations.
		/// </summary>
		private readonly MongoClient _mongoClient;
		
		/// <summary>
		/// Database instance. Only used to get collection from it.
		/// </summary>
		private readonly IMongoDatabase _database;
		
		/// <summary>
		/// Collection instance. Its methods actually perform DB operations.
		/// </summary>
		private readonly IMongoCollection<AnswerData> _collection;

		/// <summary>
		/// AnswerData that was most recent one to be passed to GetVaildForCheckAgainst.
		/// If another call uses the same, we can return cached results.
		/// </summary>
		private AnswerData _cacheKey;

		/// <summary>
		/// Field to store most recently loaded results.
		/// </summary>
		private IReadOnlyList<AnswerData> _cache;

		/// <summary>
		/// Constructor. Starts MongoDB server with port and path
		/// specified by _port and _runInPath respectively.
		/// </summary>
		public MongoDBWrapper()
		{
			_mongoProcess.StartInfo.FileName = _mongodPath;
			// journaling is off for now, we're not serious business
			// see https://docs.mongodb.com/manual/core/journaling/
			_mongoProcess.StartInfo.Arguments
				= $"--port {_port} --dbpath {_runInPath} --nojournal";

			_mongoProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
			// set _mongoProcess.StartInfo.UseShellExecute to false to see
			// the mongod console for debug purposes
			_mongoProcess.Start();
			
			// object to db mapping, pretty much 'make Data property a key
			// and also include _sources pretty please'
			BsonClassMap.RegisterClassMap<AnswerData>(cm =>
			{
				cm.MapIdProperty("Data");
				cm.MapField("_sources");
			});

			// converting Source.Content to read-only property broke implicit mapping
			// from the call above. So here's the same thing, but explicit.
			BsonClassMap.RegisterClassMap<Source>(cm =>
			{
				cm.MapProperty("Identifier");
				cm.MapField("_content");
			});

			// connect to server, db, and collection
			try
			{
				_mongoClient = new MongoClient(_connectionString);
				_database = _mongoClient.GetDatabase(_dbName);
				_collection = _database.GetCollection<AnswerData>(_collectionName);
			}
			catch (MongoConnectionException)
			{
				throw new ApplicationException("Unable to connect to MongoDB");
			}
		}
		
		/// <summary>
		/// Destructor. Cleanly shuts down MongoDB server by sending Ctrl-C to it
		/// and waiting for it to shut down.
		/// </summary>
		~MongoDBWrapper()
		{
			// wierd magic
			// see https://stackoverflow.com/a/29274238
			if (AttachConsole((uint)_mongoProcess.Id))
			{
				SetConsoleCtrlHandler(null, true);
				try
				{
					GenerateConsoleCtrlEvent(_controlCEvent, 0);
					_mongoProcess.WaitForExit();
				}
				finally
				{
					FreeConsole();
					SetConsoleCtrlHandler(null, false);
				}
			}
			else
			{
				throw new ApplicationException("Something went wrong trying to stop MongoDB");
			}
		}
		
		/// <summary>
		/// Saves AnswerData instance to DB. Overwrites any present data with same Metadata.
		/// </summary>
		/// <param name="data">Data to save to the DB.</param>
		public void SaveToDB(AnswerData data)
		{
			if (data == null || data.Data == null)
			{
				throw new ArgumentNullException("data");
			}
			// search for the record with the same id
			// is there any way i can store and reuse that expression?
			var cursor = _collection.Find(xx => xx.Data == data.Data);
			if (cursor.Count() > 0)
			{
				// we can load it before replacing and do a diff for example
				// to do so we should probably change the interface
				// so this method returns old version or null if nothing found,
				// then SystemManager manages the diff
				_collection.ReplaceOne(xx => xx.Data == data.Data, data,
					new UpdateOptions
					{
						IsUpsert = true
					});
			}
			else
			{
				// just insert it
				_collection.InsertOne(data);
			}
			// saving may alter results, just to be safe
			_cache = null;
			_cacheKey = null;
		}
		
		/// <summary>
		/// Loads AnswerData of same variant and lab number from the DB,
		/// excluding one with the same Metadata.
		/// </summary>
		/// <param name="data">AnswerData to get variant from.</param>
		/// <returns>List of AnswerData of same variant and lab number from other authors.</returns>
		public IReadOnlyList<AnswerData> GetVaildForCheckAgainst(AnswerData data)
		{
			if (data == null || data.Data == null)
			{
				throw new ArgumentNullException("data");
			}
			// caching
			if (data == _cacheKey && _cache != null)
			{
				return _cache;
			}

			var cursor = _collection.Find(xx => xx.Data != data.Data 
				&& xx.Data.Variant == data.Data.Variant
				&& xx.Data.LabNumber == data.Data.LabNumber);

			_cacheKey = data;
			_cache = cursor.ToList().AsReadOnly();

			return _cache;
		}
	}
}
