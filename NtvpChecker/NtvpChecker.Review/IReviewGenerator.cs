﻿using System.Collections.Generic;

using NtvpChecker.DataStructures;

namespace NtvpChecker.Review
{
	/// <summary>
	/// An interface to generate reviews for labs.
	/// </summary>
	public interface IReviewGenerator
	{
		/// <summary>
		/// Main method to generate review.
		/// </summary>
		/// <param name="folder">Folder to create review in.</param>
		/// <param name="metadata">Lab's Metadata to get values for header fields from.</param>
		/// <param name="buildErrors">List of build errors encountered.</param>
		/// <param name="errors">Dictionary that maps checked Sources to their errors.</param>
		/// <returns>Full path to generated review.</returns>
		string GenerateReview(string folder, Metadata metadata,
			IReadOnlyList<BuildError> buildErrors,
			IReadOnlyDictionary<Source, List<Error>> errors);
		
		/// <summary>
		/// Generates review stating that the solution was not found in the archive.
		/// </summary>
		/// <param name="folder">Folder to create review in.</param>
		/// <param name="metadata">Lab's Metadata to get values for header fields from.</param>
		/// <returns>Full path to generated review.</returns>
		string GenerateReviewNoSolution(string folder, Metadata metadata);
	}
}
