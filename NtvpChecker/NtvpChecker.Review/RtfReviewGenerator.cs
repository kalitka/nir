﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

using NtvpChecker.DataStructures;

namespace NtvpChecker.Review
{
	/// <summary>
	/// Class to form reviews in rich text format.
	/// </summary>
	public class RtfReviewGenerator: IReviewGenerator
	{
		/// <summary>
		/// (Hardcoded) path to template file.
		/// </summary>
		private const string _relativeTemplatePath = "Documents\\template.rtf";
		
		/// <summary>
		/// File name to save review to.
		/// </summary>
		private const string _reviewFilename = "review.rtf";
		
		/// <summary>
		/// String containing RTF file, modified to get the actual result.
		/// </summary>
		private string _workArea;
		
		/// <summary>
		/// A regex to match russian letters. Used to detect them to encode for RTF.
		/// </summary>
		private readonly Regex _cyrillicChar = new Regex("[а-яё]", RegexOptions.Compiled
			| RegexOptions.IgnoreCase);
		
		// these are specific for the format, no need to make them external
		
		/// <summary>
		/// Template placeholder for student's name.
		/// </summary>
		private const string _authorPlaceholder = "[STUDENT_NAME_HERE]";
		
		/// <summary>
		/// Template placeholder for student's city.
		/// </summary>
		private const string _cityPlaceholder = "[STUDENT_CITY_HERE]";
		
		/// <summary>
		/// Template placeholder for date of review.
		/// </summary>
		private const string _datePlaceholder = "[DATE]";
		
		/// <summary>
		/// Template placeholder for reviewer's name.
		/// </summary>
		private const string _reviewerPlaceholder = "[YOUR_NAME_HERE]";
		
		/// <summary>
		/// Template placeholder for errors.
		/// </summary>
		private const string _errorsPlaceholder = "[HERE_BE_ERRORS]";
		
		/// <summary>
		/// Placeholder to where the mark should be set if there are no errors.
		/// </summary>
		private const string _okPlaceholder = "[+]";
		
		/// <summary>
		/// Placeholder to where the mark should be set if there are any errors.
		/// </summary>
		private const string _notOkPlaceholder = "[-]";
		
		/// <summary>
		/// Template placeholder for lab's number.
		/// </summary>
		private const string _numberPlaceholder = "[N]";
		
		/// <summary>
		/// String to hold full RTF code of one full paragraph for error output.
		/// </summary>
		private string _paragraphValue;
		
		/// <summary>
		/// Message to show when there are no solution found.
		/// </summary>
		private readonly string _noSolution
			= ConfigurationManager.AppSettings["NoSolution"];
		
		/// <summary>
		/// Header for build errors, shown in bold above them.
		/// </summary>
		private readonly string _buildErrorsHeader
			= ConfigurationManager.AppSettings["BuildErrorsHeader"];
		
		/// <summary>
		/// Format string for build errors. Should contain three format specifiers:
		/// {0} if for BuildError.Identifier, {1} for BuildError.Line, and 
		/// {2} for BuildError.Message.
		/// </summary>
		private readonly string _buildErrorFormat
			= ConfigurationManager.AppSettings["BuildErrorFormat"];
		
		/// <summary>
		/// Format string for an error that is present on a single line in file.
		/// {0} is for Error.Message, {1} is for Error.Line.
		/// </summary>
		private readonly string _errorFormatSingleLine
			= ConfigurationManager.AppSettings["RSDNSingle"];
		
		/// <summary>
		/// Format string for an error that is present on two or more lines in file.
		/// {0} is for Error.Message, {1} is for Error.Line.
		/// </summary>
		private readonly string _errorFormatMultiLine
			= ConfigurationManager.AppSettings["RSDNMulti"];
		
		/// <summary>
		/// String to represent mark set.
		/// </summary>
		private readonly string _markToSet
			= ConfigurationManager.AppSettings["AcceptMark"];
		
		/// <summary>
		/// Reviewer name.
		/// </summary>
		private readonly string _reviewer
			= ConfigurationManager.AppSettings["Reviewer name"];
		
		/// <summary>
		/// Main method to generate review.
		/// </summary>
		/// <param name="folder">Folder to create review in.</param>
		/// <param name="metadata">Lab's Metadata to get values for header fields from.</param>
		/// <param name="buildErrors">List of build errors encountered.</param>
		/// <param name="errors">Dictionary that maps checked Sources to their errors.</param>
		/// <returns>Full path to generated review. Basically, 'folder\review.rtf' for now.</returns>
		public string GenerateReview(string folder, Metadata metadata,
			IReadOnlyList<BuildError> buildErrors,
			IReadOnlyDictionary<Source, List<Error>> errors)
		{
			LoadTemplate();
			FillHeader(metadata);
			// it is only used in GenerateReviewNoSolution
			RemoveErrorPlaceholder();
			
			var failed = (buildErrors.Count > 0 || errors.Count > 0);
			SetAccepted(!failed);
			
			// shows build errors, if any
			// can probably be moved to its own helper method
			if (buildErrors.Count > 0)
			{
				InsertParagraph(_buildErrorsHeader, true);
				foreach (var be in buildErrors)
				{
					var msg = String.Format(_buildErrorFormat, be.Identifier, be.Line,
						be.Message);
					InsertParagraph(msg);
				}
				// empty line to keep things nice and readable
				InsertParagraph("");
			}
			// shows RSDN errors, if any
			// same note about moving as the above
			foreach (var key in errors.Keys)
			{
				InsertParagraph(key.Identifier, true);
				// used to group errors with same messages
				var errorMap = new Dictionary<string, List<uint>>();
				
				foreach (var error in errors[key])
				{
					// if there was no list, let's make one
					if (!errorMap.ContainsKey(error.Message))
					{
					    errorMap[error.Message] = new List<uint>
					    {
					        error.Line
					    };
					}
					else
					{
						if (!errorMap[error.Message].Contains(error.Line))
						{
							errorMap[error.Message].Add(error.Line);
						}
					}
				}
				// writes everything created earlier
				foreach (var mappedError in errorMap.Keys)
				{
					errorMap[mappedError].Sort();
					var formatMask = errorMap[mappedError].Count > 1 
						? _errorFormatMultiLine 
						: _errorFormatSingleLine;
					
					var lines = String.Join(", ", errorMap[mappedError]);
					var message = String.Format(formatMask, mappedError, lines);
					InsertParagraph(message);
				}
				// files are separated by an empty line
				InsertParagraph("");
			}
			
			return SaveToFolder(folder);
		}
		
		/// <summary>
		/// Generates review stating that the solution was not found in the archive.
		/// </summary>
		/// <param name="folder">Folder to create review in.</param>
		/// <param name="metadata">Lab's Metadata to get values for header fields from.</param>
		/// <returns>Full path to generated review. Basically 'folder\review.rtf' for now.</returns>
		public string GenerateReviewNoSolution(string folder, Metadata metadata)
		{
			// much less code when we don't have to put errors
			LoadTemplate();
			FillHeader(metadata);
			SetNoSolutionErrorText();
			SetAccepted(false);
			
			return SaveToFolder(folder);
		}
		
		/// <summary>
		/// Saves generated review to a folder under a set name.
		/// </summary>
		/// <param name="folder">Folder to save to.</param>
		/// <returns>Full path to saved file.</returns>
		private string SaveToFolder(string folder)
		{
			var path = Path.Combine(folder, _reviewFilename);
			using (var sw = new StreamWriter(path))
			{
				sw.Write(_workArea);
			}
			return path;
		}
		
		/// <summary>
		/// Sets a mark indicating whether the work was accepted or not.
		/// </summary>
		/// <param name="value">True if accepted, false otherwise.</param>
		private void SetAccepted(bool value)
		{
			var yesText = value
			    ? _markToSet 
			    : String.Empty;
			var noText = value
			    ? String.Empty 
			    : _markToSet;
			_workArea = _workArea.Replace(_okPlaceholder, yesText)
				.Replace(_notOkPlaceholder, noText);
		}
		
		/// <summary>
		/// Loads template from predefined location, also loads paragraph config from it.
		/// </summary>
		private void LoadTemplate()
		{
			using (var sr = new StreamReader(_relativeTemplatePath))
			{
				_workArea = sr.ReadToEnd();
			}
			
			// here we find minimal paragraph config from "[HERE_BE_ERRORS]" one.
			// this config will be useful to populate error lists
			// it contains that text, starts with "\par " and ends with "}"
			// here we search exactly for that
			var textIndex = _workArea.IndexOf(_errorsPlaceholder);
			var parIndex = _workArea.LastIndexOf("\\par ", textIndex);
			var bracketIndex = _workArea.IndexOf("}", parIndex);
			_paragraphValue = _workArea.Substring(parIndex, bracketIndex - parIndex + 1);
		}
		
		/// <summary>
		/// Fills the header with values provided by metadata.
		/// </summary>
		/// <param name="metadata">Metadata to get values from.</param>
		private void FillHeader(Metadata metadata)
		{
			// rip performance, tossing ~70k strings like that
			_workArea = _workArea.Replace(_authorPlaceholder, Encode(metadata.Author))
				.Replace(_cityPlaceholder, Encode(metadata.City))
				.Replace(_datePlaceholder, Encode($"{DateTime.Now:d}"))
				.Replace(_reviewerPlaceholder, Encode(_reviewer))
				.Replace(_numberPlaceholder, Encode(metadata.LabNumber.ToString()));
		}
		
		/// <summary>
		/// Changes errors placeholder text to _noSolution string.
		/// </summary>
		private void SetNoSolutionErrorText()
		{
			_workArea = _workArea.Replace(_errorsPlaceholder, Encode(_noSolution));
		}
		
		// this is so ugly
		
		/// <summary>
		/// Encodes string to store it in RTF file. Cyrillic letters are stored as \uDDDD\'3f,
		/// where DDDD is decimal unicode code. Also backslashes and braces should be escaped.
		/// </summary>
		/// <param name="s">String to encode.</param>
		/// <returns>Encoded string that can be put into RTF files.</returns>
		private string Encode(string s)
		{
			var sb = new StringBuilder();
			foreach (var c in s)
			{
				if (c == '\\' || c == '{' || c == '}')
				{
					sb.Append($@"\{c}");
				}
				// i cannot stress enough this is all ugly
				else if (_cyrillicChar.IsMatch(c.ToString()))
				{
					// conversion to int gives us decimal code
					sb.AppendFormat(@"\u{0:D4}\'3f", ((int)c));
				}
				else
				{
					sb.Append(c);
				}
			}
			return sb.ToString();
		}
		
		/// <summary>
		/// Inserts a new paragraph at the end of the document.
		/// </summary>
		/// <param name="text">String to insert, no need to call Encode on it.</param>
		/// <param name="bold">Whether paragraph should be bold. Used for various headers.</param>
		private void InsertParagraph(string text, bool bold = false)
		{
			var index = _workArea.LastIndexOf("}");
			var formatted = _paragraphValue.Replace(_errorsPlaceholder, Encode(text));
			if (bold)
			{
				// have no idea which of these is working
				if (formatted.IndexOf("\\b0") != -1)
				{
					formatted = formatted.Replace("\\b0", "\\b1");
				}
				else
				{
					var boldIndex = formatted.IndexOf("\\cf1");
					formatted.Insert(boldIndex + 3, "\\b1");
				}
			}
			_workArea = _workArea.Insert(index, formatted);
		}
		
		/// <summary>
		/// Removes "[HERE_BE_ERRORS]" paragraph from the document.
		/// </summary>
		private void RemoveErrorPlaceholder()
		{
			_workArea = _workArea.Remove(_workArea.IndexOf(_paragraphValue),
				_paragraphValue.Length);
		}
	}
}
